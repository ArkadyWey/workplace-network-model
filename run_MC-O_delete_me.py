#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# External
import numpy
import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Internal
import visualise
import topography
import models
import output
import utils

# -----------------------------------------------
# README:

# run_MC-O module:
# Take a json file with a dictionary of parameters as a parser argument
# Initialise node locations and states
# Define matrix, each vector being the opacity of each node on a trial

# For each opacity set, run the model on a randomised network, num_state_run times,
# then move to the next opacity set
# -----------------------------------------------

start_time = time.time()
print("Started simulation at relative time: {}\n".format(start_time - start_time))

# ---------------------------------------------------------------------------------------------------------------------------
# File and directory management
# ---------------------------------------------------------------------------------------------------------------------------
# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")
parser.add_argument("-f", dest="parameter_file_path", required=True,
                    help="Input parameter file path for simulation", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Open parameter file at parameter_file_path returned by parser
try: 
    parser_args = parser.parse_args()
    # Read parameter_file and load contents as parameters_dict  
    with open(parser_args.parameter_file_path, 'r') as parameter_file:
        parameters_dict = json.load(parameter_file)
except Exception:
    raise

# Define directory where parameters file is located 
# This is used as a reference directory to define an output directory for data
parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_path))

# If output directory does not exist in directory, make one (used first time we run simulation)
# Use parameter flie directory as a reference
if os.path.exists(os.path.join(parameters_dir_path, 'output')) == False:
    os.mkdir(os.path.join(parameters_dir_path, 'output'))

# Define output file path
output_dir_path = os.path.join(parameters_dir_path, 'output')
# ------------------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------------------------------------------
# Parameters
# ---------------------------------------------------------------------------------------------------------------------------

# Monte Carlo parameters
# -----------------------
num_opacities    = parameters_dict["N"] + 1 # number of outer loop runs - first no opaques, then 1, ..., then N
#num_species_runs = parameters_dict["N"] + 1
#num_species_runs = int(parameters_dict["N"]/5) + 1
num_state_runs   = parameters_dict["num_state_runs"] # number of inner loop runs (to average over in MC)

# Initial conditions
# -------------------
# Everyone in work initially
location_initial = parameters_dict["iW"]*numpy.ones(parameters_dict["N"])

# Everyone Susceptible initially, apart from node 0, who is Exposed
state_initial      = parameters_dict["iS"]*numpy.ones(parameters_dict["N"]) # assigned states 1,2,3,4  (S, E, I, A -- Susceptible, Exposed, Infectious, Absent)
state_initial[0]   = parameters_dict["iE"]

# Opacities to test
# species_set[i,k] = the opacity of node i on the kth trial
species_set        = numpy.ones((parameters_dict["N"],num_opacities))

# Set opacities so in first trial 0 opaque, then second trial 0,1 opaque, then 0,1,2 opaque etc...
species_set_opaques      = parameters_dict["iO"]*numpy.ones((parameters_dict["N"],parameters_dict["N"]))
species_set_transparents = parameters_dict["iT"]*numpy.ones((parameters_dict["N"],parameters_dict["N"]))
liars_triu               = numpy.triu(m=species_set_opaques,k=0)
truers_tril              = numpy.tril(m=species_set_transparents,k=-1)
species_mix              = numpy.add(truers_tril, liars_triu)
species_set[:,1::]       = species_mix

# Define trials to be included - i.e. to save memory, don't try every possible population opacity, skip some.
#o_vals = numpy.concatenate((numpy.linspace(0,50,51), numpy.linspace(52,100,13) ))
o_vals = numpy.linspace(0,100,21)
print("o_vals: ", o_vals)

num_species_runs = len(o_vals) # number of opacity trials.

# ---------------------------------------------------------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------------------------------------------------------
# k indexes o_vals
for k in range(num_species_runs):
    print("species_run:", k, "of:", num_species_runs-1)
    
    o = int(o_vals[k])
    # Define species
    # species[i] = opacity of node i
    species = species_set[:,o]

    # Save defined species vector to file
    output.species_to_csv(i_species_run=o,
                          species=species,
                          output_dir_path=output_dir_path)

    for r in range(num_state_runs):
        print("state_location_run:", r, "of:", num_state_runs-1, "(species run:", k, "of:", num_species_runs-1,")")

        # Define random network for this inner run
        # (See topography module for details)
        A = topography.specify_prob(parameters_dict["N"], numbers_list=[0,1], prob_of_number=[1.0-parameters_dict["d"], parameters_dict["d"]])
        
        # Initialise location and state (reset state and location from last run)
        # ---------------
        # state[i,t] = state of node i at time t
        state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"]))
        state[:,0]    = state_initial

        # location[i,t] = location of node i at time t
        location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"]))
        location[:,0] = location_initial
        
        # Calculate location and state
        for t in range(parameters_dict["T"]-1): # T-1 because the model looks forward -- t+1th time is implied by the tth time.
            for i in range(parameters_dict["N"]):

                # Calculate location and state of i at t+1 given information about i at t
                # (See models module for details)
                location[i,t+1], state[i,t+1] =  models.triple_status_six_state(A=A, 
                                                                                N=parameters_dict["N"], 
                                                                                T=parameters_dict["T"],
                                                                                species=species, 
                                                                                location=location, 
                                                                                state=state,
                                                                                i=i, 
                                                                                t=t, 
                                                                                t_incubation=parameters_dict["t_incubation"], 
                                                                                t_asymptomatic=parameters_dict["t_asymptomatic"], 
                                                                                t_symptomatic=parameters_dict["t_symptomatic"], 
                                                                                t_quarantined=parameters_dict["t_quarantined"], 
                                                                                p_unlucky=parameters_dict["p_unlucky"], 
                                                                                p_spread=parameters_dict["p_spread"],
                                                                                p_neverUnwell=parameters_dict["p_neverUnwell"],
                                                                                p_recovery=parameters_dict["p_recovery"], 
                                                                                p_OAdviseOthers=parameters_dict["p_OAdviseOthers"], 
                                                                                p_OFollowAdvice=parameters_dict["p_OFollowAdvice"],
                                                                                iT=parameters_dict["iT"], 
                                                                                iO=parameters_dict["iO"], 
                                                                                iW=parameters_dict["iW"],
                                                                                iH=parameters_dict["iH"],       
                                                                                iS=parameters_dict["iS"], 
                                                                                iE=parameters_dict["iE"], 
                                                                                iIA=parameters_dict["iIA"], 
                                                                                iIS=parameters_dict["iIS"], 
                                                                                iR=parameters_dict["iR"], 
                                                                                iQ=parameters_dict["iQ"]
                                                                                )

        # Save calculated location and state matrix for all i and t to file
        output.state_location_to_csv(i_species_run=o,
                                     i_state_location_run=r,              
                                     location=location, 
                                     state=state,
                                     output_dir_path=output_dir_path
                                     )

print("\nFinished simulation at relative time:  {}".format(time.time() - start_time))