#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import numpy
import visualise

def ascii_to_array(file_path):
    """
    "../simulations/data_rootFind/delta09m"
    """
    # Convert ascii table of stirngs to list of strings (with shape of original table)
    cwd = os.getcwd()

    file = os.path.join(cwd,file_path)
    #file = file_path

    f=open(file,"r")
    lines=f.readlines()
    rows=[]
    for x in lines:

        row = x.split(" ")

        rows.append(row)

    f.close()

    # Convert list of strings to array of floats (with shape of original table)
    rows_floats = []
    for i_row in range(len(rows)):
        row = []
        for i_col in range(len(rows[0])):
            row.append(float(rows[i_row][i_col]))
        rows_floats.append(row)


    rows_floats_array = numpy.array(rows_floats)
    return rows_floats_array



path_diff = ["1","3","5","7","9"]
linestyles = [":", "--", "-.",(0, (15,2,5,2)), "-", ]

fig,gs,ax = visualise.plot_single_frame()
for i_path_diff in range(len(path_diff)):
    file_path = "../simulations/analytic/data_rootFind/delta0{}m".format(path_diff[i_path_diff])
    array = ascii_to_array(file_path=file_path)
    ax.plot(array[:,0], array[:,1], label=r"$\delta = 0.{}$".format(path_diff[i_path_diff]), ls = linestyles[i_path_diff])

ax.set_ylim(-0.03,1.03)
ax.set_xlim(-0.3,10.3)
ax.set_xticks([0,2,4,6,8,10])
ax.legend()
visualise.style_axes(ax, r"$r_0$",  r"$R^\infty$")
visualise.save_svg(fig, gs, os.path.join(os.getcwd(), "../simulations/analytic/R_inf_V_r_0.svg"))








