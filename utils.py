import os

# ------------------------------
# README
# utils module
# Miscellaneous working functions
# -------------------------------

def check_if_parser_arg_exists(parser, file_as_parser_arg):
    """
    Desciption 
    ------------
    Check that parser argument that has been parsed exists, 
    and return it if so.
    """
    if not os.path.exists(file_as_parser_arg):
        parser.error("The file {} does not exist!".format(file_as_parser_arg))
    else:
        return os.path.abspath(file_as_parser_arg)  