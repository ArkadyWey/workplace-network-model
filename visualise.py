#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External
import os
import numpy
import networkx

# PLotting
import matplotlib as mpl
#if os.environ.get('DISPLAY','') == '':
#    print('no display found. Using non-interactive Agg backend')
#    mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


def plot_single_frame(w=6.0, h=4.5, font_size=14, font='Helvetica Neue',plot_grid=True):
    """Create a single frame plot

    Parameters
    ----------
    w: float
        Width of plot in inches. Default is 6

    h: float
        Height of plot in inches. Default is 4.5

    font_size: 14
        Font size for axes text in pts. Default is 14.

    font: str
        Valid font name string. Note that the font needs to be installed
        in your system and available in the matplotlib font cache. Default
        is `Helvetica Neue`

    plot_grid: Boolean
        Whether to include a grid in the plot or not. Default is true.

    Returns
    -------
    fig: matplotlib.pyplot.Figure 
        A matplotlib figure instance

    gs: matplotlib.gridspec.Gridspec
        Matplotlib grid specification

    ax: matplotlib.pyplot.Axes
        Matplotlib axis
    """
    fontprops = { 'family':'sans-serif', 'sans-serif':[ font, 'sans-serif' ], 
                  'weight':'regular', 'style':'normal', 'size':font_size }
    mpl.rc('font',**fontprops)
    fig = plt.figure(figsize=(w,h))
    gs = gridspec.GridSpec(1,1)
    ax = fig.add_subplot(gs[0])

    if plot_grid:
        ax.grid(color='lightgray', ls='--', lw=0.95)

    return fig, gs, ax

def style_axes(ax, xlabel, ylabel, fontsize=16, labelpad=8):
    """
    Desciption 
    -----------
    Set labels for graph, and axes props for plot

    Parameters
    -----------
    ax: matplotlib.pyplot.Axes
        plot axes defined by matplotlib module
    xlabel: str
        label for x axis
    ylabel: str
        label for y axis
    fontsize: float
        size of font on plot
    labelpad: float
        padding before axes labels
    """
    ax.set_xlabel(xlabel, fontsize=fontsize, labelpad=labelpad)
    ax.set_ylabel(ylabel, fontsize=fontsize, labelpad=labelpad)
    
        
def save_svg(fig, gs, plotfile, rect=None):
    """
    Desciption
    ----------
    Save figure as svg

    Parameters
    ----------------
    fig: maplotlib.pyplot.Figure
        Figure to save
    gs: matplotlib.gridspec.Gridspec
        Matplotlib grid specification
    plotfile: str
        strong to sae plot as
    """
    gs.tight_layout(fig, pad=1.05, rect=rect)
    plt.savefig(plotfile, format='svg', box_inches='tight')
    plt.close(fig)

def save_png(fig, gs, plotfile):
    """"
    Desciption
    ----------
    Save figure as png

    Parameters
    ----------------
    fig: maplotlib.pyplot.Figure
        Figure to save
    gs: matplotlib.gridspec.Gridspec
        Matplotlib grid specification
    plotfile: str
        strong to sae plot as
    """
    gs.tight_layout(fig, pad=1.05)
    plt.savefig(plotfile, format='png', box_inches='tight')
    plt.close(fig)


def draw_graph( adjacency_matrix,
                N, t,
                species, location, state, 
                iT=1, iO=-1, 
                iW=1, iH=-1,       
                iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6,
                labels=None, graph_layout='shell',
                node_size=500, node_alpha=1.0,
                node_text_size=12,
                edge_color='black', edge_alpha=1.0, edge_tickness=1.5,
                edge_text_pos=0.3,
                text_font='sans-serif'):
    """
    Desciption
    --------------
    Given an adjacency matrix, create a graph representing its associated network at time t.
    Choose the color of each node according to its current state at time t.
    Choose the shape of each node according to its opacity - transparent nodes are circles, and opaque nodes are square.
    Choose the alpha od each node according to its opacity - transparent nodes are half transparentm and opaque nodes are fully solid.

    Parameters
    ------------
    adjacency_matrix: numpy.ndarray
        Adjacency matric of teh associated network to represent with the graph
    N: int
        number of nodes - size of the adjacnecy matrix (N x N)
    t: int
        time at which to represent the network (since nodes are in different states at different times)
    species: numpy.ndarray
        species[i] = opacity of node i
    location: numpy.ndaray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    state ints iX: associated ints of states
    labels: list
        node labels
    graph_layout: string
        Layout of nodes in graph - as determined by networkx module - see documentation for external module for details.
    node_size: int
        Size of nodes
    node_alpha: float 
        Default alpha of nodes
    node_text_size: int
        Default text size of node labels.
    edge_color: string
        Color of edges
    edge_alpha: float
        Default alpha of edges.
    edge_tickness: float 
        Thicknes of edges.
    edge_text_pos: float
        Position of edge text.
    text_font: string
        Name of text font to be used.

    Returns
    ---------
    G: networkx.Graph
        Graph of the network associated to the adjacency matrix specified.
    """

    # create networkx graph
    G = networkx.Graph()

    # create list of edge tuples from adjacency matrix
    i_edge, j_edge = numpy.where(adjacency_matrix == 1)
    i_edge_list    = list(i_edge)
    j_edge_list    = list(j_edge)

    edge_zip       = zip(i_edge_list, j_edge_list)
    edge_list      = list(edge_zip)

    # add edges to graph
    for edge in edge_list:
        G.add_edge(edge[0], edge[1])

    # Define graph position (layout)
    # these are different layouts for the network you may try
    # shell seems to work best
    if graph_layout == 'spring':
        graph_pos=networkx.drawing.layout.spring_layout(G)
    elif graph_layout == 'spectral':
        graph_pos=networkx.drawing.layout.spectral_layout(G)
    elif graph_layout == 'random':
        graph_pos=networkx.drawing.layout.random_layout(G)
    else:
        graph_pos=networkx.drawing.layout.shell_layout(G)
        
    # Draw nodes
    for i in range(N):
        # Choose color according to state
        if state[i,t] == iS:
            node_color = 'green'
        elif state[i,t] == iE:
            node_color = 'orange'
        elif state[i,t] == iIA:
            node_color = 'red'
        elif state[i,t] == iIS:
            node_color = 'tab:pink'
        elif state[i,t] == iQ:
            node_color = 'blue'
        elif state[i,t] == iR:
            node_color = 'lightgrey'
        else :
            raise Exception

        # Choose shape according to species
        if species[i] == iT:
            node_shape = 'o'
        elif species[i] == iO:
            node_shape = 's'
        else:
            raise Exception

        # Choose alpha according to location
        #if location[i,t] == iW:
        #    node_alpha = 1.0
        #elif location[i,t] == iH:
        #    node_alpha = 0.5
        #else:
        #    raise Exception

        # Choose node alpha according to species
        if species[i] == iT:
            node_alpha = 0.5
        elif species[i] == iO:
            node_alpha = 1.0
        else:
            raise Exception

        networkx.draw_networkx_nodes(G,graph_pos,node_size=node_size, nodelist = [i],
                                   alpha=node_alpha, node_color=node_color, 
                                   node_shape=node_shape)



    # Draw edges
    networkx.draw_networkx_edges(G,graph_pos,width=edge_tickness,
                           alpha=edge_alpha,edge_color=edge_color)
    
    # Draw node labels
    networkx.draw_networkx_labels(G, graph_pos,font_size=node_text_size,
                                  font_family=text_font)

    # Draw edge labels
    #if labels is None:
    #    labels = range(len(graph))

    #edge_labels = dict(zip(graph, labels))
    #networkx.draw_networkx_edge_labels(G, graph_pos, edge_labels=edge_labels, 
    #                            label_pos=edge_text_pos)
    
    return G