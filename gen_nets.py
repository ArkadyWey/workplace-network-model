#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External
import numpy
import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Internal
import visualise
import topography
import models
import output
import utils


# -----------------------------------------------
# README:
# gen_nets module (generate a set of network plot examples)

# Plot some example networks using the network plotting function 
# visualise.draw_graph().

# In the present example:
# We choose network of size 15 (if size is too large then will need to redefine node locations.).
# Choose probabilities (0.3, 0.5) of a connection between node i and node j.
# Since N=15, these result in degree \approx 4 and 8 respectively.
# Choose number of opaques - number of square nodes to have in network pictures (5,10 here)
# Initialise the states and locations of the nodes (to choose their colors in the plot)
# Export the set of figures to the location specified by figure_dir

# -----------------------------------------------


# Fetch current directory
current_dir = os.getcwd()

# Define directory to save network figures using current_dir as a base
figure_dir = os.path.join(current_dir, "./tests/test_gen_nets")

# Define number of nodes in network and number of times to simulate
N         = 15
T         = 1

# degreeProbs[k] = probability of adding a one to the i,j position of the 
# adjacency matrix (see topography.specify_prob() for details) on the kth run.
degreeProbs = [0.3,0.5]
# degrees[k] = actual average network degree that results from the kth degree probability.
degrees     = [4,8]
# num_opaques[k] = number of opaque nodes in population on run k
num_opaques = [5,10]

for i_d, d in enumerate(degreeProbs):
    for i_num_o, num_o in enumerate(num_opaques):
        for r in range(5):
            print(r)

            # Initial conditions
            # ---------------
            species               = 1*numpy.ones(N) # assigned types 1 or -1 (T, L -- Transparent or Opaque)
            species[0:num_o]      = (-1)*numpy.ones(num_o)

            state_initial         = 1*numpy.ones(N) # assigned states 1,2,3,4  (S, E, I, A -- Susceptible, Exposed, Infectious, Absent)
            state_initial[0]      = 2

            location_initial      = 1*numpy.ones(N)

            # Initialise
            # ---------------
            state         = numpy.zeros((N,T)) # Each column is state at given time
            state[:,0]    = state_initial

            location      = numpy.zeros((N,T)) # Each column is state at given time
            location[:,0] = location_initial

            # Adjacency matrix
            A = topography.specify_prob(N,numbers_list=[0,1], prob_of_number=[1.0-d, d])


            # Plot: network
            # --------------
            for t in range(T):

                fig,gs,ax = visualise.plot_single_frame(plot_grid=False)
                #ax.text(0.8, 0.95, "Day: {}".format(t), fontsize=15)

                ax.tick_params(
                    axis='both',       # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    left=False,        # ticks along the top edge are off
                    right=False,       # ticks along the top edge are off
                    labelbottom=False, # labels along the bottom edge are off
                    labelleft=False)   # labels along the bottom edge are off

                visualise.draw_graph(A,
                                     N, t,
                                     species, location, state, 
                                     iT=1, iO=-1, 
                                     iW=1, iH=-1,       
                                     iS=1.0, iE= 2.0, iIA=3.0, iIS=4.0, iR=5.0, iQ=6.0,
                                     labels=None, graph_layout='spring',
                                     node_size=500, node_alpha=1.0,
                                     node_text_size=12,
                                     edge_color='darkgrey', edge_alpha=1.0, edge_tickness=1.5,
                                     edge_text_pos=0.3,
                                     text_font='sans-serif'
                                    )

                visualise.save_svg(fig, gs, os.path.join(figure_dir, "net_d-{}_no-{}_r-{}.svg".format(degrees[i_d],num_o,r) ) )