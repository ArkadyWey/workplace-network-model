# README #


### What is this repository for? ###

- This was the code used to run the network models that are described and explained the paper
A. Wey et al, The benefits of peer transparency in safe workplace operation post pandemic lockdown.
This paper is available in the arxiv here: https://arxiv.org/pdf/2007.03283.pdf
- This repo includes the code to run the simulation, the codes to generate the plots that are seen in the paper, and
all figures that are in the paper themselves.

### What is in this repository? ###

#### Code 
- The repo is split into four kinds of modules of code:
    - run modules run the simulation and save the results to data files.
    - use modules use the data files that were created by the run modules to generate plots.
    - gen modules generate plots without using data files.
    - other modules consist of functions that run and use modules call to execits their tasks.
- Brief summary of each module:
    - run_I.py runs the model given a dictionary file of parameters. For example, it takes the parameter file found at ./tests/test_run_I/parameters.json
    and returns the figures found at ./tests/test_run_I/  . To run this code, pass the parameter file with the flag -i.
    - run_MC-O.py runs the model given a dictionary file of parameters a specified number of times, and saves the result of every run to a data file.
    All parameter files used to generate the data used for the publication are at ./tests/test_run_and_use_MC-O/...
    The structure of these directories are described below.
    - use_MC-O_X.py. Given a dictionary file of parameters, use the associated data files generated by using these parameters to generate some figures.
        - If X=bar, then some statistics are plotted as a bar plot against the states and saved to [location of parameter file]/../paper/V_State.
        - If X=ser, then some statistics are plotted as a time series and saved to [location of parameter file]/../paper/V_Time.
        - If X=deg, then some statistics are plotted as a function of the degree and saved to [location of parameter file]/../paper/V_Degree.
        - If X=opa, then some statistics (the same ones that are plotted against degree) are plotted as a function of the opacity and saved to [location of parameter file]/../paper/V_Opacity.
    - topography.py includes functions that generate the associated adjacency matrices for given workplace networks.
    - models.py includes function that describes how the state and location of a node evolves in time. This model is described in summary in the module itslef, and in detail in the paper listed above.
    - output.py includes functions used to generate data files, and functions iused to generate relevant statistics, which are summarised in the module, and described in the paper.
    - utils.py contains miscellaneous functions that are used.
    - visualise.py contains functions that are used for plotting - including the network plotter, which uses the networkx module.
    - gen_nets.py generates example network figures - the examples used in the paper were generated with teh current setup, and can be found at ./tests/gen_nets.
    - gen_analytic_I_V_r_Sweep-a generates the plot for the first analytic result in the paper.
    The plot can be found at tests/test_gen_analytic_I_V_r_Sweep-a/
    - gen_analytic_R_inf_V_r_o generates the plot for the second result in the paper. The data used to generate this plot, and the figure  in the paper, 
    can be found at tests/test_gen_analytic_R_inf_V_r_0/.

#### Folders   
- archive  contains old code which the user needn't look at.
- tests, as mentioned above, includes an example of the results of each of the executable modules. Within ./tests/
    - test_gen_nets - example network figures generated by gen_nets.py as above.
    - test_run_I - example of result of run_I.py. test_run_I/network is a figure showing the dynamics of the network as the fnuction in models.pychanges the state of each node.
    - test_run_and_use_MC-O - example of result of run_MC-O.py and use_MC_O_X.py. Within this directory there are two directories:cold_T-100_NSR-101_NSLR-50 - this means that all parameter files within are cold runs (meaning that there is a very small underlying risk ofinfection), the end time T is 100, the number of species runs is 101 - this means that, as the population opacities 0, ..., 100 were swept, 101 values were considered, meaning that every opacity 0, 1, 2, 3, ..., 100 was simulated. NSLR is the number of state-location runs - this is thenumber of model runs that were averaged over.
    So data files found at cold_T-100_NSR-101_NSLR-50/deg-X/output/ will be 
    species_iSR-k where k = 0,1,2,...,100.
    state_iSR-k_iSLR-j where j = 0,1,2,...,49.
    hot_T-100_NSR-21_NSLR-100 - this means that all parameter files within are hot runs (meaning that there is a very small underlying risk ofinfection), the end time T is 100, the number of species runs is 21 - this means that, as the population opacities 0, ..., 100 were swept, 21values were considered, meaning opacities were considered in steps of 5, so 0, 5, 10, 15,... NSLR is the number of state-location runs - this isthe number of model runs that were averaged over.
    This means that data files found at hot_T-100_NSR-21_NSLR-100/deg-X/output/ will be 
    species_iSR-k where k = 0,5,10,...,100.
    state_iSR-k_iSLR-j where j = 0,1,2,...,99.
    Within each of these directories, there are 8 simulation folders and a paper folder.
    Each simulation folder contains a parameter file that is identical apart from a different degree probability.
    So for each parameter file, we monte carlo for a particular average network degree, for all of the possible opacities, before moving to the nextaverage network degree, by choosing the next parameter file.
    So output data files at each degree are in the output folder at [location of parameter file]/output.
    Figures in the directory [location of parameter file] show a glimpse of what happens at this degree.
    In paper, we see the figures generated by runnign the use_X.py modules on all data files in the dependent directories.

### Who do I talk to? ###

- Repo owner: Arkady Wey (Mathematics Institute, University of Oxford) arkady.wey@maths.ox.ac.uk
- Other contacts: Alan Champneys (University of Bristol) a.r.champneys@bristol.ac.uk, Rosemary Dyson (University of Birmingham) r.j.dyson@bham.ac.uk