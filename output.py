#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy
import os

# ----------------------------------------------------------------
# README
# output module
# Define functions that determine properties of the workforce,
# for example, productivity.
# Also define functions that save output of the run_MC-O module to 
# data files.
# ----------------------------------------------------------------


def state_indicator(N,
                    state, 
                    iT=1, iO=-1, 
                    iW=1, iH=-1,       
                    iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6):
    """
    Desciption
    ------------
    Determine whether node i experienced state s at least once.

    Parameters.
    -----------
    N: int
        Number of nodes
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    state indices: ints
    
    Returns:
    ----------
    state_indication:
        state_indication[s,i] = 1 if node i did enter state s
                                0 if node i did not enter state s
    """

    # Did node i experience state s?
    # state_indication[s,i] = 1 if i did enter state s
    #                         0 if i did not enter state s
    state_indication = numpy.zeros((6,N))
    for i in range(N):

        state_count_S = numpy.count_nonzero(state[i,:] == iS)
        if state_count_S > 0:
            state_indication[0,i] = 1    

        state_count_E = numpy.count_nonzero(state[i,:] == iE)
        if state_count_E > 0:
            state_indication[1,i] = 1    

        state_count_IA = numpy.count_nonzero(state[i,:] == iIA)
        if state_count_IA > 0:
            state_indication[2,i] = 1  

        state_count_IS = numpy.count_nonzero(state[i,:] == iIS)
        if state_count_IS > 0:
            state_indication[3,i] = 1  

        state_count_R = numpy.count_nonzero(state[i,:] == iR)
        if state_count_R > 0:
            state_indication[4,i] = 1  
    
        state_count_C = numpy.count_nonzero(state[i,:] == iQ)
        if state_count_C > 0:
            state_indication[5,i] = 1  

    return state_indication

def location_indicator(N,
                       location, 
                       iT=1, iO=-1, 
                       iW=1, iH=-1,       
                       iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6):
    """
    Desciption
    ------------
    Determine whether node i experienced location l at least once.

    Parameters.
    -----------
    N: int
        Number of nodes
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state indices: ints
    
    Returns:
    ----------
    locaton_indication:
        location_indication[s,i] = 1 if node i did enter location l
                                   0 if node i did not enter location l
    """

    # Did node i experience location l?
    # location_indication[l,i] = 1 if i did enter location l
    #                            0 if i did not enter location l
    location_indication = numpy.zeros((2,N))
    for i in range(N):

        location_count_W = numpy.count_nonzero(location[i,:] == iW)
        if location_count_W > 0:
            location_indication[0,i] = 1    

        location_count_H = numpy.count_nonzero(location[i,:] == iH)
        if location_count_H > 0:
            location_indication[1,i] = 1    

    return location_indication

def location_and_state_counter(N,T,
                                location, state,
                                iT=1, iO=-1, 
                                iW=1, iH=-1,       
                                iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6):

    """
    Description
    ------------
    Determine the number of nodes in state s and location l at time t

    Parameters
    ------------
    N: int
        Number of nodes
    T: int
        Number of times
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    state indices: ints

    Returns:
        location_and_state_count[s,t,l] = Number of people in state s and location l at time t
    """
    # location_and_state[i,t] = location_and_state of i at t
    # Where location_and_state is a choice between 12 numbers (positive 0-5 for location-work and state and negative 0-5 for location-home and state)
    # location_and_state_count[s,t,l] = Number of people in state s and location l at time t 
    
    location_and_state = numpy.multiply(location, state)
    location_and_state_count = numpy.zeros((6,T,2)) 
    for t in range(T):
        location_and_state_count[0,t,0] = numpy.count_nonzero(location_and_state[:,t] == iS )
        location_and_state_count[1,t,0] = numpy.count_nonzero(location_and_state[:,t] == iE )
        location_and_state_count[2,t,0] = numpy.count_nonzero(location_and_state[:,t] == iIA)
        location_and_state_count[3,t,0] = numpy.count_nonzero(location_and_state[:,t] == iIS)
        location_and_state_count[4,t,0] = numpy.count_nonzero(location_and_state[:,t] == iR )
        location_and_state_count[5,t,0] = numpy.count_nonzero(location_and_state[:,t] == iQ )

        location_and_state_count[0,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iS )
        location_and_state_count[1,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iE )
        location_and_state_count[2,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iIA)
        location_and_state_count[3,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iIS)
        location_and_state_count[4,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iR )
        location_and_state_count[5,t,1] = numpy.count_nonzero(location_and_state[:,t] == -iQ )
    
    return location_and_state_count


def productivity_workforce(N,T,
                           location, state, 
                           prod_IllWork=0.2, prod_WellHome=0.7,
                           iT=1, iO=-1, 
                           iW=1, iH=-1,       
                           iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6):
    """
    Description
    -------------
    Determine the productivity of the workforce of N nodes at time t based on a given productivity measure.
    
    Parameters
    ------------
    N: int
        Number of nodes
    T: int
        Number of times
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    prod_IllWork: float
        The proportion of total productivity expected if a worker is ill at work - might depend for example on
        the type of work required.
    prod_Well: float
        The proportion of total productivity expected if a worker is well at home - might depend for example on
        the type of work required.
    state indices: ints

    Returns
    --------
    productivity_workforce[t] = total productivity of all N nodes at time t

    """

    #  productivity[i,t] = productivity of node i at time t
    #  productivity_workforce[t] = total productivity of all nodes at time t
    
    productivity = numpy.zeros((N,T))
    for t in range(T):
        for i in range(N): 

            if state[i,t] == iS:
                if location[i,t] == iW:
                    productivity[i,t] = 1
                elif location[i,t] == iH:
                    productivity[i,t] = prod_WellHome
                else:
                    raise Exception
            
            elif state[i,t] == iE:
                if location[i,t] == iW:
                    productivity[i,t] = 1
                elif location[i,t] == iH:
                    productivity[i,t] = prod_WellHome
                else:
                    raise Exception
            
            elif state[i,t] == iIA:
                if location[i,t] == iW:
                    productivity[i,t] = 1
                elif location[i,t] == iH:
                    productivity[i,t] = prod_WellHome
                else:
                    raise Exception
            
            elif state[i,t] == iIS:
                if location[i,t] == iW:
                    productivity[i,t] = prod_IllWork
                elif location[i,t] == iH:
                    productivity[i,t] = 0
                else:
                    raise Exception
            
            elif state[i,t] == iR:
                if location[i,t] == iW:
                    productivity[i,t] = 1
                elif location[i,t] == iH:
                    productivity[i,t] = prod_WellHome
                else:
                    raise Exception

            elif state[i,t] == iQ:
                if location[i,t] == iW:
                    productivity[i,t] = 1
                elif location[i,t] == iH:
                    productivity[i,t] = prod_WellHome
                else:
                    raise Exception

            else :
                raise Exception

    productivity_workforce = numpy.sum(a=productivity, axis=0)
    return productivity_workforce


def species_to_csv(i_species_run,               
                   species,
                   output_dir_path
                   ):
    """
    Description
    -----------
    Save species matrix to csv file.

    Parameters
    -------------
    i_species_run: int
        index of the species (outer) run
    species: numpy.ndarray
        species[i] = opacity of node i
    output_dir_path: string
        path of the directory to place the csv file in
    """
    species_csv_path = os.path.join(output_dir_path, "species_iSR-{}.csv".format(i_species_run))
    numpy.savetxt(fname=species_csv_path, X=species, fmt="%.4e", delimiter=',')


def state_location_to_csv(i_species_run,
                          i_state_location_run,              
                          location,
                          state,
                          output_dir_path
                          ):
    """
    Description
    -----------
    Save location matrix to csv file.

    Parameters
    -------------
    i_species_run: int
        index of the species (outer) run
    i_state_location_run: int
        index of model (inner) run
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    output_dir_path: string
        path of the directory to place the csv file in
    """
    location_csv_path = os.path.join(output_dir_path,"location_iSR-{}_iSLR-{}.csv".format(i_species_run,i_state_location_run)) 
    state_csv_path    = os.path.join(output_dir_path,"state_iSR-{}_iSLR-{}.csv".format(i_species_run,i_state_location_run)) 
    
    numpy.savetxt(fname=location_csv_path, X=location, fmt="%.4e", delimiter=',')
    numpy.savetxt(fname=state_csv_path,    X=state, fmt="%.4e", delimiter=',')
            
def csv_to_species(i_species_run,               
                   output_dir_path
                   ):
    """
    Description
    -----------
    USe saved species matrix in csv file as numpy.ndarray

    Parameters
    -------------
    i_species_run: int
        index of the species (outer) run
    output_dir_path: string
        path of the directory containing the csv file in

    Returns
    -------
    species: numpy.ndarray
        species[i] = opacity of node i
    """
    species_csv_path = os.path.join(output_dir_path, "species_iSR-{}.csv".format(i_species_run))
    species = numpy.loadtxt(fname=species_csv_path, dtype=float, delimiter=',')

    return species

def csv_to_state_location(i_species_run,
                          i_state_location_run,              
                          output_dir_path):
    """
    Description
    -----------
    Use saved state and location matrices in csv file as numpy.ndarray

    Parameters
    -------------
    i_species_run: int
        index of the species (outer) run
    i_state_location_run: int
        index of the state and location (inner) run
    output_dir_path: string
        path of the directory containing the csv file in

    Returns
    ---------
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    """
    location_csv_path = os.path.join(output_dir_path,"location_iSR-{}_iSLR-{}.csv".format(i_species_run,i_state_location_run)) 
    state_csv_path    = os.path.join(output_dir_path,"state_iSR-{}_iSLR-{}.csv".format(i_species_run,i_state_location_run)) 

    location = numpy.loadtxt(fname=location_csv_path, dtype=float,   delimiter=',')
    state    = numpy.loadtxt(fname=state_csv_path, dtype=float,   delimiter=',')

    return location, state
            
            

            
            


