#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# External modules
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import scipy
from scipy import interpolate
import argparse
import os
import json
import time
import numpy 

# Internal modules
import visualise
import topography
import models
import output
import utils

# -----------------------------------------------
# README
# use_MC-O_deg module: 
# Use output of the run_MC-O module to create plots with degree on the x-axis, found in V_deg folders.
# For example, this code was used to generate the plots in the V_deg folders:
#  ./figures/cold/V_deg
#  ./figures/hot/V_deg 

# -----------------------------------------------


start_time = time.time()
print("Started using data at relative time: {}\n".format(start_time - start_time))



# Create parser
parser = argparse.ArgumentParser(description="Parameter file paths")

# Add parameter_file_paths as parser requirement
# Add all parameter file paths
# - Each degree run is a separate parameter file
# - So to use more than one degree, add parameter file for each degree
# - (see ./tests/test_run_MC-O for correct directory structure)
parser.add_argument("-f", dest="parameter_file_paths", nargs="*",  required=True,
                    help="Input parameter file paths for simulation", metavar="FILE_LIST",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Return parser arguments
parser_args = parser.parse_args()
parameter_file_paths = parser_args.parameter_file_paths

# Define number of degrees (from number of parameter files in parser)
num_degs = len(parameter_file_paths)

# Number of statistics to analyse - 
# Here we look at:
# 0. ill_count_prop (defined below)
# 1. home_count_prop (defined below)
# 2. total_productivity_normalWorkplace_prop (defined below)
# 3. total_productivity_WFHWorkplace_prop (defined below)
# 4. total_productivity_NonWFHWorkplace_prop (defined below)
# 5. ill_indicator_workforce_prop (defined below)
# 6. home_indicator_workforce_prop (defined below)
num_stats = 7   

# Define empty lists to save each run (defined below)
average_stats_list = []
std_stats_list = []
degree_list = []
for i_deg in range(num_degs):

    # Try to open ith parameter file from parameter_file_paths returned by parser
    try:    
        # Read parameter_file and load contents as parameter_dict
        with open(parser_args.parameter_file_paths[i_deg], 'r') as parameter_file:
            parameters_dict = json.load(parameter_file)
    except Exception:
        raise
    
    # Add degree in current parameter file to degree list 
    # Since d is degree probability (prob of connection between two nodes), multiply by 100 to get degree
    # degre_list[i] = degree corresponding to ith parameter file.
    degree_list.append(parameters_dict["d"]*100)

    # Monte Carlo parameters
    # ---------------
    num_opacities    = parameters_dict["N"] + 1
    num_state_runs   = parameters_dict["num_state_runs"]

    #k_vals = [0, 50,  63]
    k_vals = [0, 50,  100]
    num_k_vals = len(k_vals)

    # Directories 
    parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_paths[i_deg]))
    print(parameters_dir_path)
    output_dir_path = os.path.join(parameters_dir_path, 'output')  


    

    # Run Monte Carlo
    # ------------
    # average_stats[st, k, d] = average of statistic st (avd over state runs) at opacity k at degree d
    average_stats = numpy.zeros((num_stats, num_k_vals))
    std_stats     = numpy.zeros((num_stats, num_k_vals))
    for i_k in range(num_k_vals):
        print("Using species run index k:", k_vals[i_k], "of", num_k_vals-1)

        # Define the opacity run/species run index
        k = k_vals[i_k]

        # Define species (Reset from last species run)
        species = output.csv_to_species(i_species_run=k, output_dir_path=output_dir_path)

        # stats[st,d,r] = statistic st at degree d on rth model run
        stats = numpy.zeros((num_stats, num_degs, num_state_runs))
        # r indexes the model run (inner loop)
        for r in range(num_state_runs):

            # Initialise location and state (reset state and location from last run)
            # ---------------
            state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
            location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time

            # Calculate location and state of i at t+1 given information about i at t
            location, state = output.csv_to_state_location(i_species_run=k, i_state_location_run=r, output_dir_path=output_dir_path)


            # Calculate stats
            # ----------------
            # -------------------------------------------------------------------------------------------------
            # Define stats

            # location_and_state_count[s,t,l] = Number of people in state s and location l at time t 
            location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                                         location, state,
                                                                         iT=1, iO=-1, 
                                                                         iW=1, iH=-1,       
                                                                         iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            # location_and_state_count_prop[s,t,l] = proportion of nodes in state s and location l at time t
            location_and_state_count_prop = location_and_state_count/parameters_dict["N"]

            # Record stats
            ill_at_location_count_prop = location_and_state_count_prop[parameters_dict["iE"] -1,:,:] + \
                                         location_and_state_count_prop[parameters_dict["iIA"]-1,:,:] + \
                                         location_and_state_count_prop[parameters_dict["iIS"]-1,:,:]
            ill_count_prop     = numpy.sum(a=ill_at_location_count_prop, axis=1)
            # max number of nodes ever ill at once
            max_ill_count_prop = numpy.amax(a=ill_count_prop)

            home_at_state_count_prop = location_and_state_count_prop[:,:,1]
            home_count_prop          = numpy.sum(home_at_state_count_prop, axis=0) 
            # max number of nodes ever ill at once
            max_home_count_prop      = numpy.amax(home_count_prop)

            stats[0,i_deg,r] = max_ill_count_prop
            stats[1,i_deg,r] = max_home_count_prop

            # --------------------------------------------------------------------------------------------------        
            # Define stats

            # productivity_workforce_typeWorkplace[t] = productivity of workforce at time t (with type of workplace productivity measure)
            productivity_workforce_normalWorkplace = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=0.2, prod_WellHome=0.7,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            productivity_workforce_WFHWorkplace    = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=0, prod_WellHome=1,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            productivity_workforce_NonWFHWorkplace = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=1, prod_WellHome=0,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            # productivity_workforce_prop[t] = productivity proportion of the workforce (per total possible productivity which is 1 for all) (for type of workplace)
            productivity_workforce_normalWorkplace_prop = productivity_workforce_normalWorkplace/parameters_dict["N"]
            productivity_workforce_WFHWorkplace_prop    = productivity_workforce_WFHWorkplace/parameters_dict["N"]
            productivity_workforce_NonWFHWorkplace_prop = productivity_workforce_NonWFHWorkplace/parameters_dict["N"]

            # Record stats
            total_productivity_normalWorkplace_prop = numpy.sum(a=productivity_workforce_normalWorkplace_prop, axis=0)/parameters_dict["T"]
            total_productivity_WFHWorkplace_prop    = numpy.sum(a=productivity_workforce_WFHWorkplace_prop, axis=0)/parameters_dict["T"]
            total_productivity_NonWFHWorkplace_prop = numpy.sum(a=productivity_workforce_NonWFHWorkplace_prop, axis=0)/parameters_dict["T"]

            stats[2,i_deg,r] = total_productivity_normalWorkplace_prop
            stats[3,i_deg,r] = total_productivity_WFHWorkplace_prop
            stats[4,i_deg,r] = total_productivity_NonWFHWorkplace_prop

            # ------------------------------------------------------------------------------------------------
            # Define stats


            # state_indicator[s,i] = indicates whether node i felt state s
            state_indicator    = output.state_indicator(N=parameters_dict["N"],
                                                        state=state,
                                                        iT=1, iO=-1, 
                                                        iW=1, iH=-1,       
                                                        iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6
                                                        )
            # location_indicator[l,i] = indicates whether node i felt location l                                            
            location_indicator = output.location_indicator(N=parameters_dict["N"],
                                                           location=location,
                                                           iT=1, iO=-1, 
                                                           iW=1, iH=-1,       
                                                           iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6
                                                           )
            # state_indicator_workforce[s] = number of nodes that felt state s
            state_indicator_workforce = numpy.sum(a=state_indicator, axis=1)

            # location_indicator_workforce[l] = number of nodes that felt location l 
            location_indicator_workforce = numpy.sum(a=location_indicator, axis=1)

            # state_indicator_workforce_prop[s] = proportion of nodes that felt state s
            state_indicator_workforce_prop = state_indicator_workforce/parameters_dict["N"]

            # location_indicator_workforce_prop[l] = proportion of nodes that felt location l
            location_indicator_workforce_prop = location_indicator_workforce/parameters_dict["N"]

            # ill_indicator_workforce_prop = proportion of nodes that felt state E, iIA, iIS
            ill_indicator_workforce_prop = state_indicator_workforce_prop[parameters_dict["iE"]-1] 
            # home_indicator_prop = proportion of nodes that felt location H
            home_indicator_workforce_prop = location_indicator_workforce_prop[1]

            # Record stats
            stats[5,i_deg,r] = ill_indicator_workforce_prop
            stats[6,i_deg,r] = home_indicator_workforce_prop


            # -------------------------------------------------------------------------------------------------
            # -------------------------------------------------------------------------------------------------
        # Take the mean of the stats over all model runs
        ill_count_prop_mean                          = numpy.mean(a=stats[0,i_deg,:], axis=-1)
        home_count_prop_mean                         = numpy.mean(a=stats[1,i_deg,:], axis=-1)
        total_productivity_normalWorkplace_prop_mean = numpy.mean(a=stats[2,i_deg,:], axis=-1)
        total_productivity_WFHWorkplace_prop_mean    = numpy.mean(a=stats[3,i_deg,:], axis=-1)
        total_productivity_NonWFHWorkplace_prop_mean = numpy.mean(a=stats[4,i_deg,:], axis=-1)
        ill_indicator_workforce_prop_mean            = numpy.mean(a=stats[5,i_deg,:], axis=-1)
        home_indicator_workforce_prop_mean           = numpy.mean(a=stats[6,i_deg,:], axis=-1)

        # Add mean stats to average_stats
        # average_stats[st,i_k] = average (over model runs) of statitic st at opacity with index k
        average_stats[0,i_k] = ill_count_prop_mean
        average_stats[1,i_k] = home_count_prop_mean
        average_stats[2,i_k] = total_productivity_normalWorkplace_prop_mean
        average_stats[3,i_k] = total_productivity_WFHWorkplace_prop_mean
        average_stats[4,i_k] = total_productivity_NonWFHWorkplace_prop_mean
        average_stats[5,i_k] = ill_indicator_workforce_prop_mean
        average_stats[6,i_k] = home_indicator_workforce_prop_mean

        # ------------------------------------------------------------------------------------------------------------
        # Calculate the standardd deviation of the stats over all model runs (used for error bars)

        # std_stats[st,i_k] = standard deviation (over model runs) of statitic st at opacity with index k
        std_stats[0,i_k] = numpy.std(a=stats[0,i_deg,:], axis=-1)
        std_stats[1,i_k] = numpy.std(a=stats[1,i_deg,:], axis=-1)
        std_stats[2,i_k] = numpy.std(a=stats[2,i_deg,:], axis=-1)
        std_stats[3,i_k] = numpy.std(a=stats[3,i_deg,:], axis=-1)
        std_stats[4,i_k] = numpy.std(a=stats[4,i_deg,:], axis=-1)
        std_stats[5,i_k] = numpy.std(a=stats[5,i_deg,:], axis=-1)
        std_stats[6,i_k] = numpy.std(a=stats[6,i_deg,:], axis=-1)
               
        # ------------------------------------------------------------------------------------------------------------
    # average_stats_list[i_deg, st, i_k] = mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
    average_stats_list.append(100*average_stats)
    # std_stats_list[i_deg, st, i_k] = standard dev (over model runs) of statitic st at opacity with index k at degree at index i_deg
    std_stats_list.append(100*std_stats)
    # Note: Stats are proportions (0-1) - multiplied by 100 to convert to percentage

# Convert list to array
# average_stats_array[i_deg, st, i_k] = mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
average_stats_array = numpy.asarray(average_stats_list) 
# std_stats_array[i_deg, st, i_k] = standard dev (over model runs) of statitic st at opacity with index k at degree at index i_deg
std_stats_array = numpy.asarray(std_stats_list) 

# lower_stats_array[i_deg, st, i_k] = 1 standard dev below mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
# higher_stats_array[i_deg, st, i_k] = 1 standard dev above mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
lower_stats_array = average_stats_array - std_stats_array
upper_stats_array = average_stats_array + std_stats_array


# Plotting
# -------------------
opacities = ["0%","50%","100%"]
linstyles = ["--", "-.","-"]
alphas    = [0.2, 0.4, 0.6]


# If 'paper' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path,"./../" , 'paper')) == False:
    os.mkdir(os.path.join(parameters_dir_path, "./../",'paper'))
# Define paper file path
paper_dir_path = os.path.join(parameters_dir_path, "./../",  'paper')

# If 'V_Degree' directory does not exist in paper directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(paper_dir_path , 'V_Degree')) == False:
    os.mkdir(os.path.join(paper_dir_path,'V_Degree'))
# Define paper file path
V_Degree_dir_path = os.path.join(paper_dir_path, 'V_Degree')



# -------------------------------------------------------------------------------------------------------------------

# Plot: StateCount_V_Degree
# Max number of people that were ever ill at any one time as a function of degree
# Max number of people that were ever home at any one time as a function of degree
fig,gs,ax = visualise.plot_single_frame()  

stateCount_labels = ["Ill", "Home"]
stateCount_colors = ["red", "navy"]

for i_k in range(num_k_vals):
    for i in range(2):
        i_stat=i
        ax.plot(degree_list[:], average_stats_array[:,i_stat,i_k], ls=linstyles[i_k], color=stateCount_colors[i], label=r"$s$={}, $o$={}".format(stateCount_labels[i], opacities[i_k]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
ax.set_ylim(-0.0,60 + 0.00)
#ax.set_xlim(-0.0,parameters_dict["T"] + 70)
#ax.legend(bbox_to_anchor=(1.04,1),loc="upper left")
#ax.legend(loc="best")
visualise.style_axes(ax, r"Average connectivity",  r"% workforce")
#visualise.save_svg(fig, gs, os.path.join(paper_dir_path, '{}_Stat_V_Opacity.svg'.format(parameters_dict["simulation_name"], k) ), rect=[0,0,1,1] )
visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, 'StateCount_V_Degree.svg'))




# Plot: Prod_V_Degree
# Average (over time) productivity of the workforce as a function of degree (by three measures of productivity)
fig,gs,ax = visualise.plot_single_frame()

prod_labels = ["average", "wfh"  , "non wfh"]
prod_colors = ["orange" , "green", "red" ]

for i_k in range(num_k_vals):
    for i in range(3):
        i_stat=i+2
        ax.plot(degree_list[:], 100*numpy.ones(numpy.shape(average_stats_array[:,i_stat,i_k])) - average_stats_array[:,i_stat,i_k], ls=linstyles[i_k], color=prod_colors[i], label=r"workplace type={}, $o$={}".format(prod_labels[i], opacities[i_k]))

visualise.style_axes(ax, r"Average connectivity",  r"% productivity deficit")
visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, 'Prod_V_Degree.svg'))



# Plot: StateIndicate_V_Degree
# Proportion of nodes that were ever ill as a function of  degree
# Proportion of nodes that were ever home as a function of  degree
fig,gs,ax = visualise.plot_single_frame()

stateIndicate_labels = ["Ill", "Home"]
stateIndicate_colors = ["red", "navy"]

for i_k in range(num_k_vals):
    for i in range(2):
        i_stat=i+5
        ax.plot(degree_list[:], average_stats_array[:,i_stat,i_k], ls=linstyles[i_k], color=stateIndicate_colors[i], label=r"$s$={}, $o$={}".format(stateIndicate_labels[i], opacities[i_k]))

ax.set_ylim(-0.0,100 + 0.00)

visualise.style_axes(ax, r"Average connectivity",  r"% workforce")
visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, 'StateIndicate_V_Degree.svg'))

# -----------------------------------------------------------------------------------------------------------------
# Repeat above plots but with each line on a separate plot and one standard deviation above and below shaded.


# Plot: StateCount_V_Degree each stat seperately with error

stateCount_labels = ["Ill", "Home"]
stateCount_colors = ["red", "navy"]
colors = ["green", "orange", "red"] # these colors represent the opacity in each plot - green is 0%, orange is 50%, and red is 100%

for i in range(2):
    i_stat=i
    fig,gs,ax = visualise.plot_single_frame()
    for i_k in range(num_k_vals):
        ax.plot(degree_list[:], average_stats_array[:,i_stat,i_k], ls="-",  color=colors[i_k], label=r"$s$={}, $o$={}".format(stateCount_labels[i], opacities[i_k]))
        ax.plot(degree_list[:], lower_stats_array[:,i_stat,i_k],   ls="--", color=colors[i_k])
        ax.plot(degree_list[:], upper_stats_array[:,i_stat,i_k],   ls="--", color=colors[i_k])

    ax.set_ylim(-0.0,60 + 0.00)
    visualise.style_axes(ax, r"Average connectivity",  r"% workforce")
    visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, "StateCount_V_Degree_Error_{}.svg".format(stateCount_labels[i])))




# Plot: Prod_V_Degree each state separately with error

prod_labels = ["average", "wfh"  , "non wfh"]
prod_colors = ["orange" , "green", "red" ]
colors = ["green", "orange", "red"] # these colors represent the opacity in each plot - green is 0%, orange is 50%, and red is 100%

for i in range(3):
    i_stat=i+2
    fig,gs,ax = visualise.plot_single_frame()
    for i_k in range(num_k_vals):
        ax.plot(degree_list[:], 100*numpy.ones(numpy.shape(average_stats_array[:,i_stat,i_k])) - average_stats_array[:,i_stat,i_k],                                 ls="-", color=colors[i_k], label=r"workplace type={}, $o$={}".format(prod_labels[i], opacities[i_k]))
        ax.plot(degree_list[:], 100*numpy.ones(numpy.shape(average_stats_array[:,i_stat,i_k])) - average_stats_array[:,i_stat,i_k] - std_stats_array[:,i_stat,i_k], ls="--", color=colors[i_k])
        ax.plot(degree_list[:], 100*numpy.ones(numpy.shape(average_stats_array[:,i_stat,i_k])) - average_stats_array[:,i_stat,i_k] + std_stats_array[:,i_stat,i_k], ls="--", color=colors[i_k])

    visualise.style_axes(ax, r"Average connectivity",  r"% productivity deficit")
    visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, 'Prod_V_Degree_Error_{}.svg'.format(prod_labels[i])))


# Plot: StateIndicate_V_Degree

stateIndicate_labels = ["Ill", "Home"]
stateIndicate_colors = ["red", "navy"]
colors = ["green", "orange", "red"] # these colors represent the opacity in each plot - green is 0%, orange is 50%, and red is 100%

for i in range(2):
    i_stat=i+5
    fig,gs,ax = visualise.plot_single_frame()
    for i_k in range(num_k_vals):
        ax.plot(degree_list[:], average_stats_array[:,i_stat,i_k], ls="-", color=colors[i_k], label=r"$s$={}, $o$={}".format(stateIndicate_labels[i], opacities[i_k]))
        ax.plot(degree_list[:], lower_stats_array[:,i_stat,i_k],   ls="--", color=colors[i_k])
        ax.plot(degree_list[:], upper_stats_array[:,i_stat,i_k],   ls="--", color=colors[i_k])

    ax.set_ylim(-0.0,100 + 0.00)

    visualise.style_axes(ax, r"Average connectivity",  r"% workforce")
    visualise.save_svg(fig, gs, os.path.join(V_Degree_dir_path, 'StateIndicate_V_Degree_Error_{}.svg'.format(stateIndicate_labels[i])))

print("\nFinished using data at relative time:  {}".format(time.time() - start_time))