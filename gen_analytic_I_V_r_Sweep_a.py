#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy
import matplotlib
from matplotlib import pyplot as plt
import visualise
import os
# Parameters
#a_vals = numpy.linspace(0,100,5)
a_vals = [0.0, 0.01, 0.1, 1]
linestyles = ["-","-.","--",":"]
r_0 = numpy.linspace(0.0001,10,101)

one = numpy.ones(r_0.shape)

I_aIsZero = one  - numpy.divide(one,r_0)
I_aIsZero_2 = numpy.divide(r_0 - one,r_0)


fig,gs,ax = visualise.plot_single_frame()

#ax.plot(r_0,I_aIsZero, label="a=0 true")
#ax.plot(r_0,I_aIsZero_2, label="a is zero_2")

for i in range(len(a_vals)):
    a = a_vals[i]*numpy.ones(r_0.shape)

    I_num_1 =  (r_0 - one - a)
    I_num_2_insideRoot =  numpy.add( numpy.power( I_num_1 , 2),  4*numpy.multiply(a,r_0) ) 
    I_num_2 = numpy.sqrt(I_num_2_insideRoot)

    I_den   =  2*r_0

    I = numpy.add( numpy.divide(I_num_1,I_den),   numpy.divide(I_num_2,I_den) )

    ax.plot(r_0, I, label="$a={}$".format(a_vals[i]), ls=linestyles[i])
    #ax.plot(r_0, I_num_2_insideRoot)
    #ax.plot(r_0, I_num_2)

ax.legend()
ax.set_ylim(-0.03,1.03)
ax.set_xlim(-0.3,10.3)
ax.set_xticks([0,2,4,6,8,10])
visualise.style_axes(ax, r"$r_0$",  r"$I_0^*$")

cwd = os.getcwd()
visualise.save_svg(fig, gs, os.path.join(cwd, "../simulations/I_V_r.svg") )

plt.show()

