#! /usr/bin/env python3
# -*- coding: utf-8 -*-

def define_parameters(parameters_dict):
    """
    Notes
    ------
    Generates global parameters for the model with a status triple for each node.
    """
    
    # Parameters
    # ---------------
    global  N, \
            T, \
            t_incubation, \
            t_asymptomatic, \
            t_symptomatic, \
            t_quarantined, \
            p_unlucky, \
            p_spread, \
            p_neverUnwell, \
            p_recovery, \
            p_OAdviseOthers, \
            p_OFollowAdvice, \
            iT, \
            iO, \
            iW, \
            iH, \
            iS, \
            iE, \
            iIA, \
            iIS, \
            iR, \
            iQ
    
    N                  = parameters_dict["N"]                # Population (number of nodes)
    T                  = parameters_dict["T"]                # Number of times [0,...,T-1]    
    t_incubation       = parameters_dict["t_incubation"]     # Delay between Exposed and Infectious.
    t_asymptomatic     = parameters_dict["t_asymptomatic"]   # Delay between Infectious and AbsentUnwell (for Truers)
    t_symptomatic      = parameters_dict["t_symptomatic"]    # Delay between Infectious and Susceptible (for liars)
    t_quarantined      = parameters_dict["t_quarantined"]    # Delay between Absent and Susceptible. (need d_isolation > t_symptomatic for realistic)
    p_unlucky          = parameters_dict["p_unlucky"]        # prob that I get it from someone outside network (e.g. public transport)
    p_spread           = parameters_dict["p_spread"]         # prob that if neighbour Infectious, I become Exposed (next day).
    p_neverUnwell      = parameters_dict["p_neverUnwell"]
    p_OAdviseOthers    = parameters_dict["p_OAdviseOthers"]  # prob that liar becomes Absent when neighbour is Infectious
    p_OFollowAdvice    = parameters_dict["p_OFollowAdvice"]  # prob that liar becomes Absent when they are Infectious
    p_recovery         = parameters_dict["p_recovery"]       # prob that: Truer becomes Recovered (immune) after being AU for t_symptomatic or Liar becomes Recovered afte being I for t_asymptomatic + d_symptomatuc 
    iT                 = parameters_dict["iT"]
    iO                 = parameters_dict["iO"]
    iW                 = parameters_dict["iW"]
    iH                 = parameters_dict["iH"]
    iS                 = parameters_dict["iS"]
    iE                 = parameters_dict["iE"]
    iIA                = parameters_dict["iIA"]
    iIS                = parameters_dict["iIS"]
    iR                 = parameters_dict["iR"]
    iQ                 = parameters_dict["iQ"]