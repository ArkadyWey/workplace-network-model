#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Internal modules
import visualise
import topography
import models
import output
import parameters
import utils

start_time = time.time()
print("Started using data at relative time: {}\n".format(start_time - start_time))

# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")
parser.add_argument("-f", dest="parameter_file_path", required=True,
                    help="Input parameter file path for simulation", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

parser.add_argument("-k", dest="species_run", required=True,
                    help="Input opacity index k", metavar="N",
                    type=int)

# Try to open parameter file at parameter_file_path returned by parser
try: 
    parser_args = parser.parse_args()
    # Read parameter_file and load contents as parameter_dict
    with open(parser_args.parameter_file_path, 'r') as parameter_file:
        parameters_dict = json.load(parameter_file)
except Exception:
    raise

# Define parmaeters directory 
parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_path))

# Define output file path
output_dir_path = os.path.join(parameters_dir_path, 'output')

# If 'figures' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path, 'figures')) == False:
    os.mkdir(os.path.join(parameters_dir_path, 'figures'))

# Define figures file path
figures_dir_path = os.path.join(parameters_dir_path, 'figures')

# Monte Carlo parameters
# ---------------
num_opacities    = parameters_dict["N"] + 1
num_species_runs = int(parameters_dict["N"]/5) + 1 # because first no liars, then 1, ..., then N
num_state_runs   = parameters_dict["num_state_runs"]


# Choices
# -------------

# Make a particular opacity choice (species run) to view data
k = parser_args.species_run

# Main
# --------------   
state_indication_workforce_prop_runs = numpy.zeros((6,num_state_runs))
location_and_state_count_prop_runs   = numpy.zeros((6,parameters_dict["T"], 2, num_state_runs))
productivity_workforce_prop_runs     = numpy.zeros((parameters_dict["T"],num_state_runs))
for r in range(num_state_runs):
    
    # Initialise location and state (reset state and location from last run)
    # ---------------
    state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
    location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time
    
    # Calculate location and state of i at t+1 given information about i at t
    location, state = output.csv_to_state_location(i_species_run=k,i_state_location_run=r,output_dir_path=output_dir_path)
    i=0
    t=0
# -------------------------------------------------------------------------------------------------

    # state_indication[s,i] = 1 if i did enter state s at time t
    #                         0 if i did not enter state s at any time                     
    state_indication = output.state_indicator(parameters_dict["N"],
                                        state,
                                        iT=1, iO=-1, 
                                        iW=1, iH=-1,       
                                        iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
    
    # state_indication_workforce[s] = number of nodes that experienced state s at least once.
    state_indication_workforce = numpy.sum(a=state_indication, axis=1)
    
    # state_indication_workforce_prop[s] = proportion of nodes that experienced state s at least once.
    state_indication_workforce_prop = state_indication_workforce/parameters_dict["N"]

    # Record
    state_indication_workforce_prop_runs[:,r] = state_indication_workforce_prop
# -------------------------------------------------------------------------------------------------
  
    # location_and_state_count[s,t,l] = Number of people in state s and location l at time t 
    location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                                 location, state,
                                                                 iT=1, iO=-1, 
                                                                 iW=1, iH=-1,       
                                                                 iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
    # location_and_state_count_prop[s,t,l] = proportion of nodes in state s and location l at time t
    location_and_state_count_prop = location_and_state_count/parameters_dict["N"]

    # Record 
    location_and_state_count_prop_runs[:,:,:,r] = location_and_state_count_prop
# --------------------------------------------------------------------------------------------------        

    # productivity_workforce[t] = productivity of workforce at time t
    productivity_workforce = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                           location=location, state=state,
                                                           iT=1, iO=-1, 
                                                           iW=1, iH=-1,       
                                                           iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
    # productivity_workforce_prop[t] = productivity proportion of the workforce (per total possible productivity which is 1 for all)
    productivity_workforce_prop = productivity_workforce/parameters_dict["N"]

    # Record
    productivity_workforce_prop_runs[:,r] = productivity_workforce_prop
# -----------------------------------------------------------------------------------------------------

state_indication_workforce_prop_mean = numpy.mean(a=state_indication_workforce_prop_runs, axis=-1)
location_and_state_count_prop_mean   = numpy.mean(a=location_and_state_count_prop_runs, axis=-1)
productivity_workforce_prop_mean     = numpy.mean(a=productivity_workforce_prop_runs, axis=-1)

# Plotting
# -------------------
state_names            = ["S", \
                          "E", \
                          "A", \
                          "U", \
                          "R", \
                          "Q"]

colors      = ['green','orange','red', 'tab:pink', 'black', 'blue']

# Plot: state_indication_workforce_prop_mean (prop of people who entered state)
fig,gs,ax = visualise.plot_single_frame(plot_grid=False)

ax.bar(state_names, state_indication_workforce_prop_mean*100, width=1, align='center', bottom=0, edgecolor='white', fill=True, ls='-', color=colors, alpha=1.0)

ax.set_ylim(-0.0,100 + 0.00)
#ax.set_xticks(numpy.arange(0,T+1,1))
visualise.style_axes(ax, r"State ($x$)",  r"% nodes that entered state $x$") 
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_Indicator_V_State_k-{}.svg'.format(parameters_dict["simulation_name"], k) ) )





# Plot: location_and_state_count_prop_mean[s,t,l] (prop people in each state)
fig,gs,ax = visualise.plot_single_frame()

for s in range(6):
    ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*location_and_state_count_prop_mean[s,:,0], ls="-", color=colors[s], label=r"$x={}, l=W$".format(state_names[s]))
    ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*location_and_state_count_prop_mean[s,:,1], ls="--", color=colors[s], label=r"$x={}, l=H$".format(state_names[s]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
ax.set_ylim(-0.0,100 + 0.00)
#ax.set_xlim(-0.0,parameters_dict["T"] + 70)
ax.legend(bbox_to_anchor=(1.04,1),loc="upper left")
visualise.style_axes(ax, r"Time",  r"% nodes in state $x$ at location $l$")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_StateLocation_V_Time_k-{}.svg'.format(parameters_dict["simulation_name"], k) ), rect=[0,0,1,1] )









# Plot: location_and_state_count_reduced (prop people in each state)
fig,gs,ax = visualise.plot_single_frame()


# sick_count_prop_mean[t] = number of people sick (E, IA, IS) at time t
sick_count_prop_mean = location_and_state_count_prop_mean[parameters_dict["iE"]-1,:,0] + \
                       location_and_state_count_prop_mean[parameters_dict["iE"]-1,:,1] + \
                       location_and_state_count_prop_mean[parameters_dict["iIA"]-1,:,0] + \
                       location_and_state_count_prop_mean[parameters_dict["iIA"]-1,:,1] + \
                       location_and_state_count_prop_mean[parameters_dict["iIS"]-1,:,0] + \
                       location_and_state_count_prop_mean[parameters_dict["iIS"]-1,:,1]

# healthy_count_prop_mean[t] = number of people healthy (S, R, Q) at time t
healthy_count_prop_mean = location_and_state_count_prop_mean[parameters_dict["iS"]-1,:,0] + \
                          location_and_state_count_prop_mean[parameters_dict["iS"]-1,:,1] + \
                          location_and_state_count_prop_mean[parameters_dict["iR"]-1,:,0] + \
                          location_and_state_count_prop_mean[parameters_dict["iR"]-1,:,1] + \
                          location_and_state_count_prop_mean[parameters_dict["iQ"]-1,:,0] + \
                          location_and_state_count_prop_mean[parameters_dict["iQ"]-1,:,1]

# work_count_prop_mean[t] = number of people in work at time t
work_count_prop_mean = numpy.sum(a=location_and_state_count_prop_mean[:,:,0], axis=0)
# home_count_prop_mean[t] = number of people at home at time t
home_count_prop_mean = numpy.sum(a=location_and_state_count_prop_mean[:,:,1], axis=0)

ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*healthy_count_prop_mean[:], ls="-", color="green", label=r"$s={}$".format("healthy"))
ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*sick_count_prop_mean[:], ls="-", color="red", label=r"$s={}$".format("ill"))
ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*work_count_prop_mean[:], ls="--", color="green", label=r"$s={}$".format("work"))
ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*home_count_prop_mean[:], ls="--", color="red", label=r"$s={}$".format("home"))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
ax.set_ylim(-0.0,100 + 0.00)
#ax.set_xlim(-0.0,parameters_dict["T"] + 70)
ax.legend(loc="center right")
visualise.style_axes(ax, r"Time",  r"% nodes with status $s$")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_ReducedStateLocation_V_Time_k-{}.svg'.format(parameters_dict["simulation_name"], k) ))







# Plot: productivity_workforce_prop_mean 
fig,gs,ax = visualise.plot_single_frame()

ax.plot(numpy.linspace(0,parameters_dict["T"],parameters_dict["T"]), 100*productivity_workforce_prop_mean[:], ls="-", color="black")

ax.set_ylim(-0.0,100 + 0.00)
visualise.style_axes(ax, r"Time",  r"% productivity of nodes")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_Productivity_V_Time_k-{}.svg'.format(parameters_dict["simulation_name"], k) ) )

print("\nFinished using data at relative time:  {}".format(time.time() - start_time))

