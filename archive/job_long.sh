# !/bin/bash

cwd=`pwd`
run="run_MC-O.py"
flag=f

for d in 0.010 0.015 0.020 0.025 0.030 0.035 0.040 0.045 0.050; do
    arg="${cwd}/../local/to/long_T-253_NSR-100/Sim-MC-O_d-${d}/parameters.json"
    ./${run} -${flag} ${arg}
done

