#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os

# Internal modules
import visualise
import topography
import models
import output

simulation_name = "monte_carlo_topology"
parameters_dir_path = "./../simulations/presentation/monte_carlo_topology"

# Parameters
# ---------------
N       = 8                # Population (number of nodes)
T       = 26                # Number of times [0,...,T-1]
parameters_dict["T - 1 = T-1

t_incubation   = 2           # Delay between Exposed and Infectious.
t_asymptomatic = 2           # Delay between Infectious and AbsentUnwell (for Truers)
t_symptomatic  = 3           # Delay between Infectious and Susceptible (for liars)
d_isolation    = 4           # Delay between Absent and Susceptible. (need d_isolation > t_symptomatic for realistic)

p_unlucky          = 0.01   # prob that I get it from someone outside network (e.g. public transport)
p_spread           = 0.50    # prob that if neighbour Infectious, I become Exposed (next day).

p_unlikelyHero     = 0.0    # prob that liar becomes Absent when neighbour is Infectious
p_guiltyConscience = 0.0    # prob that liar becomes Absent when they are Infectious

# Readable Indices
# ---------------
iT = 1
iO = -1

iS = 1
iE = 2
iI = 3
iA = 4
iAW = 5

# Monte Carlo parameters
# ---------------
num_network_runs = 11
num_state_runs   = 100
num_key_outputs  = 4

# Initial conditions
# ---------------
state_initial    = iS*numpy.ones(N) # assigned states 1,2,3,4  (S, E, I, A -- Susceptible, Exposed, Infectious, Absent)
state_initial[0] = iE

species          = iT*numpy.ones(N) # assigned types 1 or -1 (T, L -- Truer or Liar)
species[0]       = iO



# Initialise
# ---------------
state         = numpy.zeros((N,T)) # Each column is state at given time
state[:,0]    = state_initial


# Main
# ---------------
prob_of_numbers = [[1.0,0.0],[0.9,0.1],[0.8,0.2],[0.7,0.3],[0.6,0.4],[0.5, 0.5],[0.4, 0.6],[0.3, 0.7],[0.2, 0.8],[0.1, 0.9],[0.0, 1.0],]
key_outputs_averages = numpy.zeros((num_key_outputs, num_network_runs))
for k in range(num_network_runs):
    print(k)
    A = topography.specify_prob(N, numbers_list=[0,1], prob_of_number=prob_of_numbers[k])

    key_outputs = numpy.zeros((num_key_outputs, num_state_runs)) # i,j is the ith key output on the rth run
    for r in range(num_state_runs):
        # Calculate state
        state      = numpy.zeros((N,T)) # Each column is state at given time
        state[:,0] = state_initial
        for t in range(T-1): # T-1 because the t+1th time is given by the tth time.
            for i in range(N):
                state[i,t+1] = models.actions_3(state, i, t,
                                                   A, species,
                                                   N,T,
                                                   t_incubation, t_asymptomatic, d_isolation, t_symptomatic,
                                                   p_unlucky, p_spread,
                                                   p_unlikelyHero, p_guiltyConscience,       
                                                   iS, iE, iI, iA, iAW,
                                                   iT, iO,
                                                   )


        state_count      = output.state_counter(T,state, iS, iE, iI, iA, iAW)
        state_indication = output.state_indicator(N, state, iS, iE, iI, iA, iAW)

        # Number of time units spent in state s?
        state_count_total = numpy.sum(a=state_count, axis=1)
        # Proportion of time units spent in state s?
        state_count_proportion = state_count_total/(N*T)
        # How many people experienced state s?
        state_indication_total = numpy.sum(a=state_indication, axis=1)
        # What proportion experienced state s?
        state_indication_proportion = state_indication_total/N

        # Key outputs:
        # 1:
        # Proportion of people that got exposed at least once
        exposed_indication_proportion = state_indication_proportion[iE-1]
        key_outputs[0, r] = exposed_indication_proportion
        
        # 2:
        # Max proportion of workforce ill at any time
        ill_count =  state_count[iE-1,:] + state_count[iI-1,:] + state_count[iA-1,:]
        max_ill_count_proportion = max(ill_count/N)
        key_outputs[1, r] = max_ill_count_proportion

        # 3:
        # Proportion of time units spent ill 
        ill_count_proportion = state_count_proportion[iE-1] + state_count_proportion[iI-1] + state_count_proportion[iA-1]
        key_outputs[2, r] = ill_count_proportion

        # 4:
        # Proportion of time spent fully productive
        productivity_total = output.productivity(N,T,state, iS, iE, iI, iA, iAW)
        productivity_proportion = productivity_total/(N*T)
        key_outputs[3, r] = productivity_proportion

       
        # Proportion of people that were away at least once
        #away_indication_proportion    = state_indication_proportion[iA-1]
        #key_outputs[1, r] = away_indication_proportion
        
        # Proportion of time units spent away 
        #away_count_proportion      = state_count_proportion[iA-1]
        #key_outputs[3, r] = away_count_proportion

        state_count_check = numpy.sum(state_count_total)
        if state_count_check != N*T:
            print("state_count_check  :  \n{}".format(state_count_check) )
            print("state_count_check failed because total number of time units spent \
                   in all states by all nodes is not T*N")

    key_outputs_average = numpy.sum(key_outputs, axis=1)/num_state_runs # shape (num_key_outputs, )
    key_outputs_averages[:,k] = key_outputs_average


# Visualisation
# ----------

colors      = ['k','orange','r','g']
linestyles  = [':', '-.', '--', '-']
key_output_names = ["Prop. WF exposed at least once",\
                    "Max. prop. WF ill at any time",\
                    "Prop. time WF spent ill",\
                    "Prop. time WF spent productive"]

# Plot:
# Adjacency matrix
fig,gs,ax = visualise.plot_single_frame()
plt.spy(A)
visualise.save_png(fig, gs, os.path.join(parameters_dir_path, '{}_matrix'.format(simulation_name) ) )

# Plot:
# Network
node_indices = range(N)
node_identities = range(N)
labels = dict(zip(node_indices, node_identities))

fig,gs,ax = visualise.plot_single_frame()
visualise.show_graph_with_labels(A, labels)
visualise.save_png(fig, gs, os.path.join(parameters_dir_path, '{}_network'.format(simulation_name) ) )


# Plot:
# Key output averages V proportion of liars 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(num_key_outputs):
    ax.plot(numpy.linspace(0,1,11), key_outputs_averages[ko, :], c=colors[ko], ls=linestyles[ko], label="KO: {}".format(key_output_names[ko]))

ax.set_ylim(-0.01,1.8 + 0.01)
ax.set_yticks(numpy.linspace(0,1,6))
ax.legend(loc='upper left')
visualise.style_axes(ax, r"Prop. connectedness of WF ($-$) [-]",  r"Key output ($KO$) [-]")
visualise.save_png(fig, gs, os.path.join(parameters_dir_path, '{}_ko_V_prop_liars.svg'.format(simulation_name) ) )

