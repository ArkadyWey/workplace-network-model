#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Internal modules
import visualise
import topography
import models
import output
import parameters
import utils

start_time = time.time()
print("Started simulation at relative time: {}\n".format(start_time - start_time))

# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")
parser.add_argument("-f", dest="parameter_file_path", required=True,
                    help="Input parameter file path for simulation", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Try to open parameter file at parameter_file_path returned by parser
try: 
    parser_args = parser.parse_args()
    # Read parameter_file and load contents as parameter_dict
    with open(parser_args.parameter_file_path, 'r') as parameter_file:
        parameters_dict = json.load(parameter_file)
except Exception:
    raise

# Define parmaeters directory 
parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_path))

# If 'output' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path, 'output')) == False:
    os.mkdir(os.path.join(parameters_dir_path, 'output'))

# Define output file path
output_dir_path = os.path.join(parameters_dir_path, 'output')

# Monte Carlo parameters
# ---------------
num_opacities    = parameters_dict["N"] + 1 # because first no liars, then 1, ..., then N
#num_species_runs = parameters_dict["N"] + 1
#num_species_runs = int(parameters_dict["N"]/5) + 1
num_state_runs   = parameters_dict["num_state_runs"]

# Initial conditions
# -------------------
location_initial = parameters_dict["iW"]*numpy.ones(parameters_dict["N"])

state_initial      = parameters_dict["iS"]*numpy.ones(parameters_dict["N"]) # assigned states 1,2,3,4  (S, E, I, A -- Susceptible, Exposed, Infectious, Absent)
state_initial[0]   = parameters_dict["iE"]

# Opacities to test
species_set        = numpy.ones((parameters_dict["N"],num_opacities)) # Each column is a species vector

species_set_opaques      = parameters_dict["iO"]*numpy.ones((parameters_dict["N"],parameters_dict["N"]))
species_set_transparents = parameters_dict["iT"]*numpy.ones((parameters_dict["N"],parameters_dict["N"]))
liars_triu               = numpy.triu(m=species_set_opaques,k=0)
truers_tril              = numpy.tril(m=species_set_transparents,k=-1)
species_mix              = numpy.add(truers_tril, liars_triu)
species_set[:,1::]       = species_mix

k_vals = numpy.concatenate((numpy.linspace(0,50,51), numpy.linspace(52,100,13) ))
print(k_vals)
num_species_runs = len(k_vals)

# Main
# ---------------
num_key_outputs = 24

key_outputs_averages = numpy.zeros((num_key_outputs, num_species_runs))
for k in range(num_species_runs):
    print("species_run:", int(k_vals[k]), "of", num_opacities-1)
    
    # Define species (Reset from last species run)
    species = species_set[:,k]

    # Save species vector to file
    output.species_to_csv(i_species_run=k,
                          species=species,
                          output_dir_path=output_dir_path)

    key_outputs = numpy.zeros((num_key_outputs, num_state_runs)) # i,j is the ith key output on the rth run
    for r in range(num_state_runs):
        print("    state_location_run:", r, "of", num_state_runs-1, "(species run:", k, "of", num_opacities-1,")")

        # Define network
        A  = topography.specify_prob(parameters_dict["N"], numbers_list=[0,1], prob_of_number=[1.0-parameters_dict["d"], parameters_dict["d"]])
        
        # Initialise location and state (reset state and location from last run)
        # ---------------
        state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
        state[:,0]    = state_initial

        location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time
        location[:,0] = location_initial
        
        # Calculate location and state
        for t in range(parameters_dict["T"]-1): # T-1 because the t+1th time is given by the tth time.
            for i in range(parameters_dict["N"]):

                # Calculate location and state of i at t+1 given information about i at t
                location[i,t+1], state[i,t+1] =  models.triple_status_six_state(A=A, 
                                                                                N=parameters_dict["N"], 
                                                                                T=parameters_dict["T"],
                                                                                species=species, 
                                                                                location=location, 
                                                                                state=state,
                                                                                i=i, 
                                                                                t=t, 
                                                                                t_incubation=parameters_dict["t_incubation"], 
                                                                                t_asymptomatic=parameters_dict["t_asymptomatic"], 
                                                                                t_symptomatic=parameters_dict["t_symptomatic"], 
                                                                                t_quarantined=parameters_dict["t_quarantined"], 
                                                                                p_unlucky=parameters_dict["p_unlucky"], 
                                                                                p_spread=parameters_dict["p_spread"],
                                                                                p_neverUnwell=parameters_dict["p_neverUnwell"],
                                                                                p_recovery=parameters_dict["p_recovery"], 
                                                                                p_OAdviseOthers=parameters_dict["p_OAdviseOthers"], 
                                                                                p_OFollowAdvice=parameters_dict["p_OFollowAdvice"],
                                                                                iT=parameters_dict["iT"], 
                                                                                iO=parameters_dict["iO"], 
                                                                                iW=parameters_dict["iW"],
                                                                                iH=parameters_dict["iH"],       
                                                                                iS=parameters_dict["iS"], 
                                                                                iE=parameters_dict["iE"], 
                                                                                iIA=parameters_dict["iIA"], 
                                                                                iIS=parameters_dict["iIS"], 
                                                                                iR=parameters_dict["iR"], 
                                                                                iQ=parameters_dict["iQ"]
                                                                                )

        # Save calculated location and state matrix for all i and t to file
        output.state_location_to_csv(i_species_run=k,
                                     i_state_location_run=r,              
                                     location=location, 
                                     state=state,
                                     output_dir_path=output_dir_path
                                     )

        location_and_state_count = output.location_and_state_counter(N=parameters_dict["N"],T=parameters_dict["T"],
                                                                            location=location, state=state,
                                                                            iT=1, iO=-1, 
                                                                            iW=1, iH=-1,       
                                                                            iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)


        productivity_total = output.productivity_total(N=parameters_dict["N"],T=parameters_dict["T"],
                                            location=location, state=state,
                                            iT=1, iO=-1, 
                                            iW=1, iH=-1,       
                                            iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)

        state_indication = output.state_indicator(N=parameters_dict["N"],
                                                    state=state,
                                                    iT=1, iO=-1, 
                                                    iW=1, iH=-1,       
                                                    iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)

        # location_and_state_total[s,l] = total number of time units spent in state s and location l?
        location_and_state_count_total = numpy.sum(a=location_and_state_count, axis=1)

        # location_and_state_proportion[s,l] = proportion of time spent in state s and location l.
        location_and_state_proportion = location_and_state_count_total/(parameters_dict["N"]*parameters_dict["T"])


        # state_indication_total[s] = number of nodes that experienced state s at least once.
        state_indication_total = numpy.sum(a=state_indication, axis=1)
        
        # state_indication_proportion[s] = proportion of nodes that experienced state s at least once.
        state_indication_proportion = state_indication_total/parameters_dict["N"]

        # Key outputs:
        # --------------
        # 1:
        # Proportion of people that got exposed at least once
        #exposed_indication_proportion = state_indication_proportion[parameters_dict["iE-1]
        #key_outputs[0, r] = exposed_indication_proportion
        
        # 2:
        # Max proportion of workforce ill at any time
        #ill_count =  state_count[parameters_dict["iE-1,:] + state_count[parameters_dict["iIA-1,:]  + state_count[parameters_dict["iIS-1,:]
        #max_ill_count_proportion = max(ill_count/parameters_dict["N)
        #key_outputs[1, r] = max_ill_count_proportion

        # 3:
        # Proportion of time units spent ill 
        # ill_count_proportion = state_count_proportion[parameters_dict["iE-1] + state_count_proportion[parameters_dict["iIA-1] + state_count_proportion[parameters_dict["iIS-1]
        #key_outputs[2, r] = ill_count_proportion

        # 4:
        # Proportion of time spent fully productive
        #productivity_proportion = productivity_total/(parameters_dict["N*parameters_dict["T)
        #key_outputs[3, r] = productivity_proportion

       
        # Proportion of people that were away at least once
        #away_indication_proportion    = state_indication_proportion[iA-1]
        #key_outputs[1, r] = away_indication_proportion
        
        # Proportion of time units spent away 
        #away_count_proportion      = state_count_proportion[iA-1]
        #key_outputs[3, r] = away_count_proportion

        #state_count_check = numpy.sum(state_count_total)
        #if state_count_check != parameters_dict["N*parameters_dict["T:
        #    print("state_count_check  :  \n{}".format(state_count_check) )
        #    print("state_count_check failed because total number of time units spent \
        #           in all states by all nodes is not T*N")

        # New key outputs:
        # ---------------
        W_S_prop  = location_and_state_proportion[0,0]
        W_E_prop  = location_and_state_proportion[1,0]
        W_IA_prop = location_and_state_proportion[2,0]
        W_IS_prop = location_and_state_proportion[3,0]
        W_R_prop  = location_and_state_proportion[4,0]
        W_C_prop  = location_and_state_proportion[5,0]

        H_S_prop  = location_and_state_proportion[0,1]
        H_E_prop  = location_and_state_proportion[1,1]
        H_IA_prop = location_and_state_proportion[2,1]
        H_IS_prop = location_and_state_proportion[3,1]
        H_R_prop  = location_and_state_proportion[4,1]
        H_C_prop  = location_and_state_proportion[5,1]

        key_outputs[0, r]  = W_S_prop
        key_outputs[1, r]  = W_E_prop
        key_outputs[2, r]  = W_IA_prop
        key_outputs[3, r]  = W_IS_prop
        key_outputs[4, r]  = W_R_prop
        key_outputs[5, r]  = W_C_prop

        key_outputs[6,  r] = H_S_prop
        key_outputs[7,  r] = H_E_prop
        key_outputs[8,  r] = H_IA_prop
        key_outputs[9,  r] = H_IS_prop
        key_outputs[10, r] = H_R_prop
        key_outputs[11, r] = H_C_prop

        S_prop  = W_S_prop + H_S_prop
        E_prop  = W_E_prop + H_E_prop
        IA_prop = W_IA_prop + H_IA_prop
        IS_prop = W_IS_prop + H_IS_prop
        R_prop  = W_R_prop + H_R_prop
        C_prop  = W_C_prop + H_C_prop

        key_outputs[12, r] = S_prop
        key_outputs[13, r] = E_prop
        key_outputs[14, r] = IA_prop
        key_outputs[15, r] = IS_prop
        key_outputs[16, r] = R_prop
        key_outputs[17, r] = C_prop

        S_ind_prop  = state_indication_proportion[0]
        E_ind_prop  = state_indication_proportion[1]
        IA_ind_prop = state_indication_proportion[2]
        IS_ind_prop = state_indication_proportion[3]
        R_ind_prop  = state_indication_proportion[4]
        C_ind_prop  = state_indication_proportion[5]

        key_outputs[18, r] = S_ind_prop
        key_outputs[19, r] = E_ind_prop
        key_outputs[20, r] = IA_ind_prop
        key_outputs[21, r] = IS_ind_prop
        key_outputs[22, r] = R_ind_prop
        key_outputs[23, r] = C_ind_prop

    key_outputs_average = numpy.sum(key_outputs, axis=1)/num_state_runs # shape (num_key_outputs, )
    key_outputs_averages[:,k] = key_outputs_average



# Splrep
S_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[18,:],  k=3)
E_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[19,:],  k=3)
IA_ind_prop_spline = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[20,:], k=3)
IS_ind_prop_spline = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[21,:], k=3)
R_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[22,:],  k=3)
C_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[23,:],  k=3)

# Interpolate raw values by cubic spline
opacity_plot       = numpy.linspace(0, 1, 100)
S_ind_prop_plot    = interpolate.splev(x=opacity_plot, tck=S_ind_prop_spline)
E_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=E_ind_prop_spline)
IA_ind_prop_plot = interpolate.splev(x=opacity_plot, tck=IA_ind_prop_spline)
IS_ind_prop_plot = interpolate.splev(x=opacity_plot, tck=IS_ind_prop_spline)
R_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=R_ind_prop_spline)
C_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=C_ind_prop_spline)

splines = []
splines.append(S_ind_prop_plot)
splines.append(E_ind_prop_plot)
splines.append(IA_ind_prop_plot)
splines.append(IS_ind_prop_plot)
splines.append(R_ind_prop_plot)
splines.append(C_ind_prop_plot)


# Visualisation
# ----------

colors = ['green','orange','red', 'tab:pink', 'black', 'blue']
#linestyles  = ['-', '-.', '--', '-']
#key_output_names = ["Prop. WF exposed at least once",\
#                    "Max. prop. WF unwell at any time",\
#                    "Prop. time WF spent unwell",\
#                    "Prop. time WF spent productive"]

work_state_props = ["W_S_prop", \
                    "W_E_prop", \
                    "W_IA_prop", \
                    "W_IS_prop", \
                    "W_R_prop", \
                    "W_C_prop"]

home_state_props = ["H_S_prop", \
                    "H_E_prop", \
                    "H_IA_prop", \
                    "H_IS_prop", \
                    "H_R_prop", \
                    "H_C_prop"]

total_state_props = ["S", \
                     "E", \
                     "A", \
                     "U", \
                     "R", \
                     "Q"]

state_ind_props =   ["S_ind_prop", \
                     "E_ind_prop", \
                     "IA_ind_prop", \
                     "IS_ind_prop", \
                     "R_ind_prop", \
                     "C_ind_prop"]
# Plot:
# Adjacency matrix
fig,gs,ax = visualise.plot_single_frame()
plt.spy(A)
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_matrix'.format(parameters_dict["simulation_name"]) ) )



# Plot:
# work and state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[ko, :], ls="--", color=colors[ko], label="{}".format(work_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"work-state proportions")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_workstate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot:
# home and state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[6+ko, :], ls=":", color=colors[ko], label="{}".format(home_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"home-state proportions")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_homestate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# work and home state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[12+ko, :], ls="-", color=colors[ko], label="{}".format(total_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"total-state proportions")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_totalstate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# state indication proportions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[18+ko, :], ls="-", color=colors[ko], label="$x=${}".format(total_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.01)
ax.set_yticks(numpy.linspace(0,1,6))
ax.legend(loc='upper left')
visualise.style_axes(ax, r"Opacity",  r"Proportion entering state $x$")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_state_ind_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# state indication proportions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for spl in range(6):
    ax.plot(opacity_plot, splines[spl], ls="-", color=colors[spl], label="$x=${}".format(total_state_props[spl]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.01)
ax.set_yticks(numpy.linspace(0,1,6))
ax.legend(loc='upper left')
visualise.style_axes(ax, r"Opacity",  r"Proportion entering state $x$")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_state_ind_props_V_opacity_SPL.svg'.format(parameters_dict["simulation_name"]) ) )

print("\nFinished simulation at relative time:  {}".format(time.time() - start_time))