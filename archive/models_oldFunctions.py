#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy

def four_state_error(state, i, t,
                     A, species,
                     N,T,
                     t_incubation   = 4.00, t_asymptomatic     = 3.00, d_isolation = 14,
                     p_unlucky      = 0.01, p_spread           = 0.30, 
                     p_unlikelyHero = 0.05, p_guiltyConscience = 0.10,       
                     iS=1, iE=2, iI=3, iA=4,
                     iT=1, iO=-1,
                     ):
    """
    Here:
    - Infectious become Absent after asymptomatic delay
    - Infectious truers tell neighbours to isolate as soon as they're infectious
    - This is clearly wrong, because if you can tell people you're infectious then you can obviously isolate yourself.

    Solution:
    - Infectious people should only be able to tell other people they're infectious after the asymptomatic delay.
    - In the not A loop, we shouldn't be checking whether our neighbour is infectious, we should be checking if 
      they've been infectious for the asymptomatic delay.

    """
       
    # state[i,t] = 'Susceptible'
    # ---------------------------
    if state[i,t] == iS:
        indicator_infectiousNeighbour = numpy.zeros(N)
        for j in range(N):
            indicator_infectious = (state[j,t] == iI)
            indicator_infectiousNeighbour[j] = A[i,j]*indicator_infectious
        
        p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky
        
        if p_catch_i_unnormed < 1:
            p_catch_i = p_catch_i_unnormed
        elif p_catch_i_unnormed >= 1:
            p_catch_i = 1.0
        else:
            raise Exception
   
        r_catch = numpy.random.uniform(low=0.0,high=1.0)
        if r_catch < p_catch_i :
            # I catch off Infectious neighbour with prob p_catch_i
            state[i,t+1] = iE
        elif r_catch >= p_catch_i:
            state[i,t+1] = iS
        else: 
            raise Exception
    

    # state[i,t] = 'Exposed'
    # ---------------
    elif state[i,t] == iE:
        if t >= t_incubation-1:
            # If there have been enough days of simulation
            if numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                # If Exposed for incubation delay, then Infectious.
                state[i,t+1] = iI
            elif numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                # If not Exposed for incubation delay, then Exposed.
                state[i,t+1] = iE
            else:
                raise Exception
        elif t < t_incubation-1:
            # If there haven't been enough days of simulation
            state[i,t+1] = iE
        else:
            raise Exception
    

    # state[i,t] = 'Infectious'
    # ---------------
    elif state[i,t] == iI:
        
        if t >= t_asymptomatic-1:
            # If there have been enough days of simulation
            if numpy.any( state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                if species[i] == iT:
                     # If Infectious for asymptomatic delay, then truers become Absent with prob=1
                    state[i,t+1] = iA
                elif species[i] == iO:
                    r_guiltyConscience = numpy.random.uniform(low=0.0, high=1.0) 
                    if r_guiltyConscience < p_guiltyConscience:
                        # If Infectious for asymptomatic delay, then liars Absent with prob<1
                        state[i,t+1] = iA
                    elif r_guiltyConscience >= p_guiltyConscience:
                        # If Infectious for asymptomatic delay, then liars Susceptible with prob<1
                        state[i,t+1] = iS
                    else: 
                        raise Exception
            elif numpy.any(  state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                 # If not Infectious for asymptomatic delay, then everyone is Infectious with prob=1
                state[i,t+1] = iI
            else: 
                raise Exception
        elif t < t_asymptomatic-1:
            state[i,t+1] = iI
        else:
            raise Exception
    


    # state[i,t] = 'Absent'
    # ---------------
    elif state[i,t] == iA:
        
        if t >= d_isolation-1:
        # if last d_isolation days exist:
            if numpy.any( state[i,t+1-d_isolation:t+1] - iA*numpy.ones((1, d_isolation)) ) == False:
            # If Absent for isolation delay, then Susceptible. 
            # Note: Could add Recovered state and have some prob of moving to Recovered.
            # Note: Could make it so liars try to return to work sooner
                state[i,t+1] = iS
            elif numpy.any( state[i,t+1-d_isolation:t+1] - iA*numpy.ones((1, d_isolation)) ) == True:
                # If not Absent for isolation delay, then Absent.
                state[i,t+1] = iA
            else:
                raise Exception
        elif t < d_isolation-1:
            state[i,t+1] = iA
        else:
            raise Exception
     
    else:
        print('Conservation error: state moving to zero. \
               Current state of node {} is {}. \
               Terminating at time {}.'.format(i, state[i,t], t)) 
        exit()
        raise Exception 


    # Check neighbours (to see if I need to be Absent)
    # If state[i] != 'Absent':
    # May modify state[i,t+1] to 'Absent', or else leave it as <dictated above>.
    # ---------------
    if state[i,t] != iA:
        # If not Absent, then look at neighbours
        for j in range(N):
            indicator_infectious = (state[j,t] == iI)
            indicator_infectiousNeighbour = A[i,j]*indicator_infectious
            #print('look at neighbour {}'.format(j))
            
            if indicator_infectiousNeighbour == True:
                #print('neighbour {} is infectious'.format(j))
                # If I have Infectious neighbour
                if species[j] == iT:
                    #print('neighbour {} is truer'.format(j))
                    # If neighbour is truer
                    if species[i] == iT:
                        #print('my current state is {} and future state is{}'.format(state[i,t], state[i,t+1]))
                        # If I'm truer, I become Absent with p=1
                        state[i,t+1] = iA
                        #print('but im truer so change future state to {}'.format(state[i,t+1]))
                    elif species[i] == iO:
                        #print('my current state is {} and future state is{}'.format(state[i,t], state[i,t+1]))
                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                        if r_unlikelyHero < p_unlikelyHero:
                            # If I'm liar, I become Absent with p<1 
                            state[i,t+1] = iA
                            #print('but im unlikely hero so change future state to {}'.format(state[i,t+1]))
                        elif r_unlikelyHero >= p_unlikelyHero:
                            # Else I stay as whatever was already determined I would be.
                            state[i,t+1] = state[i,t+1]
                            #print('and im not an unlikely hero, so keep future state as {}'.format(state[i,t+1]))
                        else: 
                            raise Exception
                    else : 
                        raise Exception

                
                elif species[j] == iO:
                    #print('neighbour {} is liar'.format(j))
                    r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                    if r_guiltyConscience < p_guiltyConscience:
                        #print('neighbour {} has guilty conscience, and so tells me'.format(j))
                        if species[i] == iT:
                            #print('im truer with current state {} and future state {}'.format(state[i,t], state[i,t+1]))
                            state[i,t+1] = iA
                            #print(' but im truer, so change future state to {}'.format(state[i,t+1]))
                        elif species[i] == iO:
                            r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                            if r_unlikelyHero < p_unlikelyHero:
                                #print('im unlikely hero liar with current state {} and future state {}'.format(state[i,t], state[i,t+1]))
                                state[i,t+1] = iA
                                #print('as im unlikely hero, I change future state state  to {}'.format(state[i,t+1]))
                            elif r_unlikelyHero > p_unlikelyHero:
                                #print('im not unlikely hero liar with current state {} and future state {}'.format(state[i,t], state[i,t+1]))
                                state[i,t+1] = state[i,t+1]
                                #print('so i leave my future state at {}'.format(state[i,t+1]))
                            else :
                                raise Exception
                        else : 
                            raise Exception
                    elif r_guiltyConscience >= p_guiltyConscience:
                        #print('neighbour {} has not got guilty conscience, and so doesnt tell me'.format(j))
                        #print('my current state is {} and my future state is {}'.format(state[i,t], state[i,t+1]))
                        state[i,t+1] = state[i,t+1]
                        #print('since I dont know neighbour {} has got it, I leave my future state at {}'.format(j, state[i,t+1]))
                    else:
                        print('neighbour doesnt have or not have a guilty conscience! so stopping')
                        raise Exception
                
                else :
                    print('neighbour is neither truer or liar. Exception.')
                    raise Exception
            
            elif indicator_infectiousNeighbour == False:
                # If no infected neighbours, future state is as it was prior to loop
                # I.e. no necessarily equal to current state.
                #print('my current state is {} and my future state is {}'.format(state[i,t], state[i,t+1]))
                state[i,t+1] = state[i,t+1] 
                #print('so I should do nothing. I leave my future state as {}'.format(state[i,t+1]))
                if state[i,t+1] == 0:
                    # Check that mass conserved
                    raise Exception
            
            else:
                raise Exception

    return state[i,t+1]   





def four_state(state, i, t,
               A, species,
               N,T,
               t_incubation   = 4.00, t_asymptomatic     = 3.00, d_isolation = 14, t_symptomatic = 3.00,
               p_unlucky      = 0.01, p_spread           = 0.30, 
               p_unlikelyHero = 0.05, p_guiltyConscience = 0.10,       
               iS=1, iE=2, iI=3, iA=4,
               iT=1, iO=-1,
               ):
    """
    Notes
    ----------
    - Includes states: Susceptible, Exposed, Infectious, Absent.
    
    Problems before:
    - Infectious truers became absent after asymptomatic delay
    - But infectious truers tell neighbours to isolate as soon as they're infectious
    - This is clearly wrong, because if you can tell people you're infectious then you can obviously isolate yourself.

    - Infectious liars become Susceptible after the asympotomatic delay
    - This is wrong, because that means that there's no period where they're symptomatic
    - This is ok for truers, because as long as d_isolation > t_symptomatic, they spend their symptomatic days at home.

    Now:
    - Infectious Truers should only be able to tell other people they're infectious after the asymptomatic delay.
    - Infectious Liars shouuld only become Susceptible after they've been Infectious for the asymptomatic delay and the symptomatic delay.
    - In the not A loop, we shouldn't be checking whether our neighbour is infectious, we should be checking if 
      they've been infectious for the asymptomatic delay.

    - Infectious Truers spend the symptomatic period Away.
    - Infectious Liars spend the symptomatic period  Infectious.

    """
       
    # state[i,t] = 'Susceptible'
    # ---------------------------
    if state[i,t] == iS:
        indicator_infectiousNeighbour = numpy.zeros(N)
        for j in range(N):
            indicator_infectious = (state[j,t] == iI)
            indicator_infectiousNeighbour[j] = A[i,j]*indicator_infectious
        
        p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky
        
        if p_catch_i_unnormed < 1:
            p_catch_i = p_catch_i_unnormed
        elif p_catch_i_unnormed >= 1:
            p_catch_i = 1.0
        else:
            raise Exception
   
        r_catch = numpy.random.uniform(low=0.0,high=1.0)
        if r_catch < p_catch_i :
            # I catch off Infectious neighbour with prob p_catch_i
            state[i,t+1] = iE
        elif r_catch >= p_catch_i:
            state[i,t+1] = iS
        else: 
            raise Exception
    

    # state[i,t] = 'Exposed'
    # ---------------
    elif state[i,t] == iE:
        if t >= t_incubation-1:
            # If there have been enough days of simulation
            if numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                # If Exposed for incubation delay, then Infectious.
                state[i,t+1] = iI
            elif numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                # If not Exposed for incubation delay, then Exposed.
                state[i,t+1] = iE
            else:
                raise Exception
        elif t < t_incubation-1:
            # If there haven't been enough days of simulation
            state[i,t+1] = iE
        else:
            raise Exception
    

    # state[i,t] = 'Infectious'
    # ---------------
    elif state[i,t] == iI:
        
        if t >= t_asymptomatic-1:
            # If there have been enough days of simulation
            if numpy.any( state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                if species[i] == iT:
                     # If Infectious for asymptomatic delay, then truers become Absent with prob=1
                    state[i,t+1] = iA
                elif species[i] == iO:
                    r_guiltyConscience = numpy.random.uniform(low=0.0, high=1.0) 
                    if r_guiltyConscience < p_guiltyConscience:
                        # If Infectious for asymptomatic delay, then liars Absent with prob<1
                        state[i,t+1] = iA
                    elif r_guiltyConscience >= p_guiltyConscience:
                        if t >= (t_asymptomatic + t_symptomatic) -1:
                            # If there have been enough days to be asym and sym
                            if numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == False:
                            # If Infectious for asymptomatic delay + symptomatic delya, then liars susceptible
                                state[i,t+1] = iS
                            elif numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == True:
                            # Elif not infectious for asymptomatic delay + symptomatic delay, then liars Infectious
                                state[i,t+1] = iI
                            else:
                                raise Exception
                        elif t < (t_asymptomatic + t_symptomatic) -1:
                            state[i,t+1] = iI
                        else: 
                            raise Exception
                    else:
                        raise Exception
            elif numpy.any(  state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                 # If not Infectious for asymptomatic delay, then everyone is Infectious with prob=1
                state[i,t+1] = iI
            else: 
                raise Exception
        elif t < t_asymptomatic-1:
            state[i,t+1] = iI
        else:
            raise Exception
    


    # state[i,t] = 'Absent'
    # ---------------
    elif state[i,t] == iA:
        
        if t >= d_isolation-1:
        # if last d_isolation days exist:
            if numpy.any( state[i,t+1-d_isolation:t+1] - iA*numpy.ones((1, d_isolation)) ) == False:
            # If Absent for isolation delay, then Susceptible. 
            # Note: Could add Recovered state and have some prob of moving to Recovered.
            # Note: Could make it so liars try to return to work sooner
                state[i,t+1] = iS
            elif numpy.any( state[i,t+1-d_isolation:t+1] - iA*numpy.ones((1, d_isolation)) ) == True:
                # If not Absent for isolation delay, then Absent.
                state[i,t+1] = iA
            else:
                raise Exception
        elif t < d_isolation-1:
            state[i,t+1] = iA
        else:
            raise Exception
     
    else:
        print('Conservation error: state moving to zero. \
               Current state of node {} is {}. \
               Terminating at time {}.'.format(i, state[i,t], t)) 
        exit()
        raise Exception 


    # Check neighbours (to see if I need to be Absent)
    # If state[i] != 'Absent':
    # May modify state[i,t+1] to 'Absent', or else leave it as <dictated above>.
    # ---------------
    if state[i,t] != iA:
        # If there have been enough days of simulation
        if t >= t_asymptomatic-1:
            # Look at a potential neighbour
            for j in range(N):
                # Check if potential neighbour j is a neighbour
                indicator_neighbour = A[i,j]
                if indicator_neighbour == 1:
                    # Check if neighbour has been infected for asymptomatic delay
                    if numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                        # Situation:
                        # -----------
                        # I have a neighbour who has been infectious for the asymptomatic delay
                        # They now know they have the virus, so they might warn me to go home
                        
                        # If neighbour is Truer
                        if species[j] == iT:
                            if species[i] == iT:
                                # If I'm truer, I become Absent with p=1
                                state[i,t+1] = iA
                            elif species[i] == iO:
                                r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                if r_unlikelyHero < p_unlikelyHero:
                                    # If I'm liar, I become Absent with p<1 
                                    state[i,t+1] = iA
                                elif r_unlikelyHero >= p_unlikelyHero:
                                    # Else I stay as whatever was already determined I would be.
                                    state[i,t+1] = state[i,t+1]
                                else: 
                                    raise Exception
                            else : 
                                raise Exception
                            
                        # If neighbour is Liar  
                        elif species[j] == iO:
                            r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                            if r_guiltyConscience < p_guiltyConscience:
                                if species[i] == iT:
                                    state[i,t+1] = iA
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        state[i,t+1] = iA
                                    elif r_unlikelyHero > p_unlikelyHero:
                                        state[i,t+1] = state[i,t+1]
                                    else :
                                        raise Exception
                                else : 
                                    raise Exception
                            elif r_guiltyConscience >= p_guiltyConscience:
                                state[i,t+1] = state[i,t+1]
                            else:
                                print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                raise Exception
                            
                        else :
                            print('neighbour is neither truer or liar. Exception.')
                            raise Exception
                        

                    elif numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                        # Situation:
                        # -----------
                        # I have a neighbour who has not been infectious for the asymptomatic delay
                        # They don't know they have the virus, so they can't warn me to go home    
                        # Hence, future state is as it was prior to loop
                        state[i,t+1] = state[i,t+1] 
                        
                        if state[i,t+1] == 0:
                            # Check that mass conserved
                            raise Exception
                        
                    else:
                        raise Exception
                
                # Elif j isn't my neighbour
                elif indicator_neighbour == 0:
                    # I carry on as I was
                    state[i,t+1] = state[i,t+1] 
                else: 
                    raise Exception
        
        # If simulation not long enough to look at neighbour's history
        elif t < t_asymptomatic-1:
            # Then I carry on as I was
            state[i,t+1] = state[i,t+1] 
        else :
            raise Exception

    return state[i,t+1]
            
            


def five_state(state, i, t,
               A, species,
               N,T,
               t_incubation   = 4.00, t_asymptomatic     = 3.00, d_isolation = 14, t_symptomatic = 3.00,
               p_unlucky      = 0.01, p_spread           = 0.30, 
               p_unlikelyHero = 0.05, p_guiltyConscience = 0.10,       
               iS=1, iE=2, iI=3, iA=4, iAW=5,
               iT=1, iO=-1,
               ):
    """
    Notes
    ----------
    - Includes states: Susceptible, Exposed, Infectious, Absent-Unwell, Absent-Well.
    - Absent-Unwell here is equivalent to Absent in the four_state model.

    - five_state model was necessary because:
    - We want to understand the productivity of the workforce.
    - Before we could only understand that people were at home.
    - We didn't know whether they were ill or not, which means we didn't know if thye could do some work from home.
    - With the Absent-Well state, we can understand how many people are going home as a precaution.
    """
       
    # state[i,t] = 'Susceptible'
    # ---------------------------
    if state[i,t] == iS:
        indicator_infectiousNeighbour = numpy.zeros(N)
        for j in range(N):
            indicator_infectious = (state[j,t] == iI)
            indicator_infectiousNeighbour[j] = A[i,j]*indicator_infectious
        
        p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky
        
        if p_catch_i_unnormed < 1:
            p_catch_i = p_catch_i_unnormed
        elif p_catch_i_unnormed >= 1:
            p_catch_i = 1.0
        else:
            raise Exception
   
        r_catch = numpy.random.uniform(low=0.0,high=1.0)
        if r_catch < p_catch_i :
            # I catch off Infectious neighbour with prob p_catch_i
            state[i,t+1] = iE
        elif r_catch >= p_catch_i:
            state[i,t+1] = iS
        else: 
            raise Exception
    

    # state[i,t] = 'Exposed'
    # ---------------
    elif state[i,t] == iE:
        if t >= t_incubation-1:
            # If there have been enough days of simulation
            if numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                # If Exposed for incubation delay, then Infectious.
                state[i,t+1] = iI
            elif numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                # If not Exposed for incubation delay, then Exposed.
                state[i,t+1] = iE
            else:
                raise Exception
        elif t < t_incubation-1:
            # If there haven't been enough days of simulation
            state[i,t+1] = iE
        else:
            raise Exception
    

    # state[i,t] = 'Infectious'
    # ---------------
    elif state[i,t] == iI:
        
        if t >= t_asymptomatic-1:
        # If there have been enough days of simulation
            if numpy.any( state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                if species[i] == iT:
                     # If Infectious for asymptomatic delay, then truers become Absent with prob=1
                    state[i,t+1] = iA
                elif species[i] == iO:
                    r_guiltyConscience = numpy.random.uniform(low=0.0, high=1.0) 
                    if r_guiltyConscience < p_guiltyConscience:
                        # If Infectious for asymptomatic delay, then liars Absent with prob<1
                        state[i,t+1] = iA
                    elif r_guiltyConscience >= p_guiltyConscience:
                        if t >= (t_asymptomatic + t_symptomatic) -1:
                            # If there have been enough days to be asym and sym
                            if numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == False:
                            # If Infectious for asymptomatic delay + symptomatic delya, then liars susceptible
                                state[i,t+1] = iS
                            elif numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == True:
                            # Elif not infectious for asymptomatic delay + symptomatic delay, then liars Infectious
                                state[i,t+1] = iI
                            else:
                                raise Exception
                        elif t < (t_asymptomatic + t_symptomatic) -1:
                            state[i,t+1] = iI
                        else: 
                            raise Exception
                    else:
                        raise Exception
            elif numpy.any(  state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                 # If not Infectious for asymptomatic delay, then everyone is Infectious with prob=1
                state[i,t+1] = iI
            else: 
                raise Exception
        elif t < t_asymptomatic-1:
            state[i,t+1] = iI
        else:
            raise Exception
    


    # state[i,t] = 'AbsentUnwell'
    # At the moment we're treating L and T the same once they're Absent.
    # ---------------
    elif state[i,t] == iA:
        
        if t >= t_symptomatic-1:
        # if last d_symtomatic days exist:
            if numpy.any( state[i,t+1-t_symptomatic:t+1] - iA*numpy.ones((1, t_symptomatic)) ) == False:
            # If AbsentUnwell for symptomatic delay, then AwayWell. 
            # Note: Could make it so liars return to work before they've done the AW state
                state[i,t+1] = iAW
            elif numpy.any( state[i,t+1-d_isolation:t+1] - iA*numpy.ones((1, d_isolation)) ) == True:
                # If not AbsentUnwell for symptomatic delay, then AbsentUnwell.
                state[i,t+1] = iA
            else:
                raise Exception
        elif t < t_symptomatic-1:
            state[i,t+1] = iA
        else:
            raise Exception
     

    # state[i,t] = 'AbsentWell'
    # ---------------
    elif state[i,t] == iAW:
        d_awaywell = d_isolation - t_symptomatic

        if t >= d_awaywell -1:
        # if last d_awaywell days exist:
            if numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == False:
            # If AbsentWell for awaywell delay, then Susceptible. 
            # Note: Could add Recovered state and have some prob of moving to Recovered.
                state[i,t+1] = iS
            elif numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == True:
                # If not AbsentWell for awaywell delay, then AbsentWell.
                state[i,t+1] = iAW
            else:
                raise Exception
        elif t < d_awaywell-1:
            state[i,t+1] = iAW
        else:
            raise Exception
     
    else:
        print('Conservation error: state moving to zero. \
               Current state of node {} is {}. \
               Terminating at time {}.'.format(i, state[i,t], t)) 
        exit()
        raise Exception 

    
    # Check neighbours (to see if I need to be Absent)
    # If state[i] != 'AbsentUnwell or AbsentWell':
    # May modify state[i,t+1] to 'AbsentWell', or else leave it as <dictated above>.
    # ---------------
    if (state[i,t] != iA and state[i,t] != iAW):
        # If there have been enough days of simulation
        if t >= t_asymptomatic-1:
            # Look at a potential neighbour
            for j in range(N):
                # Check if potential neighbour j is a neighbour
                indicator_neighbour = A[i,j]
                if indicator_neighbour == 1:
                    # Check if neighbour has been infected for asymptomatic delay
                    if numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                        # Situation:
                        # -----------
                        # I have a neighbour who has been infectious for the asymptomatic delay
                        # They now know they have the virus, so they might warn me to go home
                        if state[i,t] == iS:
                        # If I'm Susceptible, then If I go away I will be AwayWell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iAW
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become AbsentWell with p<1 
                                        state[i,t+1] = iAW
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iAW
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iAW
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception
                            
                        else :
                        # If I'm Exposed or Infectious, then If I go away I will be AwayUnwell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iA
                                    #print('but im truer so change future state to {}'.format(state[i,t+1]))
                                elif species[i] == iO:
                                    #print('my current state is {} and future state is{}'.format(state[i,t], state[i,t+1]))
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become AbsentWell with p<1 
                                        state[i,t+1] = iA
                                        #print('but im unlikely hero so change future state to {}'.format(state[i,t+1]))
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                        #print('and im not an unlikely hero, so keep future state as {}'.format(state[i,t+1]))
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iA
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iA
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception       
                    elif numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                        # Situation:
                        # -----------
                        # I have a neighbour who has not been infectious for the asymptomatic delay
                        # They don't know they have the virus, so they can't warn me to go home    
                        # Hence, future state is as it was prior to loop
                        # I.e. no necessarily equal to current state.
                        state[i,t+1] = state[i,t+1] 
                        #print('so I should do nothing. I leave my future state as {}'.format(state[i,t+1]))
                        if state[i,t+1] == 0:
                            # Check that mass conserved
                            raise Exception
                        
                    else:
                        raise Exception
                    
                # Elif j isn't my neighbour
                elif indicator_neighbour == 0:
                    # I carry on as I was
                    state[i,t+1] = state[i,t+1] 
                else: 
                    raise Exception
                
        # If simulation not long enough to look at neighbour's history
        elif t < t_asymptomatic-1:
            # Then I carry on as I was
            state[i,t+1] = state[i,t+1] 
        else :
            raise Exception

    return state[i,t+1]
            




def six_state(state, i, t,
               A, species,
               N,T,
               t_incubation   = 4.00, t_asymptomatic     = 3.00, d_isolation = 14, t_symptomatic = 3.00,
               p_unlucky      = 0.01, p_spread           = 0.30, 
               p_unlikelyHero = 0.05, p_guiltyConscience = 0.10, p_recovery=0.80,       
               iS=1, iE=2, iI=3, iAU=4, iAW=5, iR=6,
               iT=1, iO=-1,
               ):
    """
    Notes
    ----------
    - Includes states: Susceptible, Exposed, Infectious, Absent-Unwell, Absent-Well, Recovered.
    - Absent-Unwell here is equivalent to Absent in the four_state model.
    - Recovered is the additional state as opposed to five_state model.

    - six_state model was necessary because:
    - We want to understand the recovery rate of the workforce.
    - Before we could not understand how quickly everyone was immune to the virus.

    - The problems here are:
    - It is difficult to tell whether a Liar is Infectious but Well or Infectious and Unwell.
    - We can know this by checking whether they've been Infectious for more than the asymptomatic delay.
    - But it would be more complete to split Infectious into two states: Infectious-Well and Infectious-Unwell
    - This will make it easier to measure the productivity of the workforce.
    """
       
    # state[i,t] = 'Susceptible'
    # ---------------------------
    if state[i,t] == iS:
        indicator_infectiousNeighbour = numpy.zeros(N)
        for j in range(N):
            indicator_infectious = (state[j,t] == iI)
            indicator_infectiousNeighbour[j] = A[i,j]*indicator_infectious
        
        p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky
        
        if p_catch_i_unnormed < 1:
            p_catch_i = p_catch_i_unnormed
        elif p_catch_i_unnormed >= 1:
            p_catch_i = 1.0
        else:
            raise Exception
   
        r_catch = numpy.random.uniform(low=0.0,high=1.0)
        if r_catch < p_catch_i :
            # I catch off Infectious neighbour with prob p_catch_i
            state[i,t+1] = iE
        elif r_catch >= p_catch_i:
            state[i,t+1] = iS
        else: 
            raise Exception
    

    # state[i,t] = 'Exposed'
    # ---------------
    elif state[i,t] == iE:
        if t >= t_incubation-1:
            # If there have been enough days of simulation
            if numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                # If Exposed for incubation delay, then Infectious.
                state[i,t+1] = iI
            elif numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                # If not Exposed for incubation delay, then Exposed.
                state[i,t+1] = iE
            else:
                raise Exception
        elif t < t_incubation-1:
            # If there haven't been enough days of simulation
            state[i,t+1] = iE
        else:
            raise Exception
    

    # state[i,t] = 'Infectious'
    # ---------------
    elif state[i,t] == iI:
        
        if t >= t_asymptomatic-1:
        # If there have been enough days of simulation
            if numpy.any( state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                
                if species[i] == iT:
                     # If Infectious for asymptomatic delay, then truers become Absent with prob=1
                    state[i,t+1] = iAU
                
                elif species[i] == iO:
                    r_guiltyConscience = numpy.random.uniform(low=0.0, high=1.0) 
                    if r_guiltyConscience < p_guiltyConscience:
                        # If Infectious for asymptomatic delay, then liars Absent with prob<1
                        state[i,t+1] = iAU
                    elif r_guiltyConscience >= p_guiltyConscience:
                        if t >= (t_asymptomatic + t_symptomatic) -1:
                            # If there have been enough days to be asym and sym
                            if numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == False:
                            # If Infectious for asymptomatic delay + symptomatic delay, then liars Susceptible or Recovered
                                r_recovery = numpy.random.uniform(low=0.0,high=1.0)
                                if r_recovery < p_recovery:
                                    state[i,t+1] = iR
                                elif r_recovery >= p_recovery:
                                    state[i,t+1] = iS
                                else : 
                                    raise Exception
                            elif numpy.any( state[i,t+1-(t_asymptomatic + t_symptomatic):t+1] - iI*numpy.ones((1, (t_asymptomatic + t_symptomatic) )) ) == True:
                            # Elif not infectious for asymptomatic delay + symptomatic delay, then liars Infectious
                                state[i,t+1] = iI
                            else:
                                raise Exception
                        elif t < (t_asymptomatic + t_symptomatic) -1:
                            state[i,t+1] = iI
                        else: 
                            raise Exception
                    else:
                        raise Exception
            elif numpy.any(  state[i,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                 # If not Infectious for asymptomatic delay, then everyone is Infectious with prob=1
                state[i,t+1] = iI
            else: 
                raise Exception
        elif t < t_asymptomatic-1:
            state[i,t+1] = iI
        else:
            raise Exception
    


    # state[i,t] = 'AbsentUnwell'
    # At the moment we're treating L and T the same once they're Absent.
    # ---------------
    elif state[i,t] == iAU:
        
        if t >= t_symptomatic-1:
        # if last d_symtomatic days exist:
            if numpy.any( state[i,t+1-t_symptomatic:t+1] - iAU*numpy.ones((1, t_symptomatic)) ) == False:
            # If AbsentUnwell for symptomatic delay, then AwayWell. 
            # Note: Could make it so liars return to work before they've done the AW state
                state[i,t+1] = iAW
            elif numpy.any( state[i,t+1-t_symptomatic:t+1] - iAU*numpy.ones((1, t_symptomatic)) ) == True:
                # If not AbsentUnwell for symptomatic delay, then AbsentUnwell.
                state[i,t+1] = iAU
            else:
                raise Exception
        elif t < t_symptomatic-1:
            state[i,t+1] = iAU
        else:
            raise Exception
     

    # state[i,t] = 'AbsentWell'
    # ---------------
    elif state[i,t] == iAW:
        d_awaywell = d_isolation - t_symptomatic

        if t >= d_awaywell -1:
        # if last d_awaywell days exist:
            if numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == False:
            # If AbsentWell for awaywell delay, then Susceptible or Recovered.
                r_recovery = numpy.random.uniform(low=0.0,high=1.0)
                print(r_recovery)
                if r_recovery < p_recovery:
                    state[i,t+1] = iR
                elif r_recovery >= p_recovery:
                    state[i,t+1] = iS
                else : 
                    raise Exception
            elif numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == True:
                # If not AbsentWell for awaywell delay, then AbsentWell.
                state[i,t+1] = iAW
            else:
                raise Exception
        elif t < d_awaywell-1:
            state[i,t+1] = iAW
        else:
            raise Exception
    
    elif state[i,t] == iR:
        # If I'm recovered, I'm immune, and stay so for all time.
        state[i,t+1] = iR
     
    else:
        print('Conservation error: state moving to zero. \
               Current state of node {} is {}. \
               Terminating at time {}.'.format(i, state[i,t], t)) 
        exit()
        raise Exception 

    
    # Check neighbours (to see if I need to be Absent)
    # If state[i] != 'AbsentUnwell or AbsentWell or Recovered':
    # May modify state[i,t+1] to 'AbsentWell', or else leave it as <dictated above>.
    # Could have it so that Recovered people have to go home too.
    # ---------------
    if (state[i,t] != iAU and state[i,t] != iAW and state[i,t] != iR):
        # If there have been enough days of simulation
        if t >= t_asymptomatic-1:
            # Look at a potential neighbour
            for j in range(N):
                # Check if potential neighbour j is a neighbour
                indicator_neighbour = A[i,j]
                if indicator_neighbour == 1:
                    # Check if neighbour has been infected for asymptomatic delay
                    if numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == False:
                        # Situation:
                        # -----------
                        # I have a neighbour who has been infectious for the asymptomatic delay
                        # They now know they have the virus, so they might warn me to go home
                        if state[i,t] == iS:
                        # If I'm Susceptible, then If I go away I will be AwayWell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iAW
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    print(r_unlikelyHero)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become AbsentWell with p<1 
                                        state[i,t+1] = iAW
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iAW
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iAW
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception
                            
                        else :
                        # If I'm Exposed or Infectious, then If I go away I will be AwayUnwell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iAU
                                    # THIS DOESN't QUITE MAKE SENSE BECAUSE NOW:
                                    # IF IM EXPOSED AND MY NEIGHBOUR TELLS ME TO GO HOME, THEN I CAN CUT THE
                                    # AMOUNT OF TIME I'M ILL FOR
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become AbsentWell with p<1 
                                        state[i,t+1] = iAU
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iAU
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iAU
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception       
                    elif numpy.any( state[j,t+1-t_asymptomatic:t+1] - iI*numpy.ones((1, t_asymptomatic)) ) == True:
                        # Situation:
                        # -----------
                        # I have a neighbour who has not been infectious for the asymptomatic delay
                        # They don't know they have the virus, so they can't warn me to go home    
                        # Hence, future state is as it was prior to loop
                        # I.e. no necessarily equal to current state.
                        state[i,t+1] = state[i,t+1] 
                        #print('so I should do nothing. I leave my future state as {}'.format(state[i,t+1]))
                        if state[i,t+1] == 0:
                            # Check that mass conserved
                            raise Exception
                        
                    else:
                        raise Exception
                    
                # Elif j isn't my neighbour
                elif indicator_neighbour == 0:
                    # I carry on as I was
                    state[i,t+1] = state[i,t+1] 
                else: 
                    raise Exception
                
        # If simulation not long enough to look at neighbour's history
        elif t < t_asymptomatic-1:
            # Then I carry on as I was
            state[i,t+1] = state[i,t+1] 
        else :
            raise Exception

    return state[i,t+1]




def seven_state(state, i, t,
                    A, species,
                    N,T,
                    t_incubation   = 4.00, t_asymptomatic     = 3.00, d_isolation = 14, t_symptomatic = 3.00,
                    p_unlucky      = 0.01, p_spread           = 0.30, 
                    p_unlikelyHero = 0.05, p_guiltyConscience = 0.10, p_recovery=0.80,       
                    iS=1, iE=2, iIW=3, iIU=4, iAU=5, iAW=6, iR=7,
                    iT=1, iO=-1,
                    ):
    """
    Notes
    ----------
    - Includes states: Susceptible, Exposed, Infectious-Well, Infectious-Unwell, Absent-Unwell, Absent-Well, Recovered.
    - Infectious in six_state model has been split into two Infectious states.
    - The purpose of this is to understand whether Liars are well or unwell when Infectious.

    - seven_state model is necessary because:
    - We want to understand the Wellness of Liars.
    - So that we can make assumptions about their productivity.
    """
       
    # state[i,t] = 'Susceptible'
    # ---------------------------
    if state[i,t] == iS:
        
        indicator_infectiousNeighbour = numpy.zeros(N)
        for j in range(N):
            indicator_infectious = (state[j,t] == (iIW or iIU) )
            indicator_infectiousNeighbour[j] = A[i,j]*indicator_infectious
        
        p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky
        
        if p_catch_i_unnormed < 1:
            p_catch_i = p_catch_i_unnormed
        elif p_catch_i_unnormed >= 1:
            p_catch_i = 1.0
        else:
            raise Exception
   
        r_catch = numpy.random.uniform(low=0,high=2)
        if r_catch < p_catch_i :
            state[i,t+1] = iE
        elif r_catch >= p_catch_i:
            state[i,t+1] = iS
        else: 
            raise Exception
    

    # state[i,t] = 'Exposed'
    # ---------------
    elif state[i,t] == iE:

        if t >= t_incubation-1:
            # If there have been enough days of simulation
            if numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                # If Exposed for incubation delay, then Infectious-Well.
                state[i,t+1] = iIW
            elif numpy.any(state[i,t+1-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                # If not Exposed for incubation delay, then Exposed.
                state[i,t+1] = iE
            else:
                raise Exception
        elif t < t_incubation-1:
            # If there haven't been enough days of simulation
            state[i,t+1] = iE
        else:
            raise Exception
    

    # state[i,t] = 'Infectious-Well'
    # ---------------
    elif state[i,t] == iIW:

        if t >= t_asymptomatic-1:
            # If there have been enough days of simulation
            if numpy.any( state[i,t+1-t_asymptomatic:t+1] - iIW*numpy.ones((1, t_asymptomatic)) ) == False:                
               
                if species[i] == iT:
                     # If Infectious-Well for asymptomatic delay, then Truers become Absent-Unwell with prob=1
                    state[i,t+1] = iAU
                
                elif species[i] == iO:
                    r_guiltyConscience = numpy.random.uniform(low=0.0, high=1.0) 
                    if r_guiltyConscience < p_guiltyConscience:
                        # If Infectious-Well for asymptomatic delay, then liars Absent-Unwell with prob<1
                        state[i,t+1] = iAU
                    elif r_guiltyConscience >= p_guiltyConscience:
                        # If Liars don't go home, they become Infectious-Unwell with prob<1
                        state[i,t+1] = iIU 
            elif numpy.any(  state[i,t+1-t_asymptomatic:t+1] - iIW*numpy.ones((1, t_asymptomatic)) ) == True:
                 # If not Infectious-Well for asymptomatic delay, then everyone is Infectious-Well with prob=1
                state[i,t+1] = iIW
            else: 
                raise Exception
        elif t < t_asymptomatic-1:
            state[i,t+1] = iIW
        else:
            raise Exception
    


    # state[i,t] = 'Infectious-Unwell'
    # For Liars only.
    # ---------------
    elif state[i,t] == iIU:
        
        if t >= t_symptomatic-1:
            # if last d_symtomatic days exist:
            if numpy.any( state[i,t+1-t_symptomatic:t+1] - iIU*numpy.ones((1, t_symptomatic)) ) == False:
                # If Infectous-Unwell for symptomatic delay, then might recover.
                r_recovery = numpy.random.uniform(low=0.0, high=1.0)
                if r_recovery < p_recovery:
                    state[i,t+1] = iR
                elif r_recovery >= p_recovery:
                    state[i,t+1] = iS
                else:
                    raise Exception
            elif numpy.any( state[i,t+1-t_symptomatic:t+1] - iIU*numpy.ones((1, t_symptomatic)) ) == True:
                # If not Infectious-Unwell for symptomatic delay, then Infectious-Unwell
                state[i,t+1] = iIU
            else:
                raise Exception
        elif t < t_symptomatic-1:
            state[i,t+1] = iIU
        else:
            raise Exception


    # state[i,t] = 'Absent-Unwell'
    # For Liars and Truers (because Liars might go home)
    # At the moment we're treating L and T the same once they're Absent.
    # ---------------
    elif state[i,t] == iAU:
        
        if t >= t_symptomatic-1:
        # If last t_symptomatic days exist:
            if numpy.any( state[i,t+1-t_symptomatic:t+1] - iAU*numpy.ones((1, t_symptomatic)) ) == False:
            # If Absent-Unwell for symptomatic delay, then Absent-Well. 
            # Note: Could make it so liars return to work before they've done the AW state
                state[i,t+1] = iAW
            elif numpy.any( state[i,t+1-t_symptomatic:t+1] - iAU*numpy.ones((1, t_symptomatic)) ) == True:
                # If not AbsentUnwell for symptomatic delay, then AbsentUnwell.
                state[i,t+1] = iAU
            else:
                raise Exception
        elif t < t_symptomatic-1:
            state[i,t+1] = iAU
        else:
            raise Exception
     

    # state[i,t] = 'Absent-Well'
    # ---------------
    elif state[i,t] == iAW:
        d_awaywell = d_isolation - t_symptomatic

        if t >= d_awaywell -1:
        # if last d_awaywell days exist:
            if numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == False:
            # If AbsentWell for awaywell delay, then Susceptible or Recovered.
                r_recovery = numpy.random.uniform(low=0.0,high=1.0)
                if r_recovery < p_recovery:
                    state[i,t+1] = iR
                elif r_recovery >= p_recovery:
                    state[i,t+1] = iS
                else : 
                    raise Exception
            elif numpy.any( state[i,t+1-d_awaywell:t+1] - iAW*numpy.ones((1, d_awaywell)) ) == True:
                # If not AbsentWell for awaywell delay, then AbsentWell.
                state[i,t+1] = iAW
            else:
                raise Exception
        elif t < d_awaywell-1:
            state[i,t+1] = iAW
        else:
            raise Exception
    


    # state[i,t] = 'Recovered'
    # ---------------
    elif state[i,t] == iR:
        # If I'm recovered, I'm immune, and stay so for all time.
        state[i,t+1] = iR


    # state[i,t] =='Other' (some other number)
    # E.g., state[i,t] == 0 - means rules haven't assigned node i in previous t step. So error.
    else:
        print("Conservation error: State moving to zero. \
               Current state of node i={} is state[i,t]={}. \
               Terminating at time t={}.".format(i, state[i,t], t)) 
        exit()
        raise Exception 

    
    # Check neighbours (to see if I need to be Absent)
    # If state[i] != 'AbsentUnwell or AbsentWell or Recovered':
    # May modify state[i,t+1] to 'AbsentWell', or else leave it as <dictated above>.
    # Could have it so that Recovered people have to go home too.
    # ---------------
    if (state[i,t] != iAU and state[i,t] != iAW and state[i,t] != iR):
        # If there have been enough days of simulation
        if t >= t_asymptomatic-1:
            # Look at a potential neighbour
            for j in range(N):
                # Check if potential neighbour j is a neighbour
                indicator_neighbour = A[i,j]
                if indicator_neighbour == 1:
                    # Check if neighbour has been Infectious-Well for asymptomatic delay
                    if numpy.any( state[j,t+1-t_asymptomatic:t+1] - iIW*numpy.ones((1, t_asymptomatic)) ) == False:
                        # Situation:
                        # -----------
                        # I have a neighbour who has been Infectious-Well for the asymptomatic delay
                        # They will now become Infectious-Unwelll, so they might warn me to go home
                        if state[i,t] == iS:
                        # If I'm Susceptible, then If I go away I will be AwayWell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iAW
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become AbsentWell with p<1 
                                        state[i,t+1] = iAW
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iAW
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iAW
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception
                            
                        elif state[i,t] == (iE or iIW or iIU) :
                        # If I'm Exposed or Infectious, then If I go away I will be AwayUnwell
                            if species[j] == iT:
                                # If neighbour is truer
                                if species[i] == iT:
                                    # If I'm truer, I become AbsentWell with p=1
                                    state[i,t+1] = iAU
                                    # THIS DOESN't QUITE MAKE SENSE BECAUSE NOW:
                                    # IF IM EXPOSED AND MY NEIGHBOUR TELLS ME TO GO HOME, THEN I CAN CUT THE
                                    # AMOUNT OF TIME I'M ILL FOR
                                elif species[i] == iO:
                                    r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                    if r_unlikelyHero < p_unlikelyHero:
                                        # If I'm liar, I become Absent-Unwell with p<1 
                                        state[i,t+1] = iAU
                                    elif r_unlikelyHero >= p_unlikelyHero:
                                        # Else I stay as whatever was already determined I would be.
                                        state[i,t+1] = state[i,t+1]
                                    else: 
                                        raise Exception
                                else : 
                                    raise Exception
                                
                                
                            elif species[j] == iO:
                                r_guiltyConscience = numpy.random.uniform(low=0.0,high=1.0)
                                if r_guiltyConscience < p_guiltyConscience:
                                    if species[i] == iT:
                                        state[i,t+1] = iAU
                                    elif species[i] == iO:
                                        r_unlikelyHero = numpy.random.uniform(low=0.0,high=1.0)
                                        if r_unlikelyHero < p_unlikelyHero:
                                            state[i,t+1] = iAU
                                        elif r_unlikelyHero > p_unlikelyHero:
                                            state[i,t+1] = state[i,t+1]
                                        else :
                                            raise Exception
                                    else : 
                                        raise Exception
                                elif r_guiltyConscience >= p_guiltyConscience:
                                    state[i,t+1] = state[i,t+1]
                                else:
                                    print('neighbour doesnt have or not have a guilty conscience! so stopping')
                                    raise Exception
                                
                            else :
                                print('neighbour is neither truer or liar. Exception.')
                                raise Exception       
                    elif numpy.any( state[j,t+1-t_asymptomatic:t+1] - iIW*numpy.ones((1, t_asymptomatic)) ) == True:
                        # Situation:
                        # -----------
                        # I have a neighbour who has not been infectious for the asymptomatic delay
                        # They don't know they have the virus, so they can't warn me to go home    
                        # Hence, future state is as it was prior to loop
                        # I.e. no necessarily equal to current state.
                        state[i,t+1] = state[i,t+1] 
                        #print('so I should do nothing. I leave my future state as {}'.format(state[i,t+1]))
                        if state[i,t+1] == 0:
                            # Check that mass conserved
                            raise Exception
                        
                    else:
                        raise Exception
                    
                # Elif j isn't my neighbour
                elif indicator_neighbour == 0:
                    # I carry on as I was
                    state[i,t+1] = state[i,t+1] 
                else: 
                    raise Exception
                
        # If simulation not long enough to look at neighbour's history
        elif t < t_asymptomatic-1:
            # Then I carry on as I was
            state[i,t+1] = state[i,t+1] 
        else :
            raise Exception

    return state[i,t+1]










# Old check to see if I need to go home.

#    if state[i,t] in (iS, iE, iIA, iIS):
#        if location[i,t] == iW:
#            if t >= t_asymptomatic-1:
#                for j in range(N):
#                    indicator_neighbour = A[i,j]
#                    if indicator_neighbour == 1:
#                        if location[j,t] == iW:
#                            if numpy.any( state[j,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == False:
#                                # Situation:
#                                # -----------
#                                # I have a connection who has been Infectious-Asymptomatic for the t_asymptomatic
#                                # They will now become Infectious-Symptomatic, so they might warn me to go home
#                                # !*So I update my location, but not my status*!
#                                if species[j] == iT:
#                                    if species[i] == iT:
#                                        location[i,t+1] = iH
#                                    elif species[i] == iO:
#                                        r_OFollowAdvice = numpy.random.uniform(low=0.0,high=1.0)
#                                        if r_OFollowAdvice < p_OFollowAdvice:
#                                            location[i,t+1] = iH
#                                        elif r_OFollowAdvice >= p_OFollowAdvice:
#                                            location[i,t+1] = location[i,t+1]
#                                        else: 
#                                            raise Exception
#                                    else : 
#                                        raise Exception
#
#                                elif species[j] == iO:
#                                    r_OAdviseOthers = numpy.random.uniform(low=0.0,high=1.0)
#                                    if r_OAdviseOthers < p_OAdviseOthers:
#                                        if species[i] == iT:
#                                            location[i,t+1] = iH
#                                        elif species[i] == iO:
#                                            r_OFollowAdvice = numpy.random.uniform(low=0.0,high=1.0)
#                                            if r_OFollowAdvice < p_OFollowAdvice:
#                                                location[i,t+1] = iH
#                                            elif r_OFollowAdvice >= p_OFollowAdvice:
#                                                location[i,t+1] = location[i,t+1]
#                                            else: 
#                                                raise Exception
#                                        else: 
#                                            raise Exception
#                                    elif r_OAdviseOthers >= p_OAdviseOthers:
#                                        location[i,t+1] = location[i,t+1]
#                                    else:
#                                        raise Exception
#                                else:
#                                    raise Exception
#                            elif numpy.any( state[j,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == True:        
#                                location[i,t+1] = location[i,t+1]
#                            else:
#                                raise Exception
#                        elif location[j,t] == iH:
#                            location[i,t+1] = location[i,t+1]
#                        else:
#                            raise Exception                        
#                    elif indicator_neighbour == 0:
#                        location[i,t+1] = location[i,t+1]                   
#                    else: 
#                        raise Exception
#            elif t < t_asymptomatic-1:
#                location[i,t+1] = location[i,t+1] 
#        
#        elif location[i,t] == iH:
#            location[i,t+1] = location[i,t+1]
#        else:
#            raise Exception
#    elif state[i,t] in (iR, iQ):
#        location[i,t+1] = location[i,t+1]
#    else:
#        raise Exception