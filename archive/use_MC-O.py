#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Internal modules
import visualise
import topography
import models
import output
import utils

start_time = time.time()
print("Started using data at relative time: {}\n".format(start_time - start_time))

# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")
parser.add_argument("-f", dest="parameter_file_path", required=True,
                    help="Input parameter file path for simulation", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Try to open parameter file at parameter_file_path returned by parser
try: 
    parser_args = parser.parse_args()
    # Read parameter_file and load contents as parameter_dict
    with open(parser_args.parameter_file_path, 'r') as parameter_file:
        parameters_dict = json.load(parameter_file)
except Exception:
    raise

# Define parmaeters directory 
parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_path))

# Define output file path
output_dir_path = os.path.join(parameters_dir_path, 'output')

# If 'figures' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path, 'figures')) == False:
    os.mkdir(os.path.join(parameters_dir_path, 'figures'))

# Define figures file path
figures_dir_path = os.path.join(parameters_dir_path, 'figures')

# Monte Carlo parameters
# ---------------
num_opacities    = parameters_dict["N"] + 1
num_species_runs = int(parameters_dict["N"]/5)+1 # because first no liars, then 1, ..., then N
num_state_runs   = parameters_dict["num_state_runs"]

num_simulated_species_runs = parameters_dict["num_simulated_species_runs"]

# Main
# --------------
num_key_outputs = 24

key_outputs_averages = numpy.zeros((num_key_outputs, num_species_runs))
for k in range(int(num_simulated_species_runs/5)+1):
    print("Using species_run:",k*5, "of", num_simulated_species_runs)
    
    # Define species (Reset from last species run)
    species = output.csv_to_species(i_species_run=k*5, output_dir_path=output_dir_path)


    key_outputs = numpy.zeros((num_key_outputs, num_state_runs)) # i,j is the ith key output on the rth run
    for r in range(num_state_runs):
        
        # Initialise location and state (reset state and location from last run)
        # ---------------
        state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time

        location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time

        
        # Calculate location and state of i at t+1 given information about i at t
        location, state = output.csv_to_state_location(i_species_run=k*5,i_state_location_run=r,output_dir_path=output_dir_path)

        i=0
        t=0
        location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                                            location, state, 
                                                                            iT=1, iO=-1, 
                                                                            iW=1, iH=-1,       
                                                                            iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)


        productivity_total = numpy.sum(output.productivity_workforce(parameters_dict["N"],parameters_dict["T"],
                                            location, state,
                                            iT=1, iO=-1, 
                                            iW=1, iH=-1,       
                                            iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6))

        state_indication = output.state_indicator(parameters_dict["N"],
                                                    state,
                                                    iT=1, iO=-1, 
                                                    iW=1, iH=-1,       
                                                    iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)

        # location_and_state_total[s,l] = total number of time units spent in state s and location l?
        location_and_state_count_total = numpy.sum(a=location_and_state_count, axis=1)

        # location_and_state_proportion[s,l] = proportion of time spent in state s and location l.
        location_and_state_proportion = location_and_state_count_total/(parameters_dict["N"]*parameters_dict["T"])


        # state_indication_total[s] = number of nodes that experienced state s at least once.
        state_indication_total = numpy.sum(a=state_indication, axis=1)
        
        # state_indication_proportion[s] = proportion of nodes that experienced state s at least once.
        state_indication_proportion = state_indication_total/parameters_dict["N"]

        # New key outputs:
        # ---------------
        W_S_prop  = location_and_state_proportion[0,0]
        W_E_prop  = location_and_state_proportion[1,0]
        W_IA_prop = location_and_state_proportion[2,0]
        W_IS_prop = location_and_state_proportion[3,0]
        W_R_prop  = location_and_state_proportion[4,0]
        W_C_prop  = location_and_state_proportion[5,0]

        H_S_prop  = location_and_state_proportion[0,1]
        H_E_prop  = location_and_state_proportion[1,1]
        H_IA_prop = location_and_state_proportion[2,1]
        H_IS_prop = location_and_state_proportion[3,1]
        H_R_prop  = location_and_state_proportion[4,1]
        H_C_prop  = location_and_state_proportion[5,1]

        key_outputs[0, r]  = W_S_prop
        key_outputs[1, r]  = W_E_prop
        key_outputs[2, r]  = W_IA_prop
        key_outputs[3, r]  = W_IS_prop
        key_outputs[4, r]  = W_R_prop
        key_outputs[5, r]  = W_C_prop

        key_outputs[6,  r] = H_S_prop
        key_outputs[7,  r] = H_E_prop
        key_outputs[8,  r] = H_IA_prop
        key_outputs[9,  r] = H_IS_prop
        key_outputs[10, r] = H_R_prop
        key_outputs[11, r] = H_C_prop

        S_prop  = W_S_prop + H_S_prop
        E_prop  = W_E_prop + H_E_prop
        IA_prop = W_IA_prop + H_IA_prop
        IS_prop = W_IS_prop + H_IS_prop
        R_prop  = W_R_prop + H_R_prop
        C_prop  = W_C_prop + H_C_prop

        key_outputs[12, r] = S_prop
        key_outputs[13, r] = E_prop
        key_outputs[14, r] = IA_prop
        key_outputs[15, r] = IS_prop
        key_outputs[16, r] = R_prop
        key_outputs[17, r] = C_prop

        S_ind_prop  = state_indication_proportion[0]
        E_ind_prop  = state_indication_proportion[1]
        IA_ind_prop = state_indication_proportion[2]
        IS_ind_prop = state_indication_proportion[3]
        R_ind_prop  = state_indication_proportion[4]
        C_ind_prop  = state_indication_proportion[5]

        key_outputs[18, r] = S_ind_prop
        key_outputs[19, r] = E_ind_prop
        key_outputs[20, r] = IA_ind_prop
        key_outputs[21, r] = IS_ind_prop
        key_outputs[22, r] = R_ind_prop
        key_outputs[23, r] = C_ind_prop

    key_outputs_average = numpy.sum(key_outputs, axis=1)/num_state_runs # shape (num_key_outputs, )
    key_outputs_averages[:,k] = key_outputs_average



# Splrep
S_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[18,:],  k=3)
E_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[19,:],  k=3)
IA_ind_prop_spline = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[20,:], k=3)
IS_ind_prop_spline = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[21,:], k=3)
R_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[22,:],  k=3)
C_ind_prop_spline  = interpolate.splrep(x=numpy.linspace(0,1,num_species_runs), y=key_outputs_averages[23,:],  k=3)

# Interpolate raw values by cubic spline
opacity_plot     = numpy.linspace(0, 1, 100)
S_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=S_ind_prop_spline)
E_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=E_ind_prop_spline)
IA_ind_prop_plot = interpolate.splev(x=opacity_plot, tck=IA_ind_prop_spline)
IS_ind_prop_plot = interpolate.splev(x=opacity_plot, tck=IS_ind_prop_spline)
R_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=R_ind_prop_spline)
C_ind_prop_plot  = interpolate.splev(x=opacity_plot, tck=C_ind_prop_spline)

splines = []
splines.append(S_ind_prop_plot)
splines.append(E_ind_prop_plot)
splines.append(IA_ind_prop_plot)
splines.append(IS_ind_prop_plot)
splines.append(R_ind_prop_plot)
splines.append(C_ind_prop_plot)


# Visualisation
# ----------

colors = ['green','orange','red', 'tab:pink', 'black', 'blue']
#linestyles  = ['-', '-.', '--', '-']
#key_output_names = ["Prop. WF exposed at least once",\
#                    "Max. prop. WF unwell at any time",\
#                    "Prop. time WF spent unwell",\
#                    "Prop. time WF spent productive"]

work_state_props = ["W_S_prop", \
                    "W_E_prop", \
                    "W_IA_prop", \
                    "W_IS_prop", \
                    "W_R_prop", \
                    "W_C_prop"]

home_state_props = ["H_S_prop", \
                    "H_E_prop", \
                    "H_IA_prop", \
                    "H_IS_prop", \
                    "H_R_prop", \
                    "H_C_prop"]

total_state_props = ["S", \
                     "E", \
                     "A", \
                     "U", \
                     "R", \
                     "Q"]

state_ind_props =   ["S_ind_prop", \
                     "E_ind_prop", \
                     "IA_ind_prop", \
                     "IS_ind_prop", \
                     "R_ind_prop", \
                     "C_ind_prop"]


# Plot:
# work and state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[ko, :], ls="--", color=colors[ko], label="{}".format(work_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"work-state proportions")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_workstate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot:
# home and state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[6+ko, :], ls=":", color=colors[ko], label="{}".format(home_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"home-state proportions")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_homestate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# work and home state propotions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[12+ko, :], ls="-", color=colors[ko], label="{}".format(total_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.8 + 0.01)
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.set_yticks(numpy.linspace(0,1,6))
#ax.legend(loc='upper left')
visualise.style_axes(ax, r"opacity",  r"total-state proportions")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_totalstate_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# state indication proportions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for ko in range(6):
    ax.plot(numpy.linspace(0,1,num_species_runs), key_outputs_averages[18+ko, :], ls="-", color=colors[ko], label="$x=${}".format(total_state_props[ko]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.01)
ax.set_yticks(numpy.linspace(0,1,6))
ax.legend(loc='upper left')
visualise.style_axes(ax, r"Opacity",  r"Proportion entering state $x$")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_state_ind_props_V_opacity.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot
# state indication proportions v opacity 
fig,gs,ax = visualise.plot_single_frame()

for spl in range(6):
    ax.plot(opacity_plot, splines[spl], ls="-", color=colors[spl], label="$x=${}".format(total_state_props[spl]))

#ax.set_ylim(-0.01,1.01)
#ax.set_ylim(-0.01,1.01)
ax.set_yticks(numpy.linspace(0,1,6))
ax.legend(loc='upper left')
visualise.style_axes(ax, r"Opacity",  r"Proportion entering state $x$")
visualise.save_svg(fig, gs, os.path.join(figures_dir_path, '{}_state_ind_props_V_opacity_SPL.svg'.format(parameters_dict["simulation_name"]) ) )

print("\nFinished using data at relative time:  {}".format(time.time() - start_time))