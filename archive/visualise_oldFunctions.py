

def show_graph_with_labels(adjacency_matrix, node_labels=None):
    # Create tuple of nodes that are connected : (i,j)
    i_edge, j_edge = numpy.where(adjacency_matrix == 1)
    i_edge_list = list(i_edge)
    j_edge_list = list(j_edge)
    edge_zip = zip(i_edge_list, j_edge_list)

    graph = networkx.Graph()
    graph.add_edges_from(edge_zip)
    networkx.draw(graph, node_size=500,  labels=node_labels, with_labels=True)
    # see https://www.udacity.com/wiki/creating-network-graphs-with-python for advanced drawing
    plt.show()




def show_graph_with_color(adjacency_matrix, color_map , node_shape_map, node_labels=None):
    # Create tuple of nodes that are connected : (i,j)
    i_edge, j_edge = numpy.where(adjacency_matrix == 1)
    i_edge_list = list(i_edge)
    j_edge_list = list(j_edge)
    edge_zip = zip(i_edge_list, j_edge_list)

    graph = networkx.Graph()
    graph.add_edges_from(edge_zip)
    return networkx.draw(graph, node_size=500,  labels=node_labels, with_labels=True, node_color = color_map, node_shape = node_shape_map)




def create_color_map(N, t,
                     state, 
                     iS, iE, iI, iA):
    """
    """
    color_map = list(numpy.zeros(N))
    for i in range(N):
        if state[i,t] == iS:
            color_map[i] = 'blue'
        elif state[i,t] == iE:
            color_map[i] = 'orange'
        elif state[i,t] == iI:
            color_map[i] = 'red'
        elif state[i,t] == iA:
            color_map[i] = 'green'
        else :
            raise Exception
    return color_map



def draw_graph_2(adjacency_matrix,
               N, t,
               state, 
               iS, iE, iI, iA,
               species,
               iT, iO, 
               labels=None, graph_layout='shell',
               node_size=500, node_alpha=1.0,
               node_text_size=12,
               edge_color='black', edge_alpha=1.0, edge_tickness=1.5,
               edge_text_pos=0.3,
               text_font='sans-serif'):


    # create networkx graph
    G = networkx.Graph()

    # create list of edge tuples from adjacency matrix
    i_edge, j_edge = numpy.where(adjacency_matrix == 1)
    i_edge_list    = list(i_edge)
    j_edge_list    = list(j_edge)

    edge_zip       = zip(i_edge_list, j_edge_list)
    edge_list      = list(edge_zip)

    # add edges to graph
    for edge in edge_list:
        G.add_edge(edge[0], edge[1])

    # Define graph position (layout)
    # these are different layouts for the network you may try
    # shell seems to work best
    if graph_layout == 'spring':
        graph_pos=networkx.spring_layout(G)
    elif graph_layout == 'spectral':
        graph_pos=networkx.spectral_layout(G)
    elif graph_layout == 'random':
        graph_pos=networkx.random_layout(G)
    else:
        graph_pos=networkx.shell_layout(G)


    # Draw nodes
    for i in range(N):

        # Choose color
        if state[i,t] == iS:
            node_color = 'green'
        elif state[i,t] == iE:
            node_color = 'orange'
        elif state[i,t] == iI:
            node_color = 'red'
        elif state[i,t] == iA:
            node_color = 'lightgrey'
        else :
            raise Exception

        # Choose shape
        if species[i] == iT:
            node_shape = 'o'
        elif species[i] == iO:
            node_shape = 's'
        else:
            raise Exception

        networkx.draw_networkx_nodes(G,graph_pos,node_size=node_size, nodelist = [i],
                                   alpha=node_alpha, node_color=node_color, 
                                   node_shape=node_shape)

    # Draw edges
    networkx.draw_networkx_edges(G,graph_pos,width=edge_tickness,
                           alpha=edge_alpha,edge_color=edge_color)
    
    # Draw node labels
    networkx.draw_networkx_labels(G, graph_pos,font_size=node_text_size,
                            font_family=text_font)

    # Draw edge labels
    #if labels is None:
    #    labels = range(len(graph))

    #edge_labels = dict(zip(graph, labels))
    #networkx.draw_networkx_edge_labels(G, graph_pos, edge_labels=edge_labels, 
    #                            label_pos=edge_text_pos)
    
    return G





