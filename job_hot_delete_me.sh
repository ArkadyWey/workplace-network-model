# !/bin/bash

cwd=`pwd`
run="run_MC-O_delete_me.py"
flag=f

for d in 0.030 0.040 0.050 0.060 0.070 0.080 0.090 0.100; do
    arg="/scratch/wey/benefits_of_transparency/hot_T-100_NSR-21_NSLR-50_NP/Sim-MC-O_d-${d}/parameters.json"
    ./${run} -${flag} ${arg}
done