# !/bin/bash

cwd=`pwd`
run="run_MC-O.py"
flag=f

for d in 0.030 0.040 0.050 0.060; do
    arg="/scratch/wey/benefits_of_transparency/method-2_beta-010_NSLR-100/hot_T-100_NSR-51_NSLR-100_beta-010/Sim-MC-O_d-${d}/parameters.json"
    ./../${run} -${flag} ${arg}
done