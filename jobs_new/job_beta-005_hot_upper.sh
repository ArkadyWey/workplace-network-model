# !/bin/bash

cwd=`pwd`
run="run_MC-O.py"
flag=f

for d in 0.070 0.080 0.090 0.100; do
    arg="/scratch/wey/benefits_of_transparency/method-2_beta-005_NSLR-100/hot_T-100_NSR-51_NSLR-100_beta-005/Sim-MC-O_d-${d}/parameters.json"
    ./../${run} -${flag} ${arg}
done