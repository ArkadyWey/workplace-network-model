#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External 
import numpy
import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Internal
import visualise
import topography
import models
import output
import utils

# -----------------------------------------------
# README
# use_MC-O_bar module: 
# Use output of the run_MC-O module to create bar plots found in V_State folders.
# For example, this code was used to generate the plots in the V_State folders:
#  ./figures/cold/V_State 
#  ./figures/hot/V_State 

# -----------------------------------------------


start_time = time.time()
print("Started using data at relative time: {}\n".format(start_time - start_time))

# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")

# Add all parameter file paths
# - Each degree run is a separate parameter file
# - So to use more than one degree, add parameter file for each degree
# - (see ./tests/test_run_MC-O for correct directory structure)
parser.add_argument("-f", dest="parameter_file_paths", nargs="*", required=True,
                    help="Input parameter file paths (wich hold degree info)", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Add k vals
# - k is the index of the opacity run
# - For example, "-k 0 50 100" would use species runs 0, 50, 100
# - So would plot graphs for 0 50 and 100 % workforce opacity (assuming that species data files are named with convention)
# - (see ./tests/test_run_MC-O for example data files)
parser.add_argument("-k", dest="k_vals", nargs="*", required=True,
                    help="Input opacity indices k", metavar="N",
                    type=int)

# Return parser arguments
parser_args = parser.parse_args()
parameter_file_paths = parser_args.parameter_file_paths
k_vals = parser_args.k_vals

# Define number of degrees (from number of parameter files in parser)
num_degs = len(parameter_file_paths)

# Define number of opacity vals (number of k values in parser)
num_k_vals = len(k_vals)
# Note: Read k_vals as k vals

# Number of statistics to analyse - 
# Here we look at:
# 0: location_and_state_count_timeProp (defined below)
num_stats = 1

# Define empty lists to save each run (defined below)
average_stats_list = []
std_stats_list = []
degree_list = []
for i_deg in range(num_degs):

    # Try to open ith parameter file from parameter_file_paths returned by parser
    try:    
        # Read parameter_file and load contents as parameter_dict
        with open(parser_args.parameter_file_paths[i_deg], 'r') as parameter_file:
            parameters_dict = json.load(parameter_file)
    except Exception:
        raise
    
    # Add degree in current parameter file to degree list 
    # Since d is degree probability (prob of connection between two nodes), multiply by 100 to get degree
    # degre_list[i] = degree corresponding to ith parameter file.
    degree_list.append(parameters_dict["d"]*100)

    # Monte Carlo parameters
    # ---------------
    num_opacities    = parameters_dict["N"] + 1
    num_state_runs   = parameters_dict["num_state_runs"]

    # Directories 
    parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_paths[i_deg]))
    print(parameters_dir_path)
    output_dir_path = os.path.join(parameters_dir_path, 'output')  


    # Run Monte Carlo
    # ------------
    # average_stats[st, k, t] = average of statistic st (avd over state runs) at opacity k at time t    
    average_stats = numpy.zeros((num_stats, num_k_vals, 6, 2))
    std_stats     = numpy.zeros((num_stats, num_k_vals, 6, 2))
    for i_k in range(num_k_vals):
        print("Using species run index k:", k_vals[i_k], "of", num_k_vals-1)

        # Define the opacity run/species run index
        k = k_vals[i_k]

        # Define species (Reset from last species run)
        species = output.csv_to_species(i_species_run=k, output_dir_path=output_dir_path)

        # stats[st, s,l,r] = statistic st of state s at location l on rth model run
        stats = numpy.zeros((num_stats, 6, 2, num_state_runs))
        # r indexes the model run (inner loop)
        for r in range(num_state_runs):

            # Initialise location and state (reset state and location from last run)
            # ---------------
            state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
            location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time

            # Calculate location and state of i at t+1 given information about i at t
            location, state = output.csv_to_state_location(i_species_run=k, i_state_location_run=r, output_dir_path=output_dir_path)


            # Calculate stats
            # ----------------
            # -------------------------------------------------------------------------------------------------
            # Define stats

            # location_and_state_count[s,t,l] = Number of people in state s and location l at time t 
            location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                                         location, state,
                                                                         iT=1, iO=-1, 
                                                                         iW=1, iH=-1,       
                                                                         iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            # location_and_state_count_prop[s,t,l] = proportion of nodes in state s and location l at time t
            location_and_state_count_prop = location_and_state_count/parameters_dict["N"]

            # location_and_state_count_timeProp[s,l] = proportion of time units that nodes spent in state s and location l
            location_and_state_count_timeProp = numpy.sum(a=location_and_state_count_prop, axis=1)/parameters_dict["T"]
            
            # Record stats
            stats[0,:,:,r] = location_and_state_count_timeProp

    
            # -------------------------------------------------------------------------------------------------
            # -------------------------------------------------------------------------------------------------
        # Take the mean of the stats over all model runs
        location_and_state_count_timeProp_mean = numpy.mean(a=stats[0,:,:,:], axis=2)

        # Add mean stats to average_stats
        # average_stats[0,i_k,s,l] = average proportion of time units that nodes spent in state s and location l (at opacity with index k)
        average_stats[0,i_k,:,:] = location_and_state_count_timeProp_mean

        # ------------------------------------------------------------------------------------------------------------
        # Calculate the standardd deviation of the stats over all model runs (used for error bars)

        # std_stats[0,i_k,s,l] = standard deviation (over model runs) proportion of time units that nodes spent in state s and location l (at opacity with index k)  
        std_stats[0,i_k,:,:] = numpy.std(a=stats[0,:,:,:], axis=2)

        # ------------------------------------------------------------------------------------------------------------
    # average_stats_list[i_deg, 0, i_k, s, l] = mean (over model runs) percentage of time units that nodes spent in state s and location l at opacity with index k at degree with index i_deg
    average_stats_list.append(100*average_stats)
    # std_stats_list[i_deg, 0, i_k, s, l] = standard deviation (over model runs) percentage of time units that nodes spent in state s and location l at opacity with index k at degree with index i_deg
    std_stats_list.append(100*std_stats)

# Convert list to array
# average_stats_array[i_deg, 0, i_k, s, l] = mean (over model runs) percentage of time units that nodes spent in state s and location l at opacity with index k at degree with index i_deg
average_stats_array = numpy.asarray(average_stats_list) 
# std_stats_array[i_deg, 0, i_k, s, l] = standard dev (over model runs) percentage of time units that nodes spent in state s and location l at opacity with index k at degree with index i_deg
std_stats_array    = numpy.asarray(std_stats_list) 

# lower_stats_array[i_deg, st, i_k] = 1 standard dev below mean (over model runs) of statitic
# higher_stats_array[i_deg, st, i_k] = 1 standard dev above mean (over model runs) of statitic 
lower_stats_array = average_stats_array - std_stats_array
upper_stats_array = average_stats_array + std_stats_array



# Plotting
# -------------------
colors      = ['green','red', 'black']
#linestyles  = [':', '-.', '--', '-', '--']
state_names = ['Well', "Sick", "Recovered"]


# If 'paper' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path,"./../" , 'paper')) == False:
    os.mkdir(os.path.join(parameters_dir_path, "./../",'paper'))
# Define paper file path
paper_dir_path = os.path.join(parameters_dir_path, "./../",  'paper')

# If 'V_Degree' directory does not exist in paper directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(paper_dir_path , 'V_State')) == False:
    os.mkdir(os.path.join(paper_dir_path,'V_State'))
# Define paper file path
V_State_dir_path = os.path.join(paper_dir_path, 'V_State')

# -------------------------------------------------------------------------------------------------------------------


# ill_array[d,o,l] = proportioon of time spent ill at location l, degree d, opacity o
ill_array = average_stats_array[:,0, :, parameters_dict["iE"] - 1,:] + \
            average_stats_array[:,0, :, parameters_dict["iIA"]- 1,:] + \
            average_stats_array[:,0, :, parameters_dict["iIS"]- 1,:]
# well_array[d,o,l] = proportion of time spent susceptible or quarentined at l, deg d, opacity o
well_array = average_stats_array[:,0, :, parameters_dict["iS"] - 1,:] + \
            average_stats_array[ :,0, :, parameters_dict["iQ"]- 1,:]
# rec_array[d,o,l] = prop time spent recovered at location l deg d opacity o
rec_array = average_stats_array[:, 0, :, parameters_dict["iR"] - 1,:]

# summary stats[d,o,l,st] = proportion of time spent in sumary stat st at location l at deg d at opa o.
summary_stats = numpy.zeros((num_degs, num_k_vals, 2, 3))
summary_stats[:,:,:,0] = well_array
summary_stats[:,:,:,1] = ill_array
summary_stats[:,:,:,2] = rec_array

# Plot: Proportion of time spent in state vs state (work and home)
for i_deg in range(num_degs):
    for i_k in range(num_k_vals): 

        fig,gs,ax = visualise.plot_single_frame(plot_grid=False)
        ax.bar(state_names, summary_stats[i_deg, i_k, 0, :], width=1, align='center', bottom=0, edgecolor='white', fill=True, ls='-', color=colors, alpha=1.0)
        ax.bar(state_names, summary_stats[i_deg, i_k, 1, :], width=1, align='center', bottom=summary_stats[i_deg, i_k, 0, :], edgecolor='white', fill=True, ls='-', color=colors, alpha=0.5)

        ax.set_ylim(-0.0,100 + 0.00)
        #ax.set_xticks(numpy.arange(0,T+1,1))
        visualise.style_axes(ax, r"State",  r"% time") 
        visualise.save_svg(fig, gs, os.path.join(V_State_dir_path, 'TimeProp_V_State_{:d}_o-{:d}.svg'.format(int(degree_list[i_deg]), k_vals[i_k]) ))

print("\nFinished using data at relative time:  {}".format(time.time() - start_time))