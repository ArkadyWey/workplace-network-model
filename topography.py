#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy
import random as random_module
import scipy
from scipy import linalg

# ----------------------------------------------------------------------------
# README:
# topography module@
# Functions that define adjacency matrices of some possible workplace networks.
# ------------------------------------------------------------------------------


def specify_prob(N,numbers_list=[0,1], prob_of_number=[0.6, 0.4]):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    where the probability that A[i,j]=numbers_list[k] is given by prob_of_number[k].

    Note that sum_j(prob_of_number) = 1, and numbers_list=[0,1] for
    meaningful adjacency matrix, where A[i,j]=1 indicates a link between
    node i and node j, and A[i,j]=0 indicates no link between node i and node j.

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    numbers_list: numpy.ndarray like
        List of numbers to fill matrix - should be [0,1] for adjacnecy matrix
    prob_of_number: numpy.ndarray like
        List of associated probabilities of numbers_list - should sum to 1 for adjacency matrix
    """
    array_2    = numpy.random.choice(a=numbers_list, size=(N,N), p=prob_of_number)
    array_tril  = numpy.tril(array_2, k=-1)
    array_trans = numpy.transpose(array_tril)
    A = array_tril + array_trans
    return A





# Some other examples of possible adjacency matrix generators:
# ----------------------------------------------------------------

def random(N):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    where the probability that A[i,j]=(low,high-1) is 0.5. 
    
    Note that low=0, high-1=1
    for meaningful adjacency matrix.

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    low: int
        smallest number to fill A - should be 0
    high: int
        highest number to fill A + 1 - shouldbe 2, so that A[i,j]= 0 or 1
    """
    random_matrix = numpy.random.randint(low=0, high=2, size=(N,N))
    random_tril   = numpy.tril(random_matrix, k=-1)
    random_trans  = numpy.transpose(random_tril)
    A             = random_tril + random_trans  
    return A    
    
def chain(N):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    where A[i,j]=1 if j = i-1 and zero otherwise.
    That is, create the adjacency matrix for a chain network.

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    """
    array_2 = numpy.diag(numpy.ones((N-1)), k=-1)
    array_tril = numpy.tril(array_2, k=-1)
    array_trans = numpy.transpose(array_tril)
    A = array_tril + array_trans
    return A

def connected(N):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    where A[i,j]=1 for all i, j.
    That is, create the adjacency matrix for a connected network.

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    """
    array_2 = numpy.ones(N)
    array_tril = numpy.tril(array_2, k=-1)
    array_trans = numpy.transpose(array_tril)
    A = array_tril + array_trans
    return A


def islands_2(N):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    corresponding to a network where nodes are fully connected 
    in two "islands", with a single link between these two "islands".

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    """
    half_N = int(0.5*N)
    array_2 = numpy.zeros((N,N))

    if N%2 == 0:
        sub_array_2     = numpy.ones((half_N,half_N))
        sub_array_tril  = numpy.tril(sub_array_2, k=-1)
        sub_array_trans = numpy.transpose(sub_array_tril)
        sub_A = sub_array_tril + sub_array_trans

        array_2[0:half_N,0:half_N] = sub_A
        array_2[half_N::, half_N::] = sub_A
        array_2[half_N, half_N-1] = 1

        array_tril = numpy.tril(array_2)
        array_trans = numpy.transpose(array_tril)
        A = array_tril + array_trans
    
    elif N%2 == 1:
        upper_array_2 = numpy.ones((half_N,half_N))
        upper_array_tril = numpy.tril(upper_array_2, k=-1)
        upper_array_trans = numpy.transpose(upper_array_tril)
        upper_A = upper_array_tril + upper_array_trans

        lower_array_2 = numpy.ones((half_N+1,half_N+1))
        lower_array_tril = numpy.tril(lower_array_2, k=-1)
        lower_array_trans = numpy.transpose(lower_array_tril)
        lower_A = lower_array_tril + lower_array_trans

        array_2 = numpy.zeros((N,N))
        array_2[0:half_N,0:half_N] = upper_A
        array_2[half_N::, half_N::] = lower_A
        array_2[half_N, half_N-1] = 1

        array_tril = numpy.tril(array_2)
        array_trans = numpy.transpose(array_tril)
        A = array_tril + array_trans
    
    else: 
        raise Exception
    return A


def islands_K(N,K):
    """
    Description
    ------------
    Create symmetric N x N matrix A with zeros on leading diagonal, 
    corresponding to a network where nodes are fully connected 
    in K "islands", with a single link between the kth and k+1th "islands".

    Parameters
    -----------
    N: int
        Number of nodes (size of square matrix)
    K: int
        Number of islands - K must divide N.
    """
    divider = N%K
    if divider != 0:
        print("Stop: K doesn't divide N exactly.")
        raise Exception

    num_nodesInIsland = int(N/K)
    island_matrix     = numpy.random.choice([0,1], size=(num_nodesInIsland,num_nodesInIsland), p=[0.3,0.7])
    island_tril       = numpy.tril(island_matrix, k=-1)
    island_trans      = numpy.transpose(island_tril)
    island_final      = island_tril + island_trans

    A = island_final
    for i in range(K-1):
        A  = linalg.block_diag(A, island_final)


    filled_matrix=numpy.ones((K,K))
    numpy.fill_diagonal(a=filled_matrix, val=0)


    for j in range(K):
        for i in range(K):
            A[i*num_nodesInIsland,j*num_nodesInIsland] = filled_matrix[i,j]

    return A



    






