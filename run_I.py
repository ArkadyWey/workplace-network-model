#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# External
import numpy 
import os
import argparse
import json 
import sys

# Plotting
import matplotlib as mpl
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
numpy.set_printoptions(threshold=sys.maxsize)

# Internal
import visualise
import topography
import models
import output
import utils


# -----------------------------------------------
# README:

# run_I  (run-inner) module:
# Take a json file with a dictionary of parameters as a parser argument
# Initialise node locations and states
# Define species vector, the opacity of each node.

# For this opacity set, run the model on a randomised network once.
# This is the inner loop which runs the model that is run many times in the run_MC-O (run-montecarlo-opacity) module.
# Create a plot of the network, a plot of the adjacency matrix.

# See test_run_I for an example.
# -----------------------------------------------

# Input and Output
# -----------------

# Add parameter_file_path as parser requirement
# Use parameters.json file in test_run_I for an example.
parser = argparse.ArgumentParser(description="Parameter file path")
parser.add_argument("-f", dest="parameter_file_path", required=True,
                    help="Input parameter file path for simulation", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Open parameter file at parameter_file_path returned by parser
try: 
    parser_args = parser.parse_args()
    # Read parameter_file and load contents as parameter_dict
    with open(parser_args.parameter_file_path, 'r') as parameter_file:
        parameters_dict = json.load(parameter_file)
except Exception:
    raise

# Define output directory 
parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_path))


# Initial conditions
# ---------------
d                     = parameters_dict["d"]
#A                     = topography.specify_prob(parameters_dict["N"],numbers_list=[0,1], prob_of_number=[1.0-d, d])
A                     = topography.connected(parameters_dict["N"])


species               = parameters_dict["iT"]*numpy.ones(parameters_dict["N"]) # assigned types 1 or -1 (T, L -- Truer or Liar)
species[0:5]          = parameters_dict["iO"]*numpy.ones(5)

state_initial         = parameters_dict["iS"]*numpy.ones(parameters_dict["N"]) # assigned states 1,2,3,4  (S, E, I, A -- Susceptible, Exposed, Infectious, Absent)
state_initial[0]      = parameters_dict["iE"]

location_initial      = parameters_dict["iW"]*numpy.ones(parameters_dict["N"])
location_initial[0:5] = parameters_dict["iW"]*numpy.ones(5)

# Initialise
# ---------------
state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
state[:,0]    = state_initial

location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
location[:,0] = location_initial

# Main
# ---------------

for t in range(parameters_dict["T"]-1): # T-1 because the t+1th time is given by the tth time.
    for i in range(parameters_dict["N"]):
        location[i,t+1], state[i,t+1] = models.triple_status_six_state(
                                                        A, 
                                                        parameters_dict["N"], parameters_dict["T"],
                                                        species, location, state,
                                                        i, t, 
                                                        parameters_dict["t_incubation"], 
                                                        parameters_dict["t_asymptomatic"], 
                                                        parameters_dict["t_symptomatic"], 
                                                        parameters_dict["t_quarantined"], 
                                                        parameters_dict["p_unlucky"], 
                                                        parameters_dict["p_spread"],
                                                        parameters_dict["p_neverUnwell"], 
                                                        parameters_dict["p_recovery"], 
                                                        parameters_dict["p_OAdviseOthers"], 
                                                        parameters_dict["p_OFollowAdvice"],
                                                        parameters_dict["iT"], 
                                                        parameters_dict["iO"], 
                                                        parameters_dict["iW"],
                                                        parameters_dict["iH"],       
                                                        parameters_dict["iS"], 
                                                        parameters_dict["iE"], 
                                                        parameters_dict["iIA"], 
                                                        parameters_dict["iIS"], 
                                                        parameters_dict["iR"], 
                                                        parameters_dict["iQ"]
                                                        )
                            

# Define directory for figures 
if os.path.exists(os.path.join(parameters_dir_path, 'network')) == False:
    os.mkdir(os.path.join(parameters_dir_path, 'network'))


# Plot: Dynamic network
# --------------------------

#node_indices    = range(parameters_dict["N"])
#node_identities = range(parameters_dict["N"])
#labels          = dict(zip(node_indices, node_identities))

for t in range(parameters_dict["T"]):
    
    fig,gs,ax = visualise.plot_single_frame(plot_grid=False)
    ax.text(0.8, 0.95, "Day: {}".format(t), fontsize=15)

    ax.tick_params(
        axis='both',       # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        left=False,        # ticks along the top edge are off
        right=False,       # ticks along the top edge are off
        labelbottom=False, # labels along the bottom edge are off
        labelleft=False)   # labels along the bottom edge are off

    visualise.draw_graph(A,
                         parameters_dict["N"], t,
                         species, location, state, 
                         iT=1, iO=-1, 
                         iW=1, iH=-1,       
                         iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6,
                         labels=None, graph_layout='shell',
                         node_size=500, node_alpha=1.0,
                         node_text_size=12,
                         edge_color='black', edge_alpha=1.0, edge_tickness=1.5,
                         edge_text_pos=0.3,
                         text_font='sans-serif'
                        )
    visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, "network",'{}_network_t={}.svg'.format(parameters_dict["simulation_name"],t) ) )


# Example: Output
# ---------------
# location_and_state_count[s,t,l] = number of people in state s and location l at time t.
location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                            location, state,
                                                            iT=1, iO=-1, 
                                                            iW=1, iH=-1,       
                                                            iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
# Example: Using output
# ---------------

# location_and_state_count_total[s,l] = total number of time units spent in state s and location l
location_and_state_count_total = numpy.sum(a=location_and_state_count, axis=1)

# location_and_state_proportion[s,l] = proportion of time spent in state s and location l.
location_and_state_proportion = location_and_state_count_total/(parameters_dict["N"]*parameters_dict["T"])

# Check that the total number of time units spent in all states and locations is N*T
location_and_state_count_check_1 = numpy.sum(a=location_and_state_count_total, axis=0)
location_and_state_count_check = numpy.sum(a=location_and_state_count_check_1, axis=0)
print("location_and_state_count_check :  \n{}".format(location_and_state_count_check) )

# Check that proportion of time unit spent in all states and locations is 1.
location_and_state_proportion_check = sum(sum(location_and_state_proportion))
print("location_and_state_proportion_check: {}".format(location_and_state_proportion_check))

# Example: Using output
# ---------------
# Did node i experience state s?

# state_indication[s,i] = indication of whether node i experienced state s
state_indication = output.state_indicator(parameters_dict["N"],
                    state,
                    iT=1, iO=-1, 
                    iW=1, iH=-1,       
                    iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)

# Example: Using output
# ---------------
# state_indication_total[s] = number of nodes that experienced state s.
state_indication_total = numpy.sum(a=state_indication, axis=1)
print("state_indication_total :  \n{}".format(state_indication_total) )

# state_indication_proportion[s] = proportion of nodes that experienced state s
state_indication_proportion = state_indication_total/parameters_dict["N"]
print("state_indication_proportion :  \n{}".format(state_indication_proportion) )


# Visualise
# ---------------


colors      = ['green','orange','red', 'tab:pink', 'black', 'blue']
#linestyles  = [':', '-.', '--', '-', '--']
state_names = ['S', 'E','IA', 'IS', 'R','C']

# Plot: Adjacency matrix
# -----------------------
fig,gs,ax = visualise.plot_single_frame()

plt.spy(A)
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_adj.svg'.format(parameters_dict["simulation_name"]) ) )

## Plot: Number of Nodes V Time
# -------------------------------
#fig,gs,ax = visualise.plot_single_frame()

#for s in range(6):
#    ax.plot(range(parameters_dict["T), state_count[s,:], c=colors[s],  label="s = {}".format(state_names[s]))
#
#ax.set_xlim(-0.0,parameters_dict["parameters_dict["T - 1 + 0.5)
#ax.set_xticks(numpy.arange(0,parameters_dict["T+1,5))
#ax.legend()
#visualise.style_axes(ax, r"Time ($t$) [d]",  r"No. nodes in state $s$ ($n_s$) [-]")
#visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_NoNodes_V_Time'.format(simulation_name) ) )


# Plot: State V Time plot
# -------------------------
fig,gs,ax = visualise.plot_single_frame()

for i in range(parameters_dict["N"]):
    ax.plot(range(parameters_dict["T"]),  state[i,:], label="i={}".format(i))

#ax.set_ylim(-0.5,parameters_dict["T - 1 + 0.5)
ax.set_yticks(numpy.arange(0,5,1))
ax.legend()
visualise.style_axes(ax, r"Time ($t$) [d]",  r"State ($s$) [-]")
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_State_V_Time.svg'.format(parameters_dict["simulation_name"]) ) )


# Plot: Proportion V State bar plot
# -----------------------------------
fig,gs,ax = visualise.plot_single_frame(plot_grid=False)
ax.bar(state_names, location_and_state_proportion[:,0], width=1, align='center', bottom=0, edgecolor='white', fill=True, ls='-', color=colors, alpha=1.0)
ax.bar(state_names, location_and_state_proportion[:,1], width=1, align='center', bottom=location_and_state_proportion[:,0], edgecolor='white', fill=True, ls='-', color=colors, alpha=0.5)

ax.set_ylim(-0.0,1 + 0.00)
#ax.set_xticks(numpy.arange(0,T+1,1))
visualise.style_axes(ax, r"State ($s$) [-]",  r"Proportion ($P$) [-]") 
visualise.save_svg(fig, gs, os.path.join(parameters_dir_path, '{}_Proportion_V_State.svg'.format(parameters_dict["simulation_name"]) ) )


