#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy

def triple_status_six_state(A, 
                            N, T,
                            species, location, state,
                            i, t, 
                            t_incubation, t_asymptomatic, t_symptomatic, t_quarantined, 
                            p_unlucky, p_spread, p_neverUnwell, p_recovery, p_OAdviseOthers, p_OFollowAdvice,
                            iT=1, iO=-1, 
                            iW=1, iH=-1,       
                            iS=1, iE=2, iIA=3, iIS=4, iR=5, iQ=6
                            ):
    """
    Description
    ------------
    Model includes states: Susceptible, Exposed, Infectious-Asymptomatic, Infectious-Symptomatic, Quarantined, Recovered.
    Based on the state X of node i at time t, model sends node i to state Y at time t+1, stochastically,
    Model is best described by the Figure 1 in paper @ https://arxiv.org/pdf/2007.03283.pdf.
    A brief summary of each rule is below:

    # state[i,t] == 'Susceptible'
    # ---------------------------
    # If i has Infectious connections in work, then the prob that it catches virus is p_spread + p_unlucky
    # Otherwise i catches the virus with the underlying probability p_unlucky.
    # When the virus is caught, i stays in Work (because not Symptomatic yet so doesn't know it has virus), but
    # moves to the Exposed state (not Infectious yet).

    # state[i,t] == 'Exposed'
    # ---------------
    # Once the Incubation Time has passed:
    # i stays in current location (becasue not Symptomatic yet so doesn't know infected)
    # and i becomes Exposed

    # state[i,t] == 'Infectious-Asymptomatic'
    # ---------------
    # Once the Asymptomatic Time has passed:
    # If i is Transparent, then i becomes Infectious Symptomatic and goes Home
    # If i is Opaque, then i becomes Infectious Symptomatic and goes home with probability p_OAdviseOthers
    # (otherwise i will stay in Work, even though it has Symptoms)
    # This in unless i is an asymptomatic carrier, and never gets symptomatic, in which case i never knows it 
    # has the virus, and so remains Asymptomatic and in Work (and so spreading the virus)

    # state[i,t] == 'Infectious-Symptomatic'
    # (Should be Opaques at Work only now)
    # ---------------
    # Once Symptomatic Time has passed:
    # Opaques at work stay in work, and have a probability p_recovery of becoming Recovered (immune), 
    # otherwise they are Susceptible to cathcing the virus again.
    # Transparents at Home move to teh Quarantined state - they remain at Home, but are no longer Symptomatic.

    # state[i,t] == 'Quarantined'
    # ---------------
    # Once the Quarantine Time has passed:
    # Transparents ransparents now have a probability p_recovery or becoming immune (just as the Opaques did).
    # If not, then Transparents move back to the Suscpetible state and have a probability of re-infection (just as the Opaques did.)

    # state[i,t] == 'Recovered'
    # ---------------
    # If i is Recovered (Immune), then they stay in this state for all time 
    # and stay at Work for all time.
    # This is therefore the situation where Immune people know they are immune.

    
    # Check neighbours (to see if I need to be Home)
    # If state[i] != 'Home Infectious-Symp or Home Infectious-Asymp or Recovered':
    # This may modify state[i,t+1] to 'Home Asymp', or else leave it as <dictated above>.
    # Could have it so that Recovered people have to go home too (case where people don't know they're Immune)
    # ---------------
    # If i in Work and not Recovered or Quarantined:
    # If a connection j of i is Symptomatic tomorrow, 
    # Then if this connection j is Transparent, they warn i tomorrow morning not to come into work
    # So i's t+1 location gets modified to Home, as long as i is a Transparent
    # If i is Opaque though, i may choose to ignore this advice, and will only isolate due to this connection's advice 
    # with probability p_OFollowAdvice.
    # If the connection j is Opaque, then they will only advise others that they are Symptomatic tomorrow with probability p_OAdviseOthers 
    # (i.e., the same probability that j stays Home itself when it's Symptomatic).

    Parameters
    -----------
    A: numpy.ndarray
        Adjacency matrix of workplace network
    N: int
        Number of nodes
    T: int
        Number of times
    species: numpy.ndarray
        species[i] = opacity of node i
    location: numpy.ndarray
        location[i,t] = location of node i at time t
    state: numpy.ndarray
        state[i,t] = state of node i at time t
    i: int
        Node index
    t: int
        Time index
    t_incubation: float
        Incubation time - $t_E$ in paper - time before infectious
    t_asymptomatic: float
        Asymptomatic time - $t_A$ in paper - time while asymptomatic but infectious
    t_symptomatic: float
        Symptomatic time - $t_U$ in paper - time spent symptomatc (unwell) and infectious
    t_quarantined: float
        Quarantined time - $t_Q$ in paper - time spent in quarantine after symptoms stop
    p_unluckly: float
        Probability of community infectious - $\alpha$ in paper
    p_spread: float
        Probability of infection from an infectious connection - $\beta$ in paper
    p_neverUnwell: float
        Probability of never becoming symptomatic - $1-\gamma$ in paper
    p_recovery: float
        Probability of gaining immunity after infection - $\delta$ in paper
    p_OAdviseOthers: float
        Probability that opaque isolates due to symptoms (and advises connections of infection) - $\zeta$ in paper
    p_OFollowAdvice: float
        Probability that opaque isolates due to contact (follows advice) - $\epsilon$ in paper
    iT: int
        Integer representing Transparents (we use 1) 
    iO: int
        Integer representing Tpaques (we use -1)           
    iW: int
        Integer representing Work (we use 1)  
    iH: int
        Integer representing Home (we use -1)                                    
    iS: int
        Integer representing Susceptible (we use 1)  
    iE: int
        Integer representing Exposed (we use 2)  
    iIA: int
        Integer representing Infectious-Asymptomatic (we use 3)
    iIS: int
        Integer representing Infectious-Symptomatic (we use 4) 
    iR: int
        Integer representing Recovered (we use 5) 
    iQ: int
        Integer representing Quarantined (we use 6)
    
    Returns
    --------
    location[i,t+1]: int
        Location of node i at time t+1
    state[i,t+1]: int
        State of node i at time t+1
    """
    
    # state[i,t] == 'Susceptible'
    # ---------------------------
    # If i has Infectious connections in work, then the prob that it catches virus is p_spread + p_unlucky
    # Otherwise i catches the virus with the underlying probability p_unlucky.
    # When the virus is caught, i stays in Work (because not Symptomatic yet so doesn't know it has virus), but
    # moves to the Exposed state (not Infectious yet).
    if state[i,t] == iS:
        if location[i,t] == iW:
            
            # Calculate prob that I catch virus off Infectious connections
            # -----------------------------------------------------------
            # If in work then chance of catching off Infectious people
            indicator_infectiousConnection = numpy.zeros(N)
            for j in range(N):
                # indicator_infectious = 1 if j Infectious
                #                        0 else
                indicator_infectious = (state[j,t] == (iIA or iIS) )
                # indicator_infectiousNeighbour = 1 if j is Infectious and a neighbour (A[i,j]=1)
                #                               = 0 else
                indicator_infectiousNeighbour = A[i,j]*indicator_infectious
                # indicator_infectiousConnection[j] = 1 if j is Infectious and a neighbour and in Work (implying connection, since i in Work)
                #                                   = else
                indicator_infectiousConnection[j] = indicator_infectiousNeighbour*(location[j,t] == iW)
            
            # Model 1:
            # If infectious connection then probability that i catch virus given by p_spread + p_unlucky
            # Else probability that i catch virus given by p_unlucky
            #if numpy.any(indicator_infectiousConnection) == True:
            #    p_catch_i_unnormed = p_spread + p_unlucky
            #elif numpy.any(indicator_infectiousConnection) == False:
            #    p_catch_i_unnormed = p_unlucky
            #else: 
            #    raise Exception
            
            # Model 2:
            # If at least one infectious connection then probaility that i catch virus given by 
            # number of infectious connections times prob of catching it off each + underlying prob
            if numpy.any(indicator_infectiousConnection) == True:
                p_catch_i_unnormed = p_spread*numpy.sum(a=indicator_infectiousConnection, axis=0) + p_unlucky
            elif numpy.any(indicator_infectiousConnection) == False:
                p_catch_i_unnormed = p_unlucky
            else: 
                raise Exception

            # Define p_catch_i (prob that i catches it)
            # Could have p_catch_i_unnormed = numpy.sum(a=indicator_infectiousNeighbour*p_spread, axis=0) + p_unlucky so 
            # that more likely to catch it the more infectious neighbours
            # However, define by normalising p_catch_i_unnormed to make sure probability does not exceed 1
            if p_catch_i_unnormed < 1:
                p_catch_i = p_catch_i_unnormed
            elif p_catch_i_unnormed >= 1:
                p_catch_i = 1.0
            else:
                raise Exception
            # -----------------------------------------

            if species[i] in (iT, iO):
                r_catch = numpy.random.uniform(low=0.0,high=1.0)
                if r_catch < p_catch_i:
                    location[i,t+1] = iW
                    state[i,t+1]    = iE
                elif r_catch >= p_catch_i:
                    location[i,t+1] = iW
                    state[i,t+1]    = iS
                else: 
                    raise Exception
            else:
                raise Exception

        elif location[i,t] == iH:
            t_cycle = t_incubation + t_asymptomatic + t_symptomatic + t_quarantined
            if t >= t_cycle-1:
                if numpy.any( state[i,(t+1)-t_cycle:(t+1)] - iS*numpy.ones((1, t_cycle)) ) == False:
                    if numpy.any( location[i,(t+1)-t_cycle:(t+1)] - iH*numpy.ones((1, t_cycle)) ) == False:
                        location[i,t+1] = iW
                        state[i,t+1]    = iS
                    elif numpy.any( location[i,(t+1)-t_cycle:(t+1)] - iH*numpy.ones((1, t_cycle)) ) == True:
                        location[i,t+1] = iH
                        state[i,t+1]    = iS
                    else:
                        raise Exception
                elif numpy.any( state[i,(t+1)-t_cycle:(t+1)] - iS*numpy.ones((1, t_cycle)) ) == True:
                    location[i,t+1] = iH
                    state[i,t+1]    = iS
                else:
                    raise Exception

            elif t < t_cycle-1:
                location[i,t+1] = iH
                state[i,t+1]    = iS
            else:
                raise Exception
        else:
            raise Exception



    # state[i,t] == 'Exposed'
    # ---------------
    # Once the Incubation Time has passed:
    # i stays in current location (becasue not Symptomatic yet so doesn't know infected)
    # and i becomes Exposed
    elif state[i,t] == iE:
        if location[i,t] == iW:
            if t >= t_incubation-1:
                if numpy.any( state[i,(t+1)-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                    location[i,t+1] = iW
                    state[i,t+1]    = iIA
                elif numpy.any( state[i,(t+1)-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                    location[i,t+1] = iW
                    state[i,t+1]    = iE
                else:
                    raise Exception
            elif t < t_incubation-1:
                location[i,t+1] = iW
                state[i,t+1]    = iE
            else:
                raise Exception
            
        elif location[i,t] == iH:
            if t >= t_incubation-1:
                if numpy.any( state[i,(t+1)-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == False:
                    location[i,t+1] = iH
                    state[i,t+1]    = iIA
                elif numpy.any( state[i,(t+1)-t_incubation:t+1] - iE*numpy.ones((1, t_incubation)) ) == True:
                    location[i,t+1] = iH
                    state[i,t+1]    = iE
                else:
                    raise Exception
            elif t < t_incubation-1:
                location[i,t+1] = iH
                state[i,t+1]    = iE
            else:
                raise Exception
        else:
            raise Exception
    

    # state[i,t] == 'Infectious-Asymptomatic'
    # ---------------
    # Once the Asymptomatic Time has passed:
    # If i is Transparent, then i becomes Infectious Symptomatic and goes Home
    # If i is Opaque, then i becomes Infectious Symptomatic and goes home with probability p_OAdviseOthers
    # (otherwise i will stay in Work, even though it has Symptoms)
    # This in unless i is an asymptomatic carrier, and never gets symptomatic, in which case i never knows it 
    # has the virus, and so remains Asymptomatic and in Work (and so spreading the virus)
    elif state[i,t] == iIA:
        # Decide if i get Symptomatic
        r_neverUnwell = numpy.random.uniform(low=0.0, high=1.0)
        if r_neverUnwell >= p_neverUnwell:
            if location[i,t] == iW:                
                if t >= t_asymptomatic-1:           
                    if numpy.any( state[i,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == False:                
                        if species[i] == iT:
                            location[i,t+1] = iH
                            state[i,t+1]    = iIS
                        elif species[i] == iO:
                            r_OAdviseOthers = numpy.random.uniform(low=0.0, high=1.0) 
                            if r_OAdviseOthers < p_OAdviseOthers:
                                location[i,t+1] = iH
                                state[i,t+1]    = iIS
                            elif r_OAdviseOthers >= p_OAdviseOthers:
                                location[i,t+1] = iW
                                state[i,t+1]    = iIS 
                            else:
                                raise Exception
                        else: 
                            raise Exception
                    elif numpy.any( state[i,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == True:
                        location[i,t+1] = iW
                        state[i,t+1] = iIA
                    else: 
                        raise Exception
                elif t < t_asymptomatic-1:
                    location[i,t+1] = iW
                    state[i,t+1]    = iIA
                else:
                    raise Exception
                
            elif location[i,t] == iH:
                if t >= t_asymptomatic-1:
                    if numpy.any( state[i,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == False:
                        # Could make it so iO don't stay home long enough.                
                        location[i,t+1] = iH
                        state[i,t+1]    = iIS
                    elif numpy.any( state[i,(t+1)-t_asymptomatic:t+1] - iIA*numpy.ones((1, t_asymptomatic)) ) == True:
                        location[i,t+1] = iH
                        state[i,t+1]    = iIA
                    else: 
                        raise Exception
                elif t < t_asymptomatic-1:
                    location[i,t+1] = iH
                    state[i,t+1]    = iIA
                else:
                    raise Exception
            else:
                raise Exception

        elif r_neverUnwell < p_neverUnwell:
        # I don't get symptomatic
            if location[i,t] == iW:
                if t >= (t_asymptomatic + t_symptomatic)-1:
                    if numpy.any( state[i,(t+1)-(t_asymptomatic+t_symptomatic):t+1] - iIA*numpy.ones((1, (t_asymptomatic+t_symptomatic))) ) == False:
                        r_recovery = numpy.random.uniform(low=0.0, high=1.0)
                        if r_recovery < p_recovery:
                            location[i,t+1] = iW
                            state[i,t+1]    = iR
                        elif r_recovery >= p_recovery:
                            location[i,t+1] = iW
                            state[i,t+1]    = iS
                        else:
                            raise Exception                    
                    elif numpy.any( state[i,(t+1)-(t_asymptomatic+t_symptomatic):t+1] - iIA*numpy.ones((1, (t_asymptomatic+t_symptomatic))) ) == True:
                        location[i,t+1] = iW
                        state[i,t+1]    = iIA          
                    else:
                        raise Exception
                elif t < (t_asymptomatic + t_symptomatic)-1:
                    location[i,t+1] = iW
                    state[i,t+1]    = iIA      
                else: 
                    raise Exception

            elif location[i,t] == iH:
                if t >= (t_asymptomatic + t_symptomatic)-1:
                    if numpy.any( state[i,(t+1)-(t_asymptomatic+t_symptomatic):t+1] - iIA*numpy.ones((1, (t_asymptomatic+t_symptomatic))) ) == False:
                        location[i,t+1] = iH
                        state[i,t+1]    = iQ                  
                    elif numpy.any( state[i,(t+1)-(t_asymptomatic+t_symptomatic):t+1] - iIA*numpy.ones((1, (t_asymptomatic+t_symptomatic))) ) == True:
                        location[i,t+1] = iH
                        state[i,t+1]    = iIA          
                    else:
                        raise Exception
                elif t < (t_asymptomatic + t_symptomatic)-1:
                    location[i,t+1] = iH
                    state[i,t+1]    = iIA      
                else: 
                    raise Exception
            else:
                raise Exception
        else:
            raise Exception


    # state[i,t] == 'Infectious-Symptomatic'
    # (Should be Opaques at Work only now)
    # ---------------
    # Once Symptomatic Time has passed:
    # Opaques at work stay in work, and have a probability p_recovery of becoming Recovered (immune), 
    # otherwise they are Susceptible to cathcing the virus again.
    # Transparents at Home move to teh Quarantined state - they remain at Home, but are no longer Symptomatic.
    elif state[i,t] == iIS:
        if location[i,t] == iW:
            if t >= t_symptomatic-1:
                if numpy.any( state[i,(t+1)-t_symptomatic:t+1] - iIS*numpy.ones((1, t_symptomatic)) ) == False:
                    if species[i] == iT:
                        raise Exception
                    elif species[i] == iO:
                        r_recovery = numpy.random.uniform(low=0.0, high=1.0)
                        # (Could make it so opaques less likely to recover than trans.
                        if r_recovery < p_recovery:
                            location[i,t+1] = iW
                            state[i,t+1]    = iR
                        elif r_recovery >= p_recovery:
                            location[i,t+1] = iW
                            state[i,t+1]    = iS
                        else:
                            raise Exception
                    else:
                        raise Exception
                elif numpy.any( state[i,(t+1)-t_symptomatic:t+1] - iIS*numpy.ones((1, t_symptomatic)) ) == True:
                    # (Could make it so opaques might go home here (so they might go home at any time)
                    # At the moment we have the case where they make that decision at the beginning of this state cycle.
                    location[i,t+1] = iW
                    state[i,t+1]    = iIS
                else:
                    raise Exception
            elif t < t_symptomatic-1:
                location[i,t+1] = iW
                state[i,t+1] = iIS
            else:
                raise Exception

        elif location[i,t] == iH:
            if t >= t_symptomatic-1:
                if numpy.any( state[i,(t+1)-t_symptomatic:t+1] - iIS*numpy.ones((1, t_symptomatic)) ) == False:
                    # (Could make it so opaques don't stay home long enough)           
                    location[i,t+1] = iH
                    state[i,t+1]    = iQ
                elif numpy.any( state[i,(t+1)-t_symptomatic:t+1] - iIS*numpy.ones((1, t_symptomatic)) ) == True:
                    location[i,t+1] = iH
                    state[i,t+1]    = iIS
                else: 
                    raise Exception
            elif t < t_symptomatic-1:
                location[i,t+1] = iH
                state[i,t+1]    = iIS
            else:
                raise Exception
        else:
            raise Exception


    # state[i,t] == 'Quarantined'
    # ---------------
    # Once the Quarantine Time has passed:
    # Transparents ransparents now have a probability p_recovery or becoming immune (just as the Opaques did).
    # If not, then Transparents move back to the Suscpetible state and have a probability of re-infection (just as the Opaques did.)
    elif state[i,t] == iQ:
        if location[i,t] == iW:
            raise Exception
        elif location[i,t] == iH:
            if t >= t_quarantined-1:
                if numpy.any( state[i,(t+1)-t_quarantined:t+1] - iQ*numpy.ones((1, t_quarantined)) ) == False:
                    r_recovery = numpy.random.uniform(low=0.0,high=1.0)
                    if r_recovery < p_recovery:
                        location[i,t+1] = iW
                        state[i,t+1]    = iR
                    elif r_recovery >= p_recovery:
                        location[i,t+1] = iW
                        state[i,t+1]    = iS
                    else:
                        raise Exception
                elif numpy.any( state[i,(t+1)-t_quarantined:t+1] - iQ*numpy.ones((1, t_quarantined)) ) == True:
                    location[i,t+1] = iH
                    state[i,t+1]    = iQ
                else:
                    raise Exception
            elif t < t_quarantined-1:
                location[i,t+1] = iH
                state[i,t+1]    = iQ
            else:
                raise Exception            
        else:
            raise Exception


    # state[i,t] == 'Recovered'
    # ---------------
    # If i is Recovered (Immune), then they stay in this state for all time 
    # and stay at Work for all time.
    # This is therefore the situation where Immune people know they are immune.
    elif state[i,t] == iR:
        if location[i,t] == iW:
            if species[i] in (iT, iO):
                location[i,t+1] = iW
                state[i,t+1] = iR
            else:
                raise Exception
        elif location[i,t] == iH:
            raise Exception
        else: 
            raise Exception 
    


    # state[i,t] == 'Other' (some other number)
    # E.g., state[i,t] == 0 - means rules haven't assigned node i in previous t step. So error.
    else:
        print("Conservation error: State moving to zero. \
               Current state of node i={} is state[i,t]={}. \
               Terminating at time t={}.".format(i, state[i,t], t)  ) 
        raise Exception 

    
    # Check neighbours (to see if I need to be Home)
    # If state[i] != 'Home Infectious-Symp or Home Infectious-Asymp or Recovered':
    # This may modify state[i,t+1] to 'Home Asymp', or else leave it as <dictated above>.
    # Could have it so that Recovered people have to go home too (case where people don't know they're Immune)
    # ---------------
    # If i in Work and not Recovered or Quarantined:
    # If a connection j of i is Symptomatic tomorrow, 
    # Then if this connection j is Transparent, they warn i tomorrow morning not to come into work
    # So i's t+1 location gets modified to Home, as long as i is a Transparent
    # If i is Opaque though, i may choose to ignore this advice, and will only isolate due to this connection's advice 
    # with probability p_OFollowAdvice.
    # If the connection j is Opaque, then they will only advise others that they are Symptomatic tomorrow with probability p_OAdviseOthers 
    # (i.e., the same probability that j stays Home itself when it's Symptomatic).
    if state[i,t] in (iS, iE, iIA, iIS):
        if location[i,t] == iW:
              
            for j in range(N):
                indicator_symptomaticTomorrow                = ( state[j,t+1] == (iIS) )
                indicator_symptomaticNeighbourTomorrow       = A[i,j]*indicator_symptomaticTomorrow
                indicator_symptomaticTomorrowConnectionToday = indicator_symptomaticNeighbourTomorrow*( location[j,t] == iW )

                if indicator_symptomaticTomorrowConnectionToday == True:
                    
                    if species[j] == iT:
                        if species[i] == iT:
                            location[i,t+1] = iH
                            if state[i,t+1] == iR:
                                state[i,t+1] = iQ
                            else: 
                                pass
                        elif species[i] == iO:
                            r_OFollowAdvice = numpy.random.uniform(low=0.0,high=1.0)
                            if r_OFollowAdvice < p_OFollowAdvice:
                                location[i,t+1] = iH
                                if state[i,t+1] == iR:
                                    state[i,t+1] = iQ
                                else:
                                    pass
                            elif r_OFollowAdvice >= p_OFollowAdvice:
                                pass
                            else:
                                raise Exception
                        else:
                            raise Exception

                    elif species[j] == iO:
                        r_OAdviseOthers = numpy.random.uniform(low=0.0,high=1.0)
                        if r_OAdviseOthers < p_OAdviseOthers:
                            if species[i] == iT:
                                location[i,t+1] = iH
                                if state[i,t+1] == iR:
                                    state[i,t+1] = iQ
                                else:
                                    pass
                            elif species[i] == iO:
                                r_OFollowAdvice = numpy.random.uniform(low=0.0,high=1.0)
                                if r_OFollowAdvice < p_OFollowAdvice:
                                    location[i,t+1] = iH
                                    if state[i,t+1] == iR:
                                        state[i,t+1] = iQ
                                    else:
                                        pass
                                elif r_OFollowAdvice >= p_OFollowAdvice:
                                    pass
                                else:
                                    raise Exception
                            else:
                                raise Exception
                        elif r_OAdviseOthers >= p_OAdviseOthers:
                            pass
                        else: 
                            raise Exception
                    else:
                        raise Exception
                elif indicator_symptomaticTomorrowConnectionToday == False:
                    pass
                else:
                    raise Exception  
        elif location[i,t] == iH:
            pass
        else:
            raise Exception
    elif state[i,t] in (iR, iQ):
        pass
    else:
        raise Exception

    # Check mass conserved:
    # ---------------------
    # Check everyone has been assigned a state:
    if state[i,t+1] in (iS, iE, iIA, iIS, iR, iQ):
        pass
    else:
        print("Not everyone has been assigned a state.")
        raise Exception 
    # Check everyone has been assigned a location:
    if location[i,t+1] in (iW, iH):
        pass
    else:
        print("Not everyone has been assigned a location.")

    return location[i,t+1], state[i,t+1] 



