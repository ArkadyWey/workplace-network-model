#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import scipy
from scipy import interpolate
import argparse
import os
import json
import time

# Internal modules
import visualise
import topography
import models
import output
import utils

# -----------------------------------------------
# README
# use_MC-O_ser module: 
# Use output of the run_MC-O module to create time series plot with time on the x-axis, found in V_Time folders.
# For example, this code was used to generate the plots in the V_Time folders:
#  ./figures/cold/V_Time
#  ./figures/hot/V_Time

# -----------------------------------------------

start_time = time.time()
print("Started using data at relative time: {}\n".format(start_time - start_time))

# Add parameter_file_path as parser requirement
parser = argparse.ArgumentParser(description="Parameter file path")

# Add all parameter file paths - each degree run is a separate parameter file
# Add all parameter file paths
# - Each degree run is a separate parameter file
# - So to use more than one degree, add parameter file for each degree
# - (see ./tests/test_run_MC-O for correct directory structure)
parser.add_argument("-f", dest="parameter_file_paths", nargs="*", required=True,
                    help="Input parameter file paths (wich hold degree info)", metavar="FILE",
                    type=lambda parser_arg: utils.check_if_parser_arg_exists(parser=parser, file_as_parser_arg=parser_arg))

# Add k vals
# - k is the index of the opacity run
# - For example, "-k 0 50 100" would use species runs 0, 50, 100
# - So would plot graphs for 0 50 and 100 % workforce opacity (assuming that species data files are named with convention)
# - (see ./tests/test_run_MC-O for example data files)
parser.add_argument("-k", dest="k_vals", nargs="*", required=True,
                    help="Input opacity indices k", metavar="N",
                    type=int)

# Return parser arguments
parser_args = parser.parse_args()
parameter_file_paths = parser_args.parameter_file_paths
k_vals = parser_args.k_vals

# Define number of degrees (from number of parameter files in parser)
num_degs = len(parameter_file_paths)

# Define number of opacity vals (number of k values in parser)
num_k_vals = len(k_vals)
# Note: Read k_vals as k vals

# Number of statistics to analyse - 
# Here we look at:
# 0. ill_count_prop_mean (defined below)
# 1. home_count_prop_mean (defined below)
# 2. productivity_workforce_normalWorkplace_prop_mean (defined below)
# 3. productivity_workforce_WFHWorkplace_prop_mean (defined below)
# 4. productivity_workforce_NonWFHWorkplace_prop_mean (defined below)
num_stats = 5

# Define empty lists to save each run (defined below)
average_stats_list = []
std_stats_list = []
degree_list = []
for i_deg in range(num_degs):

    # Try to open ith parameter file from parameter_file_paths returned by parser
    try:    
        # Read parameter_file and load contents as parameter_dict
        with open(parser_args.parameter_file_paths[i_deg], 'r') as parameter_file:
            parameters_dict = json.load(parameter_file)
    except Exception:
        raise
    
    # Add degree in current parameter file to degree list 
    # Since d is degree probability (prob of connection between two nodes), multiply by 100 to get degree
    # degre_list[i] = degree corresponding to ith parameter file.
    degree_list.append(parameters_dict["d"]*100)

    # Monte Carlo parameters
    # ---------------
    num_opacities    = parameters_dict["N"] + 1
    num_state_runs   = parameters_dict["num_state_runs"]

    # Directories 
    parameters_dir_path = os.path.abspath(os.path.dirname(parser_args.parameter_file_paths[i_deg]))
    print(parameters_dir_path)
    output_dir_path = os.path.join(parameters_dir_path, 'output')  


    # Run Monte Carlo
    # ------------
    # average_stats[st, k, t] = average of statistic st (avd over state runs) at opacity k at time t    
    average_stats = numpy.zeros((num_stats, num_k_vals, parameters_dict["T"]))
    std_stats     = numpy.zeros((num_stats, num_k_vals, parameters_dict["T"]))
    for i_k in range(num_k_vals):
        print("Using species run index k:", k_vals[i_k], "of", num_k_vals-1)

        # Define the opacity run/species run index
        k = k_vals[i_k]

        # Define species (Reset from last species run)
        species = output.csv_to_species(i_species_run=k, output_dir_path=output_dir_path)

        # stats[st,d,r] = statistic st at degree d on rth model run
        stats = numpy.zeros((num_stats, num_state_runs, parameters_dict["T"]))
        # r indexes the model run (inner loop)
        for r in range(num_state_runs):

            # Initialise location and state (reset state and location from last run)
            # ---------------
            state         = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is state at given time
            location      = numpy.zeros((parameters_dict["N"],parameters_dict["T"])) # Each column is location at given time

            # Calculate location and state of i at t+1 given information about i at t
            location, state = output.csv_to_state_location(i_species_run=k, i_state_location_run=r, output_dir_path=output_dir_path)


            # Calculate stats
            # ----------------
            # -------------------------------------------------------------------------------------------------
            # Define stats

            # location_and_state_count[s,t,l] = Number of people in state s and location l at time t 
            location_and_state_count = output.location_and_state_counter(parameters_dict["N"],parameters_dict["T"],
                                                                         location, state,
                                                                         iT=1, iO=-1, 
                                                                         iW=1, iH=-1,       
                                                                         iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            # location_and_state_count_prop[s,t,l] = proportion of nodes in state s and location l at time t
            location_and_state_count_prop = location_and_state_count/parameters_dict["N"]


            # ill_at_location_count_prop[t,l] = proportion of nodes ill at time t and location l
            ill_at_location_count_prop = location_and_state_count_prop[parameters_dict["iE"] -1,:,:] + \
                                         location_and_state_count_prop[parameters_dict["iIA"]-1,:,:] + \
                                         location_and_state_count_prop[parameters_dict["iIS"]-1,:,:]
            
            # ill_count_prop[t] = proportion of nodes ill at time t in any location
            ill_count_prop     = numpy.sum(a=ill_at_location_count_prop, axis=1)

            # home_at_state_count_prop[s,t] = proportion of nodes at home in any state  s at time t
            home_at_state_count_prop = location_and_state_count_prop[:,:,1]
            # home_count_prop[t] = proportion of nodes at home in any state
            home_count_prop          = numpy.sum(home_at_state_count_prop, axis=0)

            # Record stats
            stats[0,r,:] = ill_count_prop
            stats[1,r,:] = home_count_prop

            # --------------------------------------------------------------------------------------------------        
            # Define stats

            # productivity_workforce_typeWorkplace[t] = productivity of workforce at time t (with type of workplace productivity measure)
            productivity_workforce_normalWorkplace = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=0.2, prod_WellHome=0.7,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            productivity_workforce_WFHWorkplace    = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=0, prod_WellHome=1,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            productivity_workforce_NonWFHWorkplace = output.productivity_workforce(N=parameters_dict["N"], T=parameters_dict["T"],
                                                                                   location=location, state=state,
                                                                                   prod_IllWork=1, prod_WellHome=0,
                                                                                   iT=1, iO=-1, 
                                                                                   iW=1, iH=-1,       
                                                                                   iS=1, iE= 2, iIA=3, iIS=4, iR=5, iQ=6)
            # productivity_workforce_prop[t] = productivity proportion of the workforce (per total possible productivity which is 1 for all) (for type of workplace)
            productivity_workforce_normalWorkplace_prop = productivity_workforce_normalWorkplace/parameters_dict["N"]
            productivity_workforce_WFHWorkplace_prop    = productivity_workforce_WFHWorkplace/parameters_dict["N"]
            productivity_workforce_NonWFHWorkplace_prop = productivity_workforce_NonWFHWorkplace/parameters_dict["N"]

            # Record stats
            stats[2,r,:] = productivity_workforce_normalWorkplace_prop
            stats[3,r,:] = productivity_workforce_WFHWorkplace_prop
            stats[4,r,:] = productivity_workforce_NonWFHWorkplace_prop


            # -------------------------------------------------------------------------------------------------
            # -------------------------------------------------------------------------------------------------    
        # Take the mean of the stats over all model runs
        ill_count_prop_mean                              = numpy.mean(a=stats[0,:,:], axis=0)
        home_count_prop_mean                             = numpy.mean(a=stats[1,:,:], axis=0)
        productivity_workforce_normalWorkplace_prop_mean = numpy.mean(a=stats[2,:,:], axis=0)
        productivity_workforce_WFHWorkplace_prop_mean    = numpy.mean(a=stats[3,:,:], axis=0)
        productivity_workforce_NonWFHWorkplace_prop_mean = numpy.mean(a=stats[4,:,:], axis=0)
        
        # Add mean stats to average_stats
        # average_stats[st,i_k] = average (over model runs) of statitic st at opacity with index k
        average_stats[0,i_k,:] = ill_count_prop_mean
        average_stats[1,i_k,:] = home_count_prop_mean
        average_stats[2,i_k,:] = productivity_workforce_normalWorkplace_prop_mean
        average_stats[3,i_k,:] = productivity_workforce_WFHWorkplace_prop_mean
        average_stats[4,i_k,:] = productivity_workforce_NonWFHWorkplace_prop_mean

        # ------------------------------------------------------------------------------------------------------------
        # Calculate the standardd deviation of the stats over all model runs (used for error bars)

        # std_stats[st,i_k] = standard deviation (over model runs) of statitic st at opacity with index k
        std_stats[0,i_k] = numpy.std(a=stats[0,:,:], axis=0)
        std_stats[1,i_k] = numpy.std(a=stats[1,:,:], axis=0)
        std_stats[2,i_k] = numpy.std(a=stats[2,:,:], axis=0)
        std_stats[3,i_k] = numpy.std(a=stats[3,:,:], axis=0)
        std_stats[4,i_k] = numpy.std(a=stats[4,:,:], axis=0)
               
        # ------------------------------------------------------------------------------------------------------------
    # average_stats_list[i_deg, st, i_k] = mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
    average_stats_list.append(100*average_stats)
    # std_stats_list[i_deg, st, i_k] = standard dev (over model runs) of statitic st at opacity with index k at degree at index i_deg
    std_stats_list.append(100*std_stats)
    # Note: Stats are proportions (0-1) - multiplied by 100 to convert to percentage

# Convert list to array
# average_stats_array[i_deg, st, i_k] = mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
average_stats_array = numpy.asarray(average_stats_list) 
# std_stats_array[i_deg, st, i_k] = standard dev (over model runs) of statitic st at opacity with index k at degree at index i_deg
std_stats_array = numpy.asarray(std_stats_list) 

# lower_stats_array[i_deg, st, i_k] = 1 standard dev below mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
# higher_stats_array[i_deg, st, i_k] = 1 standard dev above mean (over model runs) of statitic st at opacity with index k at degree at index i_deg
lower_stats_array = average_stats_array - std_stats_array
upper_stats_array = average_stats_array + std_stats_array


# Plotting
# -------------------
# If 'paper' directory does not exist in directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(parameters_dir_path,"./../" , 'paper')) == False:
    os.mkdir(os.path.join(parameters_dir_path, "./../",'paper'))
# Define paper file path
paper_dir_path = os.path.join(parameters_dir_path, "./../",  'paper')

# If 'V_Degree' directory does not exist in paper directory, make one (used first time we run simulation)
if os.path.exists(os.path.join(paper_dir_path , 'V_Time')) == False:
    os.mkdir(os.path.join(paper_dir_path,'V_Time'))
# Define paper file path
V_Time_dir_path = os.path.join(paper_dir_path, 'V_Time')

# -------------------------------------------------------------------------------------------------------------------
t = numpy.linspace(0,parameters_dict["T"],parameters_dict["T"])


# Plot: StateCount_V_Time
# Max number of people that were ever ill at once as a function of time, for given dregree and opacity
# Max number of people that were ever home at once as a function of time, for given degree and opacity
stateCount_labels = ["Ill", "Home"]
stateCount_colors = ["red", "navy"]
for i_deg in range(num_degs):
    for i_k in range(num_k_vals):
        fig,gs,ax = visualise.plot_single_frame()
        for i in range(2):
            i_stat=i
            ax.plot(t[:], average_stats_array[i_deg,i_stat,i_k,:], ls="-", color=stateCount_colors[i])

        #ax.set_ylim(-0.0,50 + 0.00)
        visualise.style_axes(ax, r"Time",  r"% workforce")
        visualise.save_svg(fig, gs, os.path.join(V_Time_dir_path, 'StateCount_V_Time_d-{:d}_o-{:d}.svg'.format(int(degree_list[i_deg]), k_vals[i_k]) ))


# Plot: Prod_V_Time
# Productivity of the workforce as a function of time (by three measures of productivity)
prod_labels = ["average", "wfh"  , "non wfh"]
prod_colors = ["orange" , "green", "red" ]
for i_deg in range(num_degs):
    for i_k in range(num_k_vals):
        fig,gs,ax = visualise.plot_single_frame()
        for i in range(3):
            i_stat=i+2
            ax.plot(t[:], 100 - average_stats_array[i_deg,i_stat,i_k,:], ls="-", color=prod_colors[i])

        #ax.set_ylim(-0.0,30 + 0.00)
        visualise.style_axes(ax, r"Time",  r"% productivity deficit")
        visualise.save_svg(fig, gs, os.path.join(V_Time_dir_path, 'Prod_V_Time_d-{:d}_o-{:d}.svg'.format(int(degree_list[i_deg]), k_vals[i_k]) ))


print("\nFinished using data at relative time:  {}".format(time.time() - start_time))